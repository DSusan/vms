"use strict";
var KTDatatablesDataSourceHtml = function() {

	// var initTable1 = function() {
	// 	var table = $('#kt_table_1');

	// 	begin first table
	// 	table.DataTable({
	// 		responsive: true,
	// 		columnDefs: [
	// 			{
	// 				targets: -1,
	// 				title: 'Actions',
	// 				orderable: false,
	// 				render: function(data, type, full, meta) {
	// 					return `
 //                        <span class="dropdown">
 //                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
 //                              <i class="la la-ellipsis-h"></i>
 //                            </a>
 //                            <div class="dropdown-menu dropdown-menu-right">
 //                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
 //                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>
 //                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
 //                            </div>
 //                        </span>
 //                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
 //                          <i class="la la-edit"></i>
 //                        </a>`;
	// 				},
	// 			},
	// 			{
	// 				targets: 8,
	// 				render: function(data, type, full, meta) {
	// 					var status = {
	// 						1: {'title': 'Pending', 'class': 'kt-badge--brand'},
	// 						2: {'title': 'Delivered', 'class': ' kt-badge--danger'},
	// 						3: {'title': 'Canceled', 'class': ' kt-badge--primary'},
	// 						4: {'title': 'Success', 'class': ' kt-badge--success'},
	// 						5: {'title': 'Info', 'class': ' kt-badge--info'},
	// 						6: {'title': 'Danger', 'class': ' kt-badge--danger'},
	// 						7: {'title': 'Warning', 'class': ' kt-badge--warning'},
	// 					};
	// 					if (typeof status[data] === 'undefined') {
	// 						return data;
	// 					}
	// 					return '<span class="kt-badge ' + status[data].class + ' kt-badge--inline kt-badge--pill">' + status[data].title + '</span>';
	// 				},
	// 			},
	// 			{
	// 				targets: 9,
	// 				render: function(data, type, full, meta) {
	// 					var status = {
	// 						1: {'title': 'Online', 'state': 'danger'},
	// 						2: {'title': 'Retail', 'state': 'primary'},
	// 						3: {'title': 'Direct', 'state': 'success'},
	// 					};
	// 					if (typeof status[data] === 'undefined') {
	// 						return data;
	// 					}
	// 					return '<span class="kt-badge kt-badge--' + status[data].state + ' kt-badge--dot"></span>&nbsp;' +
	// 						'<span class="kt-font-bold kt-font-' + status[data].state + '">' + status[data].title + '</span>';
	// 				},
	// 			},
	// 		],
	// 	});

	// };
	
	var initTable1 = function() {
		var table = $('#kt_table_1');

		// begin first table
		var table = table.DataTable({
			responsive: true,
			// scrollY: '50vh',
			// scrollX: true,
			// scrollCollapse: true,
			select: {
				style: 'multi',
				selector: '.kt-checkable',
			},
			headerCallback: function(thead, data, start, end, display) {
				thead.getElementsByTagName('th')[0].innerHTML = `
                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid kt-checkbox--brand">
                        <input type="checkbox" value="" class="kt-group-checkable">
                        <span></span>
                    </label>`;
			},
			columnDefs: [
				{
					targets: 0,
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid kt-checkbox--brand">
                            <input type="checkbox" value="" class="kt-checkable">
                            <span></span>
                        </label>`;
					},
				},
			],
		});

		table.on('change', '.kt-group-checkable', function() {
			var set = $(this).closest('table').find('.kt-checkable');
			var checked = $(this).is(':checked');
			console.log('hai')

			$(set).each(function() {
				if (checked) {
					$(this).prop('checked', true);
					table.rows($(this).closest('tr')).select();
					// console.log($(this).attr('value'))
					var rowDataLength = table.rows( { selected: true } ).data().length;
					var rowData = table.rows( { selected: true } ).data()[rowDataLength-1];
					console.log(rowData)
				}
				else {
					$(this).prop('checked', false);
					table.rows($(this).closest('tr')).deselect();
				}
			});
		});
	};

	var positionTable = function() {
		var table = $('#positionTable');

		// begin first table
		var table = table.DataTable({
			responsive: true,
			// scrollY: '50vh',
			// scrollX: true,
			// scrollCollapse: true,
			select: {
				style: 'multi',
				selector: '.kt-checkable',
			},
			headerCallback: function(thead, data, start, end, display) {
				thead.getElementsByTagName('th')[0].innerHTML = `
                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid kt-checkbox--brand">
                        <input type="checkbox" value="" class="kt-group-checkable">
                        <span></span>
                    </label>`;
			},
			columnDefs: [
				{
					targets: 0,
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid kt-checkbox--brand">
                            <input type="checkbox" value="" class="kt-checkable">
                            <span></span>
                        </label>`;
					},
				},
			],
		});

		table.on('change', '.kt-group-checkable', function() {
			var set = $(this).closest('table').find('.kt-checkable');
			var checked = $(this).is(':checked');
			console.log('hai')

			$(set).each(function() {
				if (checked) {
					$(this).prop('checked', true);
					table.rows($(this).closest('tr')).select();
					// console.log($(this).attr('value'))
					var rowDataLength = table.rows( { selected: true } ).data().length;
					var rowData = table.rows( { selected: true } ).data()[rowDataLength-1];
					console.log(rowData)
				}
				else {
					$(this).prop('checked', false);
					table.rows($(this).closest('tr')).deselect();
				}
			});
		});
	};

	var roleTable = function() {
		var table = $('#roleTable');

		// begin first table
		var table = table.DataTable({
			responsive: true,
			// scrollY: '50vh',
			// scrollX: true,
			// scrollCollapse: true,
			select: {
				style: 'multi',
				selector: '.kt-checkable',
			},
			headerCallback: function(thead, data, start, end, display) {
				thead.getElementsByTagName('th')[0].innerHTML = `
                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid kt-checkbox--brand">
                        <input type="checkbox" value="" class="kt-group-checkable">
                        <span></span>
                    </label>`;
			},
			columnDefs: [
				{
					targets: 0,
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid kt-checkbox--brand">
                            <input type="checkbox" value="" class="kt-checkable">
                            <span></span>
                        </label>`;
					},
				},
			],
		});

		table.on('change', '.kt-group-checkable', function() {
			var set = $(this).closest('table').find('.kt-checkable');
			var checked = $(this).is(':checked');
			console.log('hai')

			$(set).each(function() {
				if (checked) {
					$(this).prop('checked', true);
					table.rows($(this).closest('tr')).select();
					// console.log($(this).attr('value'))
					var rowDataLength = table.rows( { selected: true } ).data().length;
					var rowData = table.rows( { selected: true } ).data()[rowDataLength-1];
					console.log(rowData)
				}
				else {
					$(this).prop('checked', false);
					table.rows($(this).closest('tr')).deselect();
				}
			});
		});
	};

	var divisionTable = function() {
		var table = $('#divisionTable');

		// begin first table
		var table = table.DataTable({
			responsive: true,
			// scrollY: '50vh',
			// scrollX: true,
			// scrollCollapse: true,
			select: {
				style: 'multi',
				selector: '.kt-checkable',
			},
			headerCallback: function(thead, data, start, end, display) {
				thead.getElementsByTagName('th')[0].innerHTML = `
                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid kt-checkbox--brand">
                        <input type="checkbox" value="" class="kt-group-checkable">
                        <span></span>
                    </label>`;
			},
			columnDefs: [
				{
					targets: 0,
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid kt-checkbox--brand">
                            <input type="checkbox" value="" class="kt-checkable">
                            <span></span>
                        </label>`;
					},
				},
			],
		});

		table.on('change', '.kt-group-checkable', function() {
			var set = $(this).closest('table').find('.kt-checkable');
			var checked = $(this).is(':checked');
			console.log('hai')

			$(set).each(function() {
				if (checked) {
					$(this).prop('checked', true);
					table.rows($(this).closest('tr')).select();
					// console.log($(this).attr('value'))
					var rowDataLength = table.rows( { selected: true } ).data().length;
					var rowData = table.rows( { selected: true } ).data()[rowDataLength-1];
					console.log(rowData)
				}
				else {
					$(this).prop('checked', false);
					table.rows($(this).closest('tr')).deselect();
				}
			});
		});
	};

	var indexWellProd = function() {
		var table = $('#indexWellProd');

		// begin first table
		table.DataTable({
			// responsive: true,
			// scrollY: '50vh',
			scrollX: true,
			scrollCollapse: true,
		});

	};

	var indexWellInj = function() {
		var table = $('#indexWellInj');

		// begin first table
		table.DataTable({
			// responsive: true,
			// scrollY: '50vh',
			scrollX: true,
			scrollCollapse: true,
		});

	};

	var indexSgsSep = function() {
		var table = $('#indexSgsSep');

		// begin first table
		table.DataTable({
			// responsive: true,
			// scrollY: '50vh',
			scrollX: true,
			scrollCollapse: true,
		});

	};

	var dataProdTest = function() {
		var table = $('#dataProdTest');

		// begin first table
		table.DataTable({
			// responsive: true,
			scrollX: true,
			scrollCollapse: true,
		});

	};

	var dataInjTest = function() {
		var table = $('#dataInjTest');

		// begin first table
		table.DataTable({
			// responsive: true,
			// scrollY: '50vh',
			scrollX: true,
			scrollCollapse: true,
		});

	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
			indexWellProd();
			indexWellInj();
			indexSgsSep();
			dataProdTest();
			dataInjTest();
			positionTable();
			roleTable();
			divisionTable();
		},

	};

}();

jQuery(document).ready(function() {
	KTDatatablesDataSourceHtml.init();
});