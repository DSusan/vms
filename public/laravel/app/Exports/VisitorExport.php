<?php

namespace App\Exports;

use App\Models\Visitor;
use App\Models\VisitorActivity;
use Maatwebsite\Excel\Concerns\FromQuery;
use DB;

class VisitorExport implements FromQuery
{
    public function __construct(string $id)
    {
        $this->id = $id;
    }
    public function query()
    {
       $id = $this->id;
       $x = explode(",",$id);
       for($i=0;$i<count($x);$i++){
           $d[] = $x[$i];
       }
    
       return VisitorActivity::query()->select("visitor_activity.*", "b.name as visitor_name")
       ->leftjoin("visitor as b", function($query){
           $query->on("b.id","=","visitor_activity.visitor_id");
       })
       ->whereIn("b.id",$d);
    }
}
