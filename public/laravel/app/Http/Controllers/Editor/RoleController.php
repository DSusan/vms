<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;

class RoleController extends Controller
{
	// public function index()
	// {
	// 	$number = 1;
	// 	$roles = Role::all();

	// 	return view ('editor.role.index', compact('roles','number'));
	// }

	public function store(Request $request)
	{
		$role = new Role;
		$role->name = $request->input('role');
		$role->desc = $request->input('desc');
		$role->created_by = session('nameUser');
		$role->save();

		// return redirect()->action('Editor\RoleController@index');
		return redirect()->action('Editor\RoleDivController@index');
	}

	public function update($id, Request $request)
	{
		$role = Role::find($id);
		$role->name = $request->input('roleEdit');
		$role->desc = $request->input('descEdit');
		$role->updated_by = session('nameUser');
		$role->save();

		// return redirect()->action('Editor\RoleController@index');
		return redirect()->action('Editor\RoleDivController@index');
	}

	public function delete($id)
	{
		Role::find($id)->delete();
		
		// return redirect()->action('Editor\RoleController@index');
		return redirect()->action('Editor\RoleDivController@index');
	}
}
