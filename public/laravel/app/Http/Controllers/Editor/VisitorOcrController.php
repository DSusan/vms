<?php

namespace App\Http\Controllers\Editor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VisitorOcrController extends Controller
{
    public function index()
    {
        return view("editor.visitorocr.index");
    }
    public function upload(Request $request)
    {
        $extension = $request->file('file');
        $extension = $request->file('file')->getClientOriginalExtension();
        $dir = 'ocrdata/';
        $unikName = uniqid().'_'.time().'_'.date('Ymd');
        $filename = $unikName.'.'.$extension;
        $request->file('file')->move($dir, $filename);
        $address =  $dir.$unikName;
        if(!file_exists( $address.".txt")){
            fopen( $address.".txt","wb");
        }
        shell_exec('"C:\\Program Files (x86)\\Tesseract-OCR\\tesseract" "C:\\xampp\\htdocs\\vms\\public\\ocrdata\\'.$filename.'" '.$address);
        $myfile = fopen( $address.".txt", "r") or die("Unable to open file!");
        $i = 1;
        $data = array();
        $NIK = "";
        $NAMA = "";
        $ALAMAT = "";
        $rtrw = "";
        $kel = "";
        $kec = "";
        while ($line = fgets($myfile)) {
            // <... Do your work with the line ...>
            $string = str_replace(' ', '-', $line); // Replaces all spaces with hyphens.
            if($i == 6){
                $NIK = str_replace('', '-',preg_replace('/[^A-Za-z0-9\-]/', '', $string));
            }
            if($i == 7){
                $NAMA = str_replace('', '-',preg_replace('/[^A-Za-z0-9\-]/', '', $string));
            }
            if($i == 10){
                $ALAMAT = str_replace('', '-',preg_replace('/[^A-Za-z0-9\-]/', '', $string));
            }
            if($i == 11){
                $rtrw = str_replace('', '-',preg_replace('/[^A-Za-z0-9\-]/', '', $string));
            }
            if($i == 12){
                $kel = str_replace('', '-',preg_replace('/[^A-Za-z0-9\-]/', '', $string));
            }
            if($i == 13){
                $kec = str_replace('', '-',preg_replace('/[^A-Za-z0-9\-]/', '', $string));
            }
            $i++;
        }
        $data['VisitorPre'][] = array(
            "nik" => $NIK,
            "nama" => $NAMA,
            "alamat" => $ALAMAT,
            "rtrw" => $ALAMAT,
            "kel" => $ALAMAT,
            "kec" => $ALAMAT,
        );
        return response()->json($data);
    }
}
