<?php

namespace App\Http\Controllers\Editor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Auth;
use DB;
use Session;
use Validator;
use App\Models\VisitorPre;
use App\Models\Visitor;

class VisitorPreController extends Controller
{
    public function index(){
        return view("editor.visitorpreregister.index");
    }
    public function getDataDtl(Request $request)
    {
        $VisitorPre = VisitorPre::where("id","=",$request->visitor_pre_id)->get();
        $data = array();
        $data['VisitorPre'] = $VisitorPre;
        return response()->json($data);
    }
    public function getData(Request $request){
        $searchByTipe = isset($request->ssec) ? $request->ssec : "";
        $itemSearch = isset($request->searchByName) ? $request->searchByName : "";
        $limit = isset($request->length) ? $request->length : 10;
        $data = array();
        $VisitorPre = VisitorPre::offset($request->start)
        ->limit($limit)
        ->get();
       # dd($VisitorPre);
        $us = VisitorPre::all();
        $total_records = count($us);
        $data['draw'] = intval( $request->draw );
        $data['recordsTotal'] = count($us);
        $data['recordsFiltered'] = count($us);
        $data['data'] = $VisitorPre;
        return response()->json($data);
    }
    public function insert(Request $request){

        $validator = Validator::make($request->all(), [
            'name'=> 'required',
            'address' => 'required',
            'mobile' => 'required',
            'company' => 'required',
            'pic' => 'required',
            'purpose' => 'required',
            'floor' => 'required', 
        ]);

        if ($validator->passes()) {

            $VisitorPre = new VisitorPre;
            $VisitorPre->name = strtolower($request->name);
            $VisitorPre->address = strtolower($request->address);
            $VisitorPre->mobile = strtolower($request->mobile);
            $VisitorPre->company = strtolower($request->company);
            $VisitorPre->period_from = strtolower($request->period_from);
            $VisitorPre->period_to = strtolower($request->period_to);
            $VisitorPre->pic = strtolower($request->pic);
            $VisitorPre->vid = strtolower($request->vid);
            $VisitorPre->purpose = strtolower($request->purpose);
            $VisitorPre->floor = strtolower($request->floor);
            $VisitorPre->created_by = intval($request->user_id);
            $VisitorPre->status = 1;
            $VisitorPre->save();

            return response()->json(['success'=>1, "msg"=>"Data Updated"]);

        } else {

            return response()->json(['error'=>$validator->errors()->all()]);

        }
        
    }
    public function edit(Request $request){
       
        $validator = Validator::make($request->all(), [
            'name'=> 'required',
            'address' => 'required',
            'mobile' => 'required',
            'company' => 'required',
            'pic' => 'required',
            'purpose' => 'required',
            'floor' => 'required', 
            'visitor_pre_id' => 'required'
        ]);

        if ($validator->passes()) {
            $VisitorPre = VisitorPre::where("id","=",$request->visitor_pre_id)->first();
            $VisitorPre->name = strtolower($request->name);
            $VisitorPre->address = strtolower($request->address);
            $VisitorPre->mobile = strtolower($request->mobile);
            $VisitorPre->company = strtolower($request->company);
            $VisitorPre->period_from = strtolower($request->period_from);
            $VisitorPre->period_to = strtolower($request->period_to);
            $VisitorPre->pic = strtolower($request->pic);
            $VisitorPre->vid = strtolower($request->vid);
            $VisitorPre->purpose = strtolower($request->purpose);
            $VisitorPre->floor = strtolower($request->floor);
            $VisitorPre->created_by = intval($request->user_id);
           // $VisitorPre->status = 1;
            $VisitorPre->save();
            return response()->json(['success'=>1, "msg"=>"Data Updated"]);
        } else {
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }
    public function app(Request $request){
        $VisitorPre = VisitorPre::where("id","=",$request->visitor_pre_id)->first();
        $VisitorPre->status = 2;
        $VisitorPre->app_at = date("Y-m-d H:i:s");
        $VisitorPre->app_by = $request->user_id;
        $VisitorPre->save();

        $VisitorPre = VisitorPre::where("id","=",$request->visitor_pre_id)->get();
        foreach($VisitorPre as $key => $val){
             # masuk ke visitor
            $visitor = new visitor;
            $visitor->name = strtolower($val->name);
            $visitor->address = strtolower($val->address);
            $visitor->company = strtolower($val->company);
            $visitor->mobile = strtolower($val->mobile);
            $visitor->vid = strtolower($val->vid);
            $visitor->visitor_pre_id = strtolower($request->visitor_pre_id);
            $visitor->save();
        }
        
        return response()->json(['success'=>1, "msg"=>"Data Updated"]);
    }

}
