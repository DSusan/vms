<?php

namespace App\Http\Controllers\Editor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HardwareInfo;
use App\Models\HardwareType;

class HardwareInfoController extends Controller
{
        public function index()
        {
            $number = 1;
            $hardwareTypes = HardwareType::pluck('name', 'id');
            $hardwareInfos = HardwareInfo::select('hardware_type.name AS hardwareTypeName','hardware_info.*')
            ->join('hardware_type', 'hardware_type.id', '=', 'hardware_info.id_hardware_type')
            ->get();
            return view('editor.hardware.index', compact('number','hardwareInfos','hardwareTypes'));
        }

        public function store(Request $request)
        {
            //dd($request);
            $addHardwareInfo = new HardwareInfo;
            $addHardwareInfo->name = $request->name;
            $addHardwareInfo->list_ip = $request->list_ip;
            $editHardwareInfo->folder = $request->folder;
            if( $request->has('aktif') ){
                $addHardwareInfo->aktif = 1;
            } else {
                $addHardwareInfo->aktif = 0;
            }
            $addHardwareInfo->floor = $request->floor;
            $addHardwareInfo->id_hardware_type = $request->id_hardware_type;
            $addHardwareInfo->created_by = session('namaUser');
            $addHardwareInfo->save();

            return redirect()->action('Editor\HardwareInfoController@index');
        }

        public function update($id, Request $request)
        {
            $editHardwareInfo = HardwareInfo::find($id);
            $editHardwareInfo->name = $request->name;
            $editHardwareInfo->list_ip = $request->list_ip;
            $editHardwareInfo->floor = $request->floor;
            $editHardwareInfo->id_hardware_type = $request->id_hardware_type;
            $editHardwareInfo->folder = $request->folder;
           # $editHardwareInfo->aktif = floatval($request->aktif);
            if( $request->has('aktif') ){
                $editHardwareInfo->aktif = 1;
            } else {
                $editHardwareInfo->aktif = 0;
            }
            $editHardwareInfo->created_by = session('namaUser');
            $editHardwareInfo->save();

            return redirect()->action('Editor\HardwareInfoController@index');

        }

        public function delete($id)
	{
		HardwareInfo::find($id)->delete();
		
		    return redirect()->action('Editor\HardwareInfoController@index');
	}


}
