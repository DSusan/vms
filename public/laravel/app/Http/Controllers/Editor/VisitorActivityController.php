<?php

namespace App\Http\Controllers\Editor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Auth;
use DB;
use Session;
use Validator;
use App\Models\Visitor;
use App\Models\VisitorActivity;

class VisitorActivityController extends Controller
{
    public function index(){
        return view("editor.visitoractivity.index");
    }
    public function getVisitor(Request $request){
        $Visitor = Visitor::select("id","name as text")->where("name","LIKE","%".$request->searchTerm."%")->get();
        return response()->json($Visitor);
    }
    public function getVisitorDtl(Request $request){
        $Visitor = Visitor::where("id","=",$request->id)->get();
        return response()->json($Visitor);
    }
    public function insertData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'visitor_id' => 'required',
            'card_id' => 'required',
            'pic' => 'required',
            'floor' => 'required',
            'purpose' => 'required',
        ]);
        if ($validator->passes()) {
            $VisitorActivity = new VisitorActivity;
            $VisitorActivity->pic = strtolower($request->pic);
            $VisitorActivity->floor = strtolower($request->floor);
            $VisitorActivity->purpose = strtolower($request->purpose);
            $VisitorActivity->card_id = intval($request->card_id);
            $VisitorActivity->cek_in = date("Y-m-d H:i:s");
            $VisitorActivity->created_by = intval($request->user_id);
            $VisitorActivity->visitor_id = intval($request->visitor_id);
            $VisitorActivity->save();
            return response()->json(['success'=>'Added new Activity.']);
        } else {
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }
    public function updateData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'visitor_id' => 'required',
            'card_id' => 'required',
            'pic' => 'required',
            'floor' => 'required',
            'purpose' => 'required',
        ]);
        if ($validator->passes()) {
            $VisitorActivity = VisitorActivity::where("id","=",$request->id)->first();
            $VisitorActivity->pic = strtolower($request->pic);
            $VisitorActivity->floor = strtolower($request->floor);
            $VisitorActivity->purpose = strtolower($request->purpose);
            $VisitorActivity->card_id = intval($request->card_id);
            $VisitorActivity->updated_by = intval($request->user_id);
            $VisitorActivity->updated_at = date("Y-m-d H:i:s");
            $VisitorActivity->visitor_id = intval($request->visitor_id);
            $visitor->save();
            return response()->json(['success'=>'Updated Activity.']);
        } else {
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }
    public function getDataDtl(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        if ($validator->passes()) {
            $VisitorActivity = VisitorActivity::select("visitor_activity.*","b.name as visitor_name")
            ->leftjoin("visitor as b", function($query) {
                $query->on("b.id","=","visitor_activity.visitor_id");
            })
            ->first();
            return response()->json($VisitorActivity);
            /* 
            $users = DoData::select("do_data.*", "b.name as reqUser")
        ->leftjoin("users as b", function($query) {
            $query->on("b.id","=","do_data.created_by");
        })
        ->when($typeSearch, function($query) use ($typeSearch, $itemSearch){
            if ($typeSearch == "id" && $itemSearch != "") {
                $query->where("do_data.id","=", floatval($itemSearch)); 
            }
            if ($typeSearch == "created_by" && $itemSearch != "") {
                $query->where("b.username","LIKE", "%".$itemSearch."%");
            }
        })
        ->whereIn("do_data.status", array(0,1,2))
        ->offset($start)
        ->limit($limit)
        ->orderBy('id', 'DESC')
        ->get();
            */
        } else {
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }
    public function getData(Request $request){
        /*  data tables */
        $itemSearch = isset($request->searchByName) ? $request->searchByName : "";
        $limit = isset($request->length) ? $request->length : 10;
        $visitor = VisitorActivity::select("visitor_activity.*", "b.name as visitor_name")
        ->leftjoin("visitor as b", function($query) use ($itemSearch){
            $query->on("b.id","=","visitor_activity.visitor_id");
        })
        ->where("b.name","LIKE", "%".$itemSearch."%")
       // ->whereIn("do_data.status", array(0,1,2))
        ->offset($request->start)
        ->limit($limit)
        ->orderBy('id', 'DESC')
        ->get();
        #dd($visitor);
        $data = array();
        $us = VisitorActivity::all();
        $total_records = count($us);
        $data['draw'] = intval( $request->draw );
        $data['recordsTotal'] = count($us);
        $data['recordsFiltered'] = count($us);
        $data['data'] = $visitor;
    	return response()->json($data);
    }
}
