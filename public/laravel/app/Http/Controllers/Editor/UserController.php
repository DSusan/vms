<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserPrivilege;
use App\Models\RolePrivilege;
use App\Models\Module;
use App\Models\Action;
use App\Models\UserLevel;
use Auth;
use DB;
use Session;
use Validator;
use App\Models\Visitor;
use App\Models\VisitorActivity;
use GuzzleHttp\Client;
use App\Models\VisitorPre;
use App\Models\HardwareInfo;
use App\Models\HardwareType;

class UserController extends Controller
{
	public function dashboard(Request $request)
	{
        $totalpre = 0;
        $totalVis = 0;
		#$din = array();
		$date = date("Y-m-d");
		// preregister 
		$pre = VisitorPre::whereBetween('created_at', [$date." 00:00:01", $date." 23:59:59"])->get();
        // cek yang in dl
        $in = VisitorActivity::select("visitor_activity.*","b.name as names", "b.company")
        ->join("visitor as b", function($query){
            $query->on("b.id","=","visitor_activity.visitor_id");
		})
		->whereBetween('visitor_activity.cek_in', [$date." 00:00:01", $date." 23:59:59"])
		->WhereNull('visitor_activity.cek_out')
        ->orderBy('visitor_activity.id', 'DESC')
		->get();
		$out = VisitorActivity::select("visitor_activity.*","b.name as names", "b.company")
        ->join("visitor as b", function($query){
            $query->on("b.id","=","visitor_activity.visitor_id");
		})
		->whereBetween('visitor_activity.cek_in', [$date." 00:00:01", $date." 23:59:59"])
		->whereNotNull('visitor_activity.cek_out')
        ->orderBy('visitor_activity.id', 'DESC')
		->get();
		$all = VisitorActivity::select("visitor_activity.*","b.name as names", "b.company")
        ->join("visitor as b", function($query){
            $query->on("b.id","=","visitor_activity.visitor_id");
		})
		->whereBetween('visitor_activity.cek_in', [$date." 00:00:01", $date." 23:59:59"])
        ->orderBy('visitor_activity.id', 'DESC')
		->get();
		$dal = array();
		foreach($all as $key => $val){
			$c1 = explode(" ", $val->cek_in);
			$c2 = substr($c1[1],0,5); 
			//$cs = $c2[0];
			if($val->cek_out){
				$o1 = explode(" ", $val->cek_out);
				$o2 = substr($o1[1],0,5); 
				//$os = $o2;
				
			} else {
				$o2 = NULL;
			}
			$dal[] = array(
				"cek_in" => $c2,
				"cek_out" => $o2,
				"names" => $val->names,
			);
		}
		#dd($dal);
		$totalpre = count($pre);
		$totalin = count($in);
		$totalout = count($out);
		$totalVis = $totalin + $totalout + $totalpre;
		#dd($dal);
		//cek alat
		$HardwareInfo = HardwareInfo::select("hardware_info.*","a.name as Hname")
        ->join("hardware_type as a", "a.id", "=", "hardware_info.id_hardware_type")
		->get();
		$alat = array();
		foreach ($HardwareInfo as $key => $value) {
			$alat[] = array(
				"nama_alat" => $value->Hname,
				"aktif" => $value->aktif,
			);
		}
		
		return view ('editor.dashboard.index', compact('totalin','totalout','in', 'totalpre','totalVis','in','out','all','dal','alat'));
	}
	public function index()
	{
		$number = 1;

		$users = DB::table('users')
		->join('roles_divisions', 'roles_divisions.id', '=', 'users.id_roles_divisions')
		->join('roles', 'roles.id', '=', 'roles_divisions.id_roles')
		->join('divisions', 'divisions.id', '=', 'roles_divisions.id_divisions')
		->select('users.id AS userId', 'users.username AS username', 'users.password AS password', 'users.id_roles_divisions AS userRoleDivId', 'roles_divisions.id_roles AS userRoleId', 'roles_divisions.id_divisions AS userDivId', 'roles.name AS roleName', 'divisions.name AS divName')
		->whereNull('users.deleted_at')
		->orderBy('users.username', 'ASC')
		->get();

		$roleDivsPriv = DB::table('roles_privileges')
		->select('id_roles_divisions')
		->whereNull('deleted_at')
		->groupBy('id_roles_divisions')
		->get();		

		foreach ($roleDivsPriv as $key => $value) 
		{
			$roleHasPriv []= $value->id_roles_divisions;
		}

		//Only select roles_divisions that have privilege
		$roleDivsList = DB::table('roles_divisions')
		->join('roles', 'roles.id', '=', 'roles_divisions.id_roles')
		->join('divisions', 'divisions.id', '=', 'roles_divisions.id_divisions')
		->select('roles.name AS roleName', 'divisions.name AS divName', 'roles_divisions.id AS id', 'roles.id AS roleId', 'divisions.id AS divId')
		->whereNull('roles_divisions.deleted_at')
		->whereIn('roles_divisions.id', $roleHasPriv)
		->orderBy('roles.name', 'ASC')
		->get();

		return view ('editor.user.index', compact('users', 'number', 'roleDivsList'));
	}

	public function store(Request $request)
	{
		//Add user
		$user = new User;
		$user->id_roles_divisions = $request->input('userRoleAdd');
		$user->username = $request->input('username');
		$user->password = bcrypt($request->input('password'));
		$user->created_by = session('nameUser');
		$user->save();

		//Add user privilege based on position
		$roleDivPriv = DB::table('roles_privileges')
		->select('id_modules', 'id_actions')
		->where('id_roles_divisions', $request->input('userRoleAdd'))
		->get();

		foreach ($roleDivPriv as $key => $valDivPriv) 
		{
			$addUserRole = new UserPrivilege;
			$addUserRole->id_users = $user->id;
			$addUserRole->id_modules = $valDivPriv->id_modules;
			$addUserRole->id_actions = $valDivPriv->id_actions;
			$addUserRole->created_by = session('nameUser');
			$addUserRole->save();
		}

		return redirect()->action('Editor\UserController@index');
	}

	public function update($id, Request $request)
	{
		// Change user position
		if($request->input('userRoleEdit') != $request->input('userRoleEditOld'))
		{			
			$user = User::find($id);
			$user->id_roles_divisions = $request->input('userRoleEdit');
			$user->save();

			//Delete existing user privilege
			$delUserPrivilege = DB::delete('DELETE FROM users_privileges where id_users = ?', [$id]);

			//Edit user privilege based on new position
			$roleDivPriv = DB::table('roles_privileges')
			->select('id_modules', 'id_actions')
			->where('id_roles_divisions', $request->input('userRoleEdit'))
			->get();

			foreach ($roleDivPriv as $key => $valDivPriv) 
			{
				$editUserRole = new UserPrivilege;
				$editUserRole->id_users = $id;
				$editUserRole->id_modules = $valDivPriv->id_modules;
				$editUserRole->id_actions = $valDivPriv->id_actions;
				$editUserRole->updated_by = session('nameUser');
				$editUserRole->save();
			}
		}

		return redirect()->action('Editor\UserController@index');
	}

	public function delete($id)
	{
		User::find($id)->delete();
		
		return redirect()->action('Editor\UserController@index');
	}

	public function editPrivilege($id)
	{
		$user = User::find($id);
		$moduleList = Module::pluck('name', 'id');
		$actionList = Action::pluck('name', 'id');
		
		return view ('editor.user.form', compact('user', 'moduleList', 'actionList'));
	}

	public function updatePrivilege($id, Request $request)
	{
		// dd('kesini');
		if($request->input('privilege'))
		{
			$delUserPrivilege = DB::delete('DELETE FROM users_privileges where id_users = ?', [$id]);
			foreach($request->input('privilege') as $moduleId => $actionList)
			{
				foreach($actionList as $actionId => $value)
				{
					$privilege = new UserPrivilege;
					$privilege->id_users = $id;
					$privilege->id_modules = $moduleId;
					$privilege->id_actions = $actionId;
					$privilege->created_by = session('nameUser');
					$privilege->updated_by = session('nameUser');
					$privilege->save();
				}
			}
		}

		return redirect()->action('Editor\UserController@index');
	}
}
