<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Division;

class DivisionController extends Controller
{
	// public function index()
	// {
	// 	$number = 1;
	// 	$divisions = Division::all();

	// 	return view ('editor.division.index', compact('divisions','number'));
	// }

	public function store(Request $request)
	{
		$division = new Division;
		$division->name = $request->input('division');
		$division->desc = $request->input('desc');
		$division->created_by = session('nameUser');
		$division->save();

		// return redirect()->action('Editor\DivisionController@index');
		return redirect()->action('Editor\RoleDivController@index');
	}

	public function update($id, Request $request)
	{
		$division = Division::find($id);
		$division->name = $request->input('divisionEdit');
		$division->desc = $request->input('descEdit');
		$division->updated_by = session('nameUser');
		$division->save();

		// return redirect()->action('Editor\DivisionController@index');
		return redirect()->action('Editor\RoleDivController@index');
	}

	public function delete($id)
	{
		Division::find($id)->delete();
		
		// return redirect()->action('Editor\DivisionController@index');
		return redirect()->action('Editor\RoleDivController@index');
	}
}
