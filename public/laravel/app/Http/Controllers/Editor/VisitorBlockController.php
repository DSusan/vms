<?php

namespace App\Http\Controllers\Editor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Auth;
use DB;
use Session;
use Validator;
use App\Models\Visitor;
use App\Models\VisitorActivity;

class VisitorBlockController extends Controller
{
    public function index()
    {
        return view ('editor.visitorblock.index');
    }
    
    public function getData(Request $request)
    {
        $searchByTipe = isset($request->ssec) ? $request->ssec : "";
        $itemSearch = isset($request->searchByName) ? $request->searchByName : "";
        $limit = isset($request->length) ? $request->length : 10;
        $data = array();
        $Visitor = Visitor::where("blok","=",1)->offset($request->start)
        ->limit($limit)
        ->get();
       # dd($VisitorPre);
        $us = Visitor::where("blok","=",1)->get();
        $total_records = count($us);
        $data['draw'] = intval( $request->draw );
        $data['recordsTotal'] = count($us);
        $data['recordsFiltered'] = count($us);
        $data['data'] = $Visitor;
        return response()->json($data);
    }
    public function getDatac(Request $request)
    {
        $searchTerm = $request->searchTerm;
        $data = array();
        $Visitor = Visitor::where("name","LIKE","%".$searchTerm."%")->where("blok", "!=", 1)->get();
		
        foreach ($Visitor as $value){
            $data[] = array(
                "id" => $value->id,
                "text" => $value->name
            );
        }
		
		return response()->json($data);
    }
    
    public function getDataDtl(Request $request)
    {
        $itemSearch = $request->itemSearch;
        $Visitor = Visitor::select("visitor.*","CekIn", "cek_in","cek_out", "CardId")
        ->leftjoin(DB::raw("(select visitor_id, max(card_id) as CardId, max(id) as CekIn, max(cek_in) as cek_in, max(cek_out) as cek_out from visitor_activity group by visitor_id) as b"), 
        function($query) use ($itemSearch){
            $query->on("b.visitor_id","=","visitor.id");
        })
        ->where("visitor.id","=",$request->visitor_id)
        ->get();
        $data = array();
        $data['Visitor'] = $Visitor;
        return response()->json($data);
    }

    public function blockThis(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'visitor_id'=> 'required'
        ]);
        if ($validator->passes()) {

            $Visitor = Visitor::where("id","=",$request->visitor_id)->first();
            $Visitor->blok = 1;
            $Visitor->blok_at = date("Y-m-d H:i:s");
            $Visitor->blok_by = $request->user_id;
            $Visitor->save();
            return response()->json(['success'=>1, "msg"=>"Data Updated"]);

        } else {

            return response()->json(['error'=>$validator->errors()->all()]);

        }
    }
    public function UnBlockThis(Request $request){


    }
}
