<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\RoleDivision;
use App\Models\Role;
use App\Models\Division;
use App\Models\Module;
use App\Models\Action;
use App\Models\RolePrivilege;
use DB;

class RoleDivController extends Controller
{
	public function index()
	{
		$number = 1;
		$roleList = Role::pluck('name', 'id');

		$divList = Division::pluck('name', 'id');

		$parentLists = RoleDivision::all();

		$roleDivsList = RoleDivision::select('roles.name AS roleName', 'divisions.name AS divName', 'roles.id AS roleId', 'divisions.id AS divId', 'roles_divisions.*')
		->join('roles', 'roles.id', '=', 'roles_divisions.id_roles')
		->join('divisions', 'divisions.id', '=', 'roles_divisions.id_divisions')
		->get();
		
		$parentPosition = [];

		foreach ($roleDivsList as $key => $roleDivList) 
		{
			$parentPosition [$roleDivList->id] = RoleDivision::find($roleDivList->id_roles_divisions_parent);
		}

		//ROLE
		$numberRole = 1;
		$roles = Role::all();

		//DIVISION
		$numberDiv = 1;
		$divisions = Division::all();

		return view ('editor.rolediv.index', compact('number', 'roleDivsList', 'roleList', 'divList', 'parentLists','parentPosition','numberRole','roles','numberDiv','divisions'));
	}

	public function store(Request $request)
	{
		//Role name
		$roleName = Role::find($request->input('id_roles'));

		//Division name
		$divName = Division::find($request->input('id_divisions'));

		//Role Division name
		$roleDivName = $roleName->name." ".$divName->name;

		$roleDiv = new RoleDivision;
		$roleDiv->id_roles_divisions_parent = $request->input('id_roles_divisions_parent');
		$roleDiv->id_roles = $request->input('id_roles');
		$roleDiv->id_divisions = $request->input('id_divisions');
		$roleDiv->name = $roleDivName;
		$roleDiv->created_by = session('nameUser');
		$roleDiv->save();

		return redirect()->action('Editor\RoleDivController@index');
	}

	public function update($id, Request $request)
	{
		//Role name
		$roleName = Role::find($request->input('id_roles'));

		//Division name
		$divName = Division::find($request->input('id_divisions'));

		//Role Division name
		$roleDivName = $roleName->name." ".$divName->name;

		$roleDiv = RoleDivision::find($id);
		$roleDiv->id_roles_divisions_parent = $request->input('id_roles_divisions_parent');
		$roleDiv->id_roles = $request->input('id_roles');
		$roleDiv->id_divisions = $request->input('id_divisions');
		$roleDiv->name = $roleDivName;
		$roleDiv->updated_by = session('nameUser');
		$roleDiv->save();

		return redirect()->action('Editor\RoleDivController@index');
	}

	public function delete($id)
	{
		RoleDivision::find($id)->delete();
		
		return redirect()->action('Editor\RoleDivController@index');
	}

	public function editPrivilege($id)
	{
		$rolePriv = RoleDivision::find($id);
		$roleDiv = DB::table('roles_divisions')
		->join('roles', 'roles.id', '=', 'roles_divisions.id_roles')
		->join('divisions', 'divisions.id', '=', 'roles_divisions.id_divisions')
		->select('roles.name AS roleName', 'divisions.name AS divName', 'roles_divisions.id AS id', 'roles.id AS roleId', 'divisions.id AS divId')
		->whereNull('roles_divisions.deleted_at')
		->where('roles_divisions.id', $id)
		->first();

		$moduleList = Module::pluck('name', 'id');
		$actionList = Action::pluck('name', 'id');
		return view ('editor.rprivilege.form', compact('roleDiv', 'rolePriv', 'moduleList', 'actionList'));
	}

	public function updatePrivilege($id, Request $request)
	{
		if($request->input('privilege'))
		{
			$delRolePrivilege = DB::delete('DELETE FROM roles_privileges where id_roles_divisions = ?', [$id]);
			foreach($request->input('privilege') as $moduleId => $actionList)
			{
				foreach($actionList as $actionId => $value)
				{
					$rolePrivilege = new RolePrivilege;
					$rolePrivilege->id_roles_divisions = $id;
					$rolePrivilege->id_modules = $moduleId;
					$rolePrivilege->id_actions = $actionId;
					$rolePrivilege->created_by = session('nameUser');
					$rolePrivilege->updated_by = session('nameUser');
					$rolePrivilege->save();
				}
			}
		}

		return redirect()->action('Editor\RoleDivController@index');
	}
}
