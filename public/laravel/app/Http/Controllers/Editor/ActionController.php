<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Action;

class ActionController extends Controller
{
	public function index()
	{
		$number = 1;
		$actions = Action::all();

		return view ('editor.action.index', compact('actions','number'));
	}

	public function store(Request $request)
	{
		$action = new Action;
		$action->name = strtolower($request->input('action'));
		$action->desc = $request->input('desc');
		$action->created_by = session('nameUser');
		$action->save();

		return redirect()->action('Editor\ActionController@index');
	}

	public function update($id, Request $request)
	{
		$action = Action::find($id);
		$action->name = strtolower($request->input('actionEdit'));
		$action->desc = $request->input('descEdit');
		$action->updated_by = session('nameUser');
		$action->save();

		return redirect()->action('Editor\ActionController@index');
	}

	public function delete($id)
	{
		Action::find($id)->delete();
		
		return redirect()->action('Editor\ActionController@index');
	}
}
