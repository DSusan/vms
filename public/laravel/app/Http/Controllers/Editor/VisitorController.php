<?php

namespace App\Http\Controllers\Editor;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Auth;
use DB;
use Session;
use Validator;
use App\Models\Visitor;
use App\Models\VisitorActivity;
use App\Models\HardwareInfo;
use App\Models\HardwareType;
use GuzzleHttp\Client;
use App\Exports\visitorExport;
use Maatwebsite\Excel\Facades\Excel;
use SoapClient;
use SoapVar;
use SimpleXMLElement;
use simplexml_load_string;

class VisitorController extends Controller
{

    public function getFloor(Request $request)
    {
        $HardwareInfo = HardwareInfo::all();
        foreach ($HardwareInfo as $value){
            $data[] = array(
                "id" => $value->floor,
                "text" => $value->name." / ".$value->floor
            );
        }
        //$data['floor'] = $HardwareInfo;
        return response()->json($data);

    }
    public function uploadsxxx(Request $request)
    {
        /* $file = $request->file('webcam')->getPathName();
        dd($file);
        $extension =  "png";
        $dir = public_path().'/uploaded_files/';
        $unikName = uniqid().'_'.time().'_'.date('Ymd');
        $filename = $unikName.'.'.$extension;
        $file->move($dir, $filename);
        return $file; */
        $file = $request->file('webcam');
        $file->move($tujuan_upload,$file->getClientOriginalName());
        return "asd";
    }
    public function cek_alat()
    {
        $HardwareInfo = HardwareInfo::select("hardware_info.*","a.name as Hname")
        ->join("hardware_type as a", "a.id", "=", "hardware_info.id_hardware_type")
        ->get();
        $tid = count($HardwareInfo);
        $result = 0;
        foreach ($HardwareInfo as $key => $value) {
            // apa bisa konek
            if($value->Hname == "entrypass"){
                $result += 1;
            } elseif($value->Hname == "falco"){

            } elseif($value->Hname == "matrix"){
                $IP = $value->list_ip;
              #  http://192.168.50.50/device.cgi/command?action=getusercount&format=xml
                $client = new \GuzzleHttp\Client();
                $res = $client->request('GET', 'http://'.$IP.'/device.cgi/command?action=getusercount&format=xml', [
                    'auth' => ['admin', '1234']
                ]);
                $response = $res->getBody()->getContents();
                $xml=simplexml_load_string($response);
                foreach($xml as $data)
                {
                    $datax = floatval($data);
                }
                if($datax == 0){
                    // berhasil konek
                    $result += 1;
                } else {
                    // gagal konek ke alat
                    $result += 0;
                }
            }
        }
        $h = 0;
        if($tid == $result) {
            $h = 1;
        } else {
            $h = 0;
        }
        return $result;
    }
    public function export_excel()
	{
		return Excel::download(new VisitorExportController, 'visitorExport.xlsx');
	}
    public function fc()
    {
        return view('editor.visitorfc.index');
    }
    public function block(){
        return view('editor.visitorblock.index');
    }
    public function preregister(){
        return view('editor.visitorpreregister.index');
    }
    public function tes(Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'http://192.168.50.50/device.cgi/users?action=get&user-id=17&format=xml', [
            'auth' => ['admin', '1234']
        ]);
        $response = $res->getBody()->getContents();
        #$xml=simplexml_load_string($response);
        return response()->json($response);
        // $xml = simplexml_load_string($response);
        //         foreach($xml as $data)
        //         {
        //             echo $data->name;
        //         }
    }
    public function index()
    {  
    	return view("editor.visitor.index");
    }
    public function getData(Request $request)
    {
        $searchByTipe = isset($request->ssec) ? $request->ssec : "";
        $itemSearch = isset($request->searchByName) ? $request->searchByName : "";
        $limit = isset($request->length) ? $request->length : 10;
        $data = array();
        $que1 = "select a.*, (select e.id from visitor_activity e where e.visitor_id = a.id order by e.id desc LIMIT 1) as CekIn, (select b.cek_in from visitor_activity b where b.visitor_id = a.id order by b.id desc LIMIT 1) as cek_in, (select c.cek_out from visitor_activity c where c.visitor_id = a.id order by c.id desc LIMIT 1) as cek_out, a.card_id as CardId from visitor a where a.blok != 1 ";
        if($searchByTipe == "byName"){
            $que1 .= "and a.name LIKE '%".$itemSearch."%' ";
        } elseif($searchByTipe == "byCard") {
            #$query->where("CardId", "=", $itemSearch);
            $que1 .= "and a.card_id = '".$itemSearch."' ";
        }
        $que1 .= "order by a.id desc limit ".$limit." offset ".$request->start;

        $visitor = DB::select($que1);
        #dd($visitor);
        /* $visitor = Visitor::select("visitor.*","CekIn", "cek_in","cek_out", "CardId")
        ->leftjoin(DB::raw("(select visitor_id, max(card_id) as CardId, max(id) as CekIn, max(cek_in) as cek_in, max(cek_out) as cek_out from visitor_activity group by visitor_id) as b"), 
        function($query) use ($itemSearch){
            $query->on("b.visitor_id","=","visitor.id");
        })
        ->where(function($query) use ($itemSearch, $searchByTipe){
            if($searchByTipe == "byName"){
                $query->where("visitor.name", "LIKE", "%".$itemSearch."%");
            } elseif($searchByTipe == "byCard") {
                $query->where("CardId", "=", $itemSearch);
            }
        })
        ->where("visitor.blok","!=", 1)
        ->offset($request->start)
        ->limit($limit)
        ->groupBy('visitor.id')
        ->orderBy('visitor.id', 'DESC')
        ->toSql();
        dd($visitor);*/
        $que2 = "select a.*, (select e.id from visitor_activity e where e.visitor_id = a.id order by e.id desc LIMIT 1) as CekIn, (select b.cek_in from visitor_activity b where b.visitor_id = a.id order by b.id desc LIMIT 1) as cek_in, (select c.cek_out from visitor_activity c where c.visitor_id = a.id order by c.id desc LIMIT 1) as cek_out, (select d.card_id from visitor_activity d where d.visitor_id = a.id order by d.id desc LIMIT 1) as CardId from visitor a where a.blok != 1 ";
        if($searchByTipe == "byName"){
            $que2 .= "and a.name LIKE '%".$itemSearch."%' ";
        } elseif($searchByTipe == "byCard") {
            #$query->where("CardId", "=", $itemSearch);
            $que2 .= "and a.card_id = '".$itemSearch."' ";
        }
        $us = DB::select($que2);
        $total_records = count($us);
        $data['draw'] = intval( $request->draw );
        $data['recordsTotal'] = count($us);
        $data['recordsFiltered'] = count($us);
        $data['data'] = $visitor;
        return response()->json($data);
    }
    public function cekoutData(Request $request)
    {

        $HardwareInfo = HardwareInfo::select("hardware_info.*","a.name as Hname")
        ->join("hardware_type as a", "a.id", "=", "hardware_info.id_hardware_type")
        ->where("hardware_info.aktif", "=", 1)
        ->get();
        $tid = count($HardwareInfo);
        if($tid > 0){
            $VisitorActivity = VisitorActivity::where("id","=",$request->visitor_activity_id)->first();
            $visitor_id = $VisitorActivity->visitor_id;
            $VisitorActivity->cek_out = date("Y-m-d H:i:s");
            $VisitorActivity->save();
            
            $VA = DB::table('visitor_activity')
            ->select('visitor_id','card_id')
            ->where("id","=",$request->visitor_activity_id)
            ->get();
        # $VisitorActivity = VisitorActivity::where("id","=",$request->visitor_activity_id)->get();
            foreach($VA as $key => $val){
                $Visitor = Visitor::where("id","=",$val->visitor_id)->first();
                $Visitor->card_id = "";
                $Visitor->save();
                $CARD_ID = $val->card_id;
                $LID = $request->visitor_id;
            }
           
            // keluarin dari controller
            foreach ($HardwareInfo as $key => $value) {
                $IP = $value->list_ip;
                $FOLDER = $value->folder;
                if($value->Hname == "entrypass"){
                    /* START ENTRY PASS */
                    $jdl = "P1Imp_".$request->card_id;
                    $content = "";
                    $content .= $LID.",";
                    $content .= ",";
                    $content .= strtolower($request->card_id).",";
                    $content .= ",";
                    $content .= ",";
                    $content .= intval(0).",";
                    $content .= '"No Access",';
                    $content .= intval(0).",";
                    $content .= intval(1).",";
                    $content .= intval(0).",";
                    $content .= intval(0).",";
                    $content .= intval(1).",";
                    $content .= "\r\n";
                    $fp = fopen($FOLDER.$jdl.".txt","wb");
                    fwrite($fp,$content);
                    fclose($fp);
                    /* END ENTRY PASS */
                } elseif($value->Hname == "falco"){
                    /* START FALCO */
                    try {
                    # $soapclient = new SoapClient('http://127.0.0.1:81/Vaultpro/VaultWebAPI.asmx?wsdl');
                        $data = '<?xml version="1.0" encoding="utf-8"?>
                        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                          <soap:Body>
                            <DeleteCard xmlns="VaultWebAPI">
                              <CardNo>'.$CARD_ID.'</CardNo>
                              <DeleteFromDevice>1</DeleteFromDevice>
                            </DeleteCard>
                          </soap:Body>
                        </soap:Envelope>';
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, "http://".$IP."/Vaultpro/VaultWebAPI.asmx");
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                        curl_setopt($ch, CURLOPT_HEADER, FALSE);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml; charset=utf-8", "SOAPAction: VaultWebAPI/DeleteCard", "Content-Length: " . strlen($data)));
                        $output = curl_exec($ch);
                        curl_close($ch);
                        // converting
                        $response1 = str_replace("<soap:Body>","",$output);
                        $response2 = str_replace("</soap:Body>","",$response1);
                        // convertingc to XML
                        $parser = simplexml_load_string(str_replace('xmlns="VaultWebAPI"','',$response2));
                            #   $x = (string)$parser->AddCardResponse[0]->AddCardResult[0]->ErrCode[0];
                        function object2array($object) { return @json_decode(@json_encode($object),1); }
                        $xml_array=object2array($parser);
                     #   dd($xml_array);
                        $x = $xml_array['DeleteCardResponse']['DeleteCardResult']['ErrCode'];
                    # $x2 = explode(" ",$x);
                    //return response()->json(['error'=>$x]);
                    } catch(Exception $e){
                        echo $e->getMessage();
                    }
                    /* END FALOC */
                } elseif($value->Hname == "matrix"){
                   
                     /* START MATRIX */
                    $url = "http://".$IP."/device.cgi/users?action=set&user-id=".$visitor_id."&card1=0&format=xml";
                    $client = new \GuzzleHttp\Client();
                    $res = $client->request('GET', $url, [
                        'auth' => ['admin', '1234']
                    ]);
                    $response = $res->getBody()->getContents();
                    $xml=simplexml_load_string($response);
                    /* END MATRIX */
                }
                return response()->json(['success'=>1, "msg"=>"Data Updated"]);
            }
        } else {
            $validator->getMessageBag()->add('Controller', 'No Controller Connected');
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }   
    public function cekinData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'visitor_id'=> 'required',
            'card_id' => 'required',
            'user_id' => 'required',
            'pic' => 'required',
            'floor' => 'required',
            'purpose' => 'required',
        ]);
        if ($validator->passes()) {
            $v1 = visitor::where("card_id","=",$request->card_id)->get();
            if(count($v1) == 0){
                $HardwareInfo = HardwareInfo::select("hardware_info.*","a.name as Hname")
                ->join("hardware_type as a", "a.id", "=", "hardware_info.id_hardware_type")
                ->where("hardware_info.aktif", "=", 1)
                ->get();
                $tid = count($HardwareInfo);
                if($tid > 0){
                    $VisitorActivity = new VisitorActivity;
                    $VisitorActivity->pic = strtolower($request->pic);
                    $VisitorActivity->floor = strtolower($request->floor);
                    $VisitorActivity->purpose = strtolower($request->purpose);
                    $VisitorActivity->card_id = strtolower($request->card_id);
                    $VisitorActivity->cek_in = date("Y-m-d H:i:s");
                    $VisitorActivity->created_by = intval($request->user_id);
                    $VisitorActivity->visitor_id = $request->visitor_id;
                    $VisitorActivity->save();
    
                    $Visitor = Visitor::where("id","=",$request->visitor_id)->first();
                    $Visitor->card_id = strtolower($request->card_id);
                    $Visitor->save();
                    $LID = $request->visitor_id;
                    // masukin ke controller
                    foreach ($HardwareInfo as $key => $value) {
                        $IP = $value->list_ip;
                        $FOLDER = $value->folder;
                        if($value->Hname == "entrypass"){
                            /* START ENTRY PASS */
                            $jdl = "P1Imp_".$request->card_id;
                            $content = "";
                            $content .= $LID.",";
                            $content .= $LID.",";
                            $content .= strtolower($request->card_id).",";
                            $content .= "VISITOR,";
                            $content .= date("d/m/Y").",";
                            $content .= "1,";
                            $content .= '"THANODOOR",';
                            $content .= "1,";
                            $content .= "1,";
                            $content .= "1,";
                            $content .= "1,";
                            $content .= "1";
                            $content .= "\r\n";
                            $fp = fopen($FOLDER.$jdl.".txt","wb");
                            fwrite($fp,$content);
                            fclose($fp);
                            /* END ENTRY PASS */
                        } elseif($value->Hname == "falco"){
                            /* START FALCO */
                            $IP = $value->list_ip;
                            try {
                            # $soapclient = new SoapClient('http://127.0.0.1:81/Vaultpro/VaultWebAPI.asmx?wsdl');
                                $data = '<?xml version="1.0" encoding="utf-8"?>
                                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                                <soap:Body>
                                    <AddCard xmlns="VaultWebAPI">
                                    <CardProfile>
                                        <CardNo>'.strtolower($request->card_id).'</CardNo>
                                        <Name>'.$LID.'</Name>
                                        <CardPinNo>'.strtolower($request->card_id).'</CardPinNo>
                                        <CardType>Normal Card</CardType>
                                        <Department>thano</Department>
                                        <Company>thano</Company>
                                        <Gentle>string</Gentle>
                                        <AccessLevel>1</AccessLevel>
                                        <LiftAccessLevel>00</LiftAccessLevel>
                                        <BypassAP>1</BypassAP>
                                        <ActiveStatus>1</ActiveStatus>
                                        <NonExpired>1</NonExpired>
                                        <ExpiredDate></ExpiredDate>
                                        <VehicleNo></VehicleNo>
                                        <FloorNo></FloorNo>
                                        <UnitNo></UnitNo>
                                        <ParkingNo></ParkingNo>
                                        <StaffNo>'.$LID.'</StaffNo>
                                        <Title></Title>
                                        <Position></Position>
                                        <NRIC></NRIC>
                                        <Passport></Passport>
                                        <Race></Race>
                                        <DOB></DOB>
                                        <JoiningDate></JoiningDate>
                                        <ResignDate></ResignDate>
                                        <Address1></Address1>
                                        <Address2></Address2>
                                        <PostalCode></PostalCode>
                                        <City></City>
                                        <State></State>
                                        <Email></Email>
                                        <MobileNo></MobileNo>
                                        <Photo></Photo>
                                        <DownloadCard>1</DownloadCard>
                                    </CardProfile>
                                    </AddCard>
                                </soap:Body>
                                </soap:Envelope>';
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, "http://".$IP."/Vaultpro/VaultWebAPI.asmx");
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml; charset=utf-8", "SOAPAction: VaultWebAPI/AddCard", "Content-Length: " . strlen($data)));
                                $output = curl_exec($ch);
                                curl_close($ch);
                                // converting
                                $response1 = str_replace("<soap:Body>","",$output);
                                $response2 = str_replace("</soap:Body>","",$response1);

                                // convertingc to XML
                                $parser = simplexml_load_string(str_replace('xmlns="VaultWebAPI"','',$response2));
                            #   $x = (string)$parser->AddCardResponse[0]->AddCardResult[0]->ErrCode[0];
                                function object2array($object) { return @json_decode(@json_encode($object),1); }
                                $xml_array=object2array($parser);
                                $x = $xml_array['AddCardResponse']['AddCardResult']['ErrCode'];
                            # $x2 = explode(" ",$x);
                            //return response()->json(['error'=>$x]);
                            } catch(Exception $e){
                                echo $e->getMessage();
                            }
                            /* END FALOC */
                        } elseif($value->Hname == "matrix"){
                            $IP = $value->list_ip;
                            /* MATRIX */
                            $client = new \GuzzleHttp\Client();
                            $res = $client->request('GET', 'http://192.168.50.50/device.cgi/users?action=set&user-id='.$LID.'&ref-user-id='.$LID.'&name='.$LID.'&card1='.$request->card_id.'&user-active=1&vip=1&format=xml', [
                                'auth' => ['admin', '1234']
                            ]);
                            $response = $res->getBody()->getContents();
                            $xml=simplexml_load_string($response);
                            foreach($xml as $data)
                            {
                                $datax = floatval($data);
                            }
                            if($datax == 8 || $datax == 21) {
                                $validator->getMessageBag()->add('CardId', 'Card Id being Use');
                                return response()->json(['error'=>$validator->errors()->all()]);
                            } elseif($response == 0) {
                               // return response()->json(['success'=>1, "msg"=>"Data Updated"]);
                            }
                        }
                    }
                } else {
                    $validator->getMessageBag()->add('Controller', 'No Controller Connected');
                    return response()->json(['error'=>$validator->errors()->all()]);
                }
            } else {
                $validator->getMessageBag()->add('Card', 'Card Being Use');
                return response()->json(['error'=>$validator->errors()->all()]);
            }
            
            /* START ENTRY PASS
                $jdl = "P1Imp_".date("YmdHis");
                $content = "";
                $content .= $LID.",";
                $content .= $LID.",";
                $content .= strtolower($request->card_id).",";
                $content .= "THANO,";
                $content .= date("d/m/Y").",";
                $content .= "1,";
                $content .= "Turnstilles,";
                $content .= "1,";
                $content .= "1,";
                $content .= "1,";
                $content .= "1,";
                $content .= "1";
                $content .= "\r\n";
                $fp = fopen("D:/entrypassupload/".$jdl.".txt","wb");
                fwrite($fp,$content);
                fclose($fp);
                return response()->json(['success'=>1, "msg"=>"Data Updated"]);
            /* END ENTRY PASS */

            /* START MATRIX */
            /* $url = "http://192.168.50.50/device.cgi/users?action=set&user-id=".$request->visitor_id."&card1=".$request->card_id."&format=xml";
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $url, [
                'auth' => ['admin', '1234']
            ]);
            $response = $res->getBody()->getContents();
            $xml=simplexml_load_string($response); */
            /* END MATRIX */

            /* START FALCO */
                /* try {
                   # $soapclient = new SoapClient('http://127.0.0.1:81/Vaultpro/VaultWebAPI.asmx?wsdl');
                    $data = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Body>
                        <AddCard xmlns="VaultWebAPI">
                          <CardProfile>
                            <CardNo>'.strtolower($request->card_id).'</CardNo>
                            <Name>'.$LID.'</Name>
                            <CardPinNo>'.strtolower($request->card_id).'</CardPinNo>
                            <CardType>string</CardType>
                            <Department>string</Department>
                            <Company>string</Company>
                            <Gentle>string</Gentle>
                            <AccessLevel>1</AccessLevel>
                            <LiftAccessLevel>00</LiftAccessLevel>
                            <BypassAP>1</BypassAP>
                            <ActiveStatus>1</ActiveStatus>
                            <NonExpired>1</NonExpired>
                            <ExpiredDate></ExpiredDate>
                            <VehicleNo>string</VehicleNo>
                            <FloorNo>string</FloorNo>
                            <UnitNo>string</UnitNo>
                            <ParkingNo>string</ParkingNo>
                            <StaffNo>string</StaffNo>
                            <Title>string</Title>
                            <Position>string</Position>
                            <NRIC>string</NRIC>
                            <Passport>string</Passport>
                            <Race>string</Race>
                            <DOB></DOB>
                            <JoiningDate></JoiningDate>
                            <ResignDate></ResignDate>
                            <Address1>string</Address1>
                            <Address2>string</Address2>
                            <PostalCode>string</PostalCode>
                            <City>string</City>
                            <State>string</State>
                            <Email>string</Email>
                            <MobileNo>'.strtolower($request->mobile).'</MobileNo>
                            <Photo></Photo>
                            <DownloadCard>1</DownloadCard>
                          </CardProfile>
                        </AddCard>
                      </soap:Body>
                    </soap:Envelope>';
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1:81/Vaultpro/VaultWebAPI.asmx");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($ch, CURLOPT_HEADER, FALSE);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml; charset=utf-8", "SOAPAction: VaultWebAPI/AddCard", "Content-Length: " . strlen($data)));
                    $output = curl_exec($ch);
                    curl_close($ch);
                    // converting
                    $response1 = str_replace("<soap:Body>","",$output);
                    $response2 = str_replace("</soap:Body>","",$response1);

                    // convertingc to XML
                    $parser = simplexml_load_string(str_replace('xmlns="VaultWebAPI"','',$response2));
                 #   $x = (string)$parser->AddCardResponse[0]->AddCardResult[0]->ErrCode[0];
                    function object2array($object) { return @json_decode(@json_encode($object),1); }
                    $xml_array=object2array($parser);
                    $x = $xml_array['AddCardResponse']['AddCardResult']['ErrCode'];
                   # $x2 = explode(" ",$x);
                   //return response()->json(['error'=>$x]);
                } catch(Exception $e){
                    echo $e->getMessage();
                }
                */
                /* END FALCO */
            return response()->json(['success'=>1, "msg"=>"Data Updated"]);
        } else {
             return response()->json(['error'=>$validator->errors()->all()]);
        }
    }
    public function insertData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'company' => 'required',
            'mobile' => 'required',
            'vid' => 'required',
            'card_id' => 'required',
            //'user_id' => 'required',
            'pic' => 'required',
            'floor' => 'required',
            'purpose' => 'required',
        ]);

        if ($validator->passes()) {
          #  $dr = visitor::where("email","=",$request->email)->get();
           /* if (visitor::where('email', '=', $request->email)->exists()) {
                $validator->getMessageBag()->add('email', 'Email Already registerd');
                return response()->json(['error'=>$validator->errors()->all()]);
            } else {*/
            $v1 = visitor::where("card_id","=",$request->card_id)->get();
            if(count($v1) == 0){
                $HardwareInfo = HardwareInfo::select("hardware_info.*","a.name as Hname")
                ->join("hardware_type as a", "a.id", "=", "hardware_info.id_hardware_type")
                ->where("hardware_info.aktif", "=", 1)
                ->get();
                $tid = count($HardwareInfo);
                if($tid > 0){
                     # masuk ke visitor
                    $visitor = new visitor;
                    $visitor->name = strtolower($request->name);
                    $visitor->address = strtolower($request->address);
                    $visitor->company = strtolower($request->company);
                    $visitor->mobile = strtolower($request->mobile);
                    $visitor->card_id = strtolower($request->card_id);
                    $visitor->vid = strtolower($request->vid);
                    $visitor->save();
                    $LID = $visitor->id;
                    // masuk ke activity
                    $VisitorActivity = new VisitorActivity;
                    $VisitorActivity->pic = strtolower($request->pic);
                    $VisitorActivity->floor = strtolower($request->floor);
                    $VisitorActivity->purpose = strtolower($request->purpose);
                    $VisitorActivity->card_id = strtolower($request->card_id);
                    $VisitorActivity->cek_in = date("Y-m-d H:i:s");
                    $VisitorActivity->created_by = intval($request->user_id);
                    $VisitorActivity->visitor_id = $LID;
                    $VisitorActivity->save();
                    // masukin ke controller
                    foreach ($HardwareInfo as $key => $value) {
                        $IP = $value->list_ip;
                        $FOLDER = $value->folder;
                        if($value->Hname == "entrypass"){
                            $jdl = "P1Imp_".$request->card_id;
                            $content = "";
                            $content .= $LID.",";
                            $content .= $LID.",";
                            $content .= strtolower($request->card_id).",";
                            $content .= "VISITOR,";
                            $content .= date("d/m/Y").",";
                            $content .= "1,";
                            $content .= '"THANODOOR",';
                            $content .= "1,";
                            $content .= "1,";
                            $content .= "1,";
                            $content .= "1,";
                            $content .= "1";
                            $content .= "\r\n";
                            $fp = fopen($FOLDER.$jdl.".txt","wb");
                            fwrite($fp,$content);
                            fclose($fp);
                            /* END ENTRY PASS */
                        } elseif($value->Hname == "falco"){
                            /* START FALCO */
                            $IP = $value->list_ip;
                            try {
                            # $soapclient = new SoapClient('http://127.0.0.1:81/Vaultpro/VaultWebAPI.asmx?wsdl');
                                $data = '<?xml version="1.0" encoding="utf-8"?>
                                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                                <soap:Body>
                                    <AddCard xmlns="VaultWebAPI">
                                    <CardProfile>
                                        <CardNo>'.strtolower($request->card_id).'</CardNo>
                                        <Name>'.$LID.'</Name>
                                        <CardPinNo>'.strtolower($request->card_id).'</CardPinNo>
                                        <CardType>Normal Card</CardType>
                                        <Department>thano</Department>
                                        <Company>thano</Company>
                                        <Gentle>string</Gentle>
                                        <AccessLevel>1</AccessLevel>
                                        <LiftAccessLevel>00</LiftAccessLevel>
                                        <BypassAP>1</BypassAP>
                                        <ActiveStatus>1</ActiveStatus>
                                        <NonExpired>1</NonExpired>
                                        <ExpiredDate></ExpiredDate>
                                        <VehicleNo></VehicleNo>
                                        <FloorNo></FloorNo>
                                        <UnitNo></UnitNo>
                                        <ParkingNo></ParkingNo>
                                        <StaffNo>'.$LID.'</StaffNo>
                                        <Title></Title>
                                        <Position></Position>
                                        <NRIC></NRIC>
                                        <Passport></Passport>
                                        <Race></Race>
                                        <DOB></DOB>
                                        <JoiningDate></JoiningDate>
                                        <ResignDate></ResignDate>
                                        <Address1></Address1>
                                        <Address2></Address2>
                                        <PostalCode></PostalCode>
                                        <City></City>
                                        <State></State>
                                        <Email></Email>
                                        <MobileNo></MobileNo>
                                        <Photo></Photo>
                                        <DownloadCard>1</DownloadCard>
                                    </CardProfile>
                                    </AddCard>
                                </soap:Body>
                                </soap:Envelope>';
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, "http://".$IP."/Vaultpro/VaultWebAPI.asmx");
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml; charset=utf-8", "SOAPAction: VaultWebAPI/AddCard", "Content-Length: " . strlen($data)));
                                $output = curl_exec($ch);
                                curl_close($ch);
                                // converting
                                $response1 = str_replace("<soap:Body>","",$output);
                                $response2 = str_replace("</soap:Body>","",$response1);

                                // convertingc to XML
                                $parser = simplexml_load_string(str_replace('xmlns="VaultWebAPI"','',$response2));
                            #   $x = (string)$parser->AddCardResponse[0]->AddCardResult[0]->ErrCode[0];
                                function object2array($object) { return @json_decode(@json_encode($object),1); }
                                $xml_array=object2array($parser);
                                $x = $xml_array['AddCardResponse']['AddCardResult']['ErrCode'];
                            # $x2 = explode(" ",$x);
                            //return response()->json(['error'=>$x]);
                            } catch(Exception $e){
                                echo $e->getMessage();
                            }
                            /* END FALOC */
                        } elseif($value->Hname == "matrix"){
                            $IP = $value->list_ip;
                            /* MATRIX */
                            $client = new \GuzzleHttp\Client();
                            $res = $client->request('GET', 'http://192.168.50.50/device.cgi/users?action=set&user-id='.$LID.'&ref-user-id='.$LID.'&name='.$LID.'&card1='.$request->card_id.'&user-active=1&vip=1&format=xml', [
                                'auth' => ['admin', '1234']
                            ]);
                            $response = $res->getBody()->getContents();
                            $xml=simplexml_load_string($response);
                            foreach($xml as $data)
                            {
                                $datax = floatval($data);
                            }
                            if($datax == 8 || $datax == 21) {
                                $validator->getMessageBag()->add('CardId', 'Card Id being Use');
                                return response()->json(['error'=>$validator->errors()->all()]);
                            } elseif($response == 0) {
                               // return response()->json(['success'=>1, "msg"=>"Data Updated"]);
                            }
                        }
                    }
                    return response()->json(['success'=>1, "msg"=>"Data Updated"]);
                } else {
                    $validator->getMessageBag()->add('Controller', 'No Controller Connected');
                    return response()->json(['error'=>$validator->errors()->all()]);
                }
            } else {
                $validator->getMessageBag()->add('Card', 'Card Being Use');
                return response()->json(['error'=>$validator->errors()->all()]);
            }
            // return response()->json(['success'=>'Added new records.']);
                /* jaga2 
                $url = "http://192.168.50.50/device.cgi/users?action=get&user-id=17&format=xml";
                $username = "admin";
                $password = "1234";
                $context = stream_context_create(array(
                    'http' => array(
                        'header'  => "Authorization: Basic " . base64_encode("$username:$password")
                    )
                ));
                $data = file_get_contents($url, false, $context);
                $xml = simplexml_load_string($data);

                print_r($xml);
                echo "<hr /><br /><br />";

                echo $xml->{'user-id'};
                */
        } else {
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }
    public function getDataDtl(Request $request)
    {
        $visitor = visitor::where("id","=",$request->visitor_id)->get();
        $visitor_activity = VisitorActivity::where("id","=",$request->visitor_activity_id)->get();
        $data = array();
        $data['visitor'] = $visitor;
        $data['visitor_activity'] = $visitor_activity;
        return response()->json($data);
    }
    public function updateData(Request $request)
    {
        $visitor = visitor::where("id","=",$request->id)->first();
        $visitor->name = strtolower($request->name);
        $visitor->address = strtolower($request->address);
        $visitor->company = strtolower($request->company);
        $visitor->mobile = strtolower($request->mobile);
        $visitor->card_id = strtolower($request->card_id);
        $visitor->save();
        return response()->json(['success'=>1, "msg"=>"Data Updated"]);
    }
    public function delete(Request $request)
    {

    }
    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
        ]);

        if ($validator->passes()) {
            $visitor = visitor::find($request->id);
            $visitor->password = Hash::make($request->password);
            $visitor->save();
            return response()->json(['success'=>'data updated']);
        } else {
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }
}
