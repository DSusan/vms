<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Module;

class ModuleController extends Controller
{
	public function index()
	{
		$number = 1;
		$modules = Module::all();

		return view ('editor.module.index', compact('modules','number'));
	}

	public function store(Request $request)
	{
		$module = new Module;
		$module->name = strtolower($request->input('module'));
		$module->desc = $request->input('desc');
		$module->created_by = session('nameUser');
		$module->save();

		return redirect()->action('Editor\ModuleController@index');
	}

	public function update($id, Request $request)
	{
		$module = Module::find($id);
		$module->name = strtolower($request->input('moduleEdit'));
		$module->desc = $request->input('descEdit');
		$module->updated_by = session('nameUser');
		$module->save();

		return redirect()->action('Editor\ModuleController@index');
	}

	public function delete($id)
	{
		Module::find($id)->delete();
		
		return redirect()->action('Editor\ModuleController@index');
	}
}
