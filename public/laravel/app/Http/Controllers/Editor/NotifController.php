<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\NotifCustom;
use App\Models\User;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class NotifController extends Controller
{
	public function notifCek(Request $request)
	{
		$userId = $request->user_id;
		$notif = NotifCustom::select('id')
		->where('target', '=', $userId)
		->whereNull('read')
		->get();
		$countNotif = count($notif);

		return $countNotif;
	}

	public function notifCekDtl(Request $request)
	{	
		$userId = $request->user_id;
		$notif = DB::table('notification AS a')
		->leftjoin('users AS b', 'b.id', '=', 'a.from')
		->leftjoin('users AS c', 'c.id', '=', 'a.target')
		->where('target', '=', $userId)
		->whereNull('a.read')
		->select('a.id','a.date','b.username as name_from', 'c.username as name_target')
		->get();

		return view('notif.index', compact('notif'));
	}

	public function notifRead($id, Request $request)
	{
		// dd($id);
		$notifRead = NotifCustom::find($request->notifKeyId);
		$notifRead->read = date('Y-m-d H:i:s');
		$notifRead->save();
	}
}
