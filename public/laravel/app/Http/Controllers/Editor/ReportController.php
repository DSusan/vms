<?php

namespace App\Http\Controllers\Editor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Auth;
use DB;
use Session;
use Validator;
use App\Models\Visitor;
use App\Models\VisitorActivity;
use GuzzleHttp\Client;
use App\Exports\VisitorExport;
use Excel;

class ReportController extends Controller
{
    public function index()
    {
        return view("editor.report.index");
    }
    public function getData(Request $request)
    {
        $from = isset($request->from) ? $request->from : "";
        $to = isset($request->to) ? $request->to : "";
        $searchByTipe = isset($request->ssec) ? $request->ssec : "";
        $itemSearch = isset($request->searchByName) ? $request->searchByName : "";
        $limit = 1000000000;
        $data = array();
        $visitor = Visitor::select("visitor.*","CekIn", "cek_in","cek_out", "CardId")
        ->join(DB::raw("(select visitor_id, max(card_id) as CardId, max(id) as CekIn, max(cek_in) as cek_in, max(cek_out) as cek_out from visitor_activity group by visitor_id) as b"), 
        function($query) use ($from, $to){
            $query->on("b.visitor_id","=","visitor.id")
            ->where("b.cek_in",">=","2019-01-01 00:00:01")
            ->where("b.cek_in","<=","2020-12-01 23:59:59");
        })
        /* ->where(function($query) use ($from, $to){
            if($searchByTipe == "byName"){
                $query->where("visitor.name", "LIKE", "%".$itemSearch."%");
            } elseif($searchByTipe == "byCard") {
                $query->where("CardId", "=", $itemSearch);
            }
        }) */
        ->offset($request->start)
        ->limit($limit)
        ->groupBy('visitor.id')
        ->orderBy('visitor.id', 'DESC')
        ->get();
       # dd()
        $total_records = count($visitor);
        $data['draw'] = intval( $request->draw);
        $data['recordsTotal'] = count($visitor);
        $data['recordsFiltered'] = count($visitor);
        $data['data'] = $visitor;
        return response()->json($data);
    }
    public function export_query(Request $request) 
    {
        return Excel::download(new visitorExport($request->id), 'peserta_query.xlsx');
    }
}
