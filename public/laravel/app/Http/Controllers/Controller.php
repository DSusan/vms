<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Models\GpMeqVelocityType;
use App\Models\WellAnalysisCuttingType;
use App\Models\WellAnalysisLoggingType;
use App\Models\WellAnalysisCoringType;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function __construct()
	{
		$gpMeqVeloTypes = GpMeqVelocityType::select('id','name')->orderBy('name')->get();
		\View::share('globalGpMeqVeloTypes', $gpMeqVeloTypes);

		$wellAnalysisCuttingTypes = WellAnalysisCuttingType::select('id','name')->orderBy('id')->get();
		\View::share('globalWellAnalysisCuttingTypes', $wellAnalysisCuttingTypes);

		$wellAnalysisLoggingTypes = WellAnalysisLoggingType::select('id','name')->orderBy('id')->get();
		\View::share('globalWellAnalysisLoggingTypes', $wellAnalysisLoggingTypes);

		$wellAnalysisCoringTypes = WellAnalysisCoringType::select('id','name')->orderBy('id')->get();
		\View::share('globalWellAnalysisCoringTypes', $wellAnalysisCoringTypes);
	}
}
