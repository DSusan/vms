# -*- coding: utf-8 -*-
'''
Spyder Editor

This is a temporary script file.
'''
import json
import sys
from pyXSteam.XSteam import XSteam
from math import log10
steamTable = XSteam(XSteam.UNIT_SYSTEM_MKS)

def Dryness_Total_Flowrate(Data):
    Data['Total_Flowrate'] =  round(Data['Brine_Flowrate'] + Data['Steam_Flowrate'],2)
    Data['Dryness'] = round(Data['Steam_Flowrate'] / Data['Total_Flowrate'],2)
    return Data['Total_Flowrate'],Data['Dryness']

def Bara_pressure(Elevation,P_sampl_barg):
    Atm_Press_bara = 101325*(1-2.25577*10**-5*Elevation)**5.25588/10**5
    P_Sampl_bara = P_sampl_barg+Atm_Press_bara
    return P_Sampl_bara

def ResCond(Data):
    P_Sampl_bara = Bara_pressure(Data['Elevation'],Data['P_sampl_barg'])
    TD = steamTable.x_ph(P_Sampl_bara,Data['Enthalpy'])

    A = [  -3.5532, 1.46E-01, -4.93E-04, 1.23E-06, -4.94E-10]
    B = [-4.22E+01, 2.88E-01, -3.67E-04, 3.17E-07,  7.70E+01]

    SiO2_TD = (1-steamTable.x_ph(P_Sampl_bara,Data['Enthalpy']))*Data['Brine']['SiO2']
    TQtz = SiO2_TD

    for i in range (0,100):
        TQtz1 = B[0]+(TQtz*B[1])+(TQtz**2*B[2])+(TQtz**3*B[3])+(log10(TQtz)*B[4])
        Enthalp = steamTable.hL_t(TQtz1)
        water_fraction = 1-((Data['Enthalpy']-steamTable.hL_t(TQtz1))/\
                          (steamTable.hV_t(TQtz1)-steamTable.hL_t(TQtz1)))
        silica = SiO2_TD/water_fraction
        mgkg = (silica+TQtz)/2
        TQtz = mgkg

    Tres_TQtz = B[0]+(TQtz*B[1])+(TQtz**2*B[2])+(TQtz**3*B[3])+(log10(TQtz)*B[4])

    Hvsurf_P_sampl_Tres = steamTable.hV_t(Tres_TQtz)
    Hlsurf_P_sampl_Tres = steamTable.hL_t(Tres_TQtz)
    Hvsurf_P_sampl_bara = steamTable.hV_p(P_Sampl_bara)
    Hlsurf_P_sampl_bara = steamTable.hL_p(P_Sampl_bara)
    
    TD =  steamTable.x_ph(P_Sampl_bara,Data['Enthalpy'])
    Excess = (Data['Enthalpy']-Hlsurf_P_sampl_Tres)/ \
        (Hvsurf_P_sampl_Tres-Hlsurf_P_sampl_Tres)
    # Dryness = float(sys.argv[9])
    Dryness = Data['Dryness']

    sp=dict(Enthalpy=Data['Enthalpy'],P_Sampl_bara=P_Sampl_bara)
    if Sampling_Data['Enthalpy_Res']>Sampling_Data['Entalphy_Sat']:
        # print('lebih besar')
        for i in Data['Brine'].keys():
            sp[i] = round( Data['Brine'][i]*((1-TD)/(1-Excess)),3)
    else:
        # print('lebih kecil')
        for i in Data['Brine'].keys():
            sp[i] = round( Data['Brine'][i]*(1- Dryness),3)
    return sp

def Geothermometer_cation(Data):
    #----- Amorphous Silica -------
    Amorp_silica = 731/(4.52-log10(Data['SiO2']))-273.15

    #----- Chalcedony ------------
    Chal = 1032/(4.69-log10(Data['SiO2']))-273.15

    #------ Quartz Condensation --------
    Qtz_Condensation = 0.00000031665*Data['SiO2']**3 - 0.00036686* \
         Data['SiO2']**2 + 0.28831* Data['SiO2'] + 77.034 * \
         log10(Data['SiO2']) - 42.198

    #-------Quartz Adiabatic------------
    Qtz_Adiabatic = 1522/(5.75-log10(Data['SiO2']))-273.15

    #---------log (Ca^.5/Na) + 2.06----------------
    logCaNa = log10(Data['Ca']**0.5/Data['Na'])+2.06

    #---------T beta = 4/3----------------
    T_beta = 1647/(log10(Data['Na']/Data['K'])+ \
                 1.333*(log10(Data['Ca']**0.5/Data['Na'])+ \
                        2.06)+2.47)-273.15
    #---------beta----------------
    if logCaNa>0 and T_beta<100:
        beta = 1.333
    else:
        beta = 0.333

    #---------Na-K-Ca-------------
    NaKca = 1647/(log10(Data['Na']/Data['K'])+beta* \
                (log10(Data['Ca']**0.5/Data['Na'])+ \
                 2.06)+2.47)-273.15

    #---------R factor Mg corr------
    R_factor_Mg_corr = 100*(0.08226*Data['Mg']/ \
            (0.08226*Data['Mg']+0.0499*Data['Ca']+ \
             0.02557*Data['K']))

    #---------delta T R<5----------
    if R_factor_Mg_corr <1:
        detltaT_Rless5 = 0.1
    else:
        detltaT_Rless5 = 1.03+59.97*log10(R_factor_Mg_corr)+145.05* \
        ((log10(R_factor_Mg_corr))**2)-36711*((log10(R_factor_Mg_corr))**2)/ \
        (NaKca+273)-16700000*log10(R_factor_Mg_corr)/((NaKca+273)**2)

    #--------delta T R>5----------
    if R_factor_Mg_corr <1:
        detltaT_Rmore5 = 0.1
    else:
        detltaT_Rmore5 = 10.66-4.7415*log10(R_factor_Mg_corr)+325.87* \
        (log10(R_factor_Mg_corr))**2-103200*((log10(R_factor_Mg_corr))**2)/ \
        (NaKca+273)-19680000*((log10(R_factor_Mg_corr))**2)/((NaKca+273)**2)+ \
        16050000*((log10(R_factor_Mg_corr))**3)/((NaKca+273)**2)

    #-------Na-K-Ca Mg corr---------
    if R_factor_Mg_corr<5:
        if detltaT_Rless5<0:
            NaKcaMg_corr = NaKca
        else:
            NaKcaMg_corr = NaKca-detltaT_Rless5
    else:
        if detltaT_Rmore5<0:
            NaKcaMg_corr = NaKca
        else:
            NaKcaMg_corr = NaKca-detltaT_Rmore5

    #---------Na/K  Fournier---------
    NaK_Fournier = 1217/(log10(Data['Na'] / Data['K'])+1.483)-273.15

    #--------Na/K  Truesdell--------
    NaK_Truesdell = 855.6/(log10(Data['Na'] / Data['K']) + 0.8573)-273.15

    #------Na/K (Giggenbach)---------
    NaK_Giggenbach = 1390/(1.75-log10(Data['K']/Data['Na']))-273.15

    #------K/Mg (Giggenbach)---------
    KMg_Giggenbach = 4410/(13.95-log10(Data['K']**2/Data['Mg']))-273.15

    return dict(Amorp_silica=Amorp_silica,Chal=Chal,Qtz_Condensation=Qtz_Condensation,
                Qtz_Adiabatic=Qtz_Adiabatic, logCaNa=logCaNa, T_beta=T_beta, beta =beta,
                NaKca=NaKca, R_factor_Mg_corr =R_factor_Mg_corr, detltaT_Rless5=detltaT_Rless5,
                detltaT_Rmore5=detltaT_Rmore5, NaKcaMg_corr=NaKcaMg_corr, NaK_Fournier=NaK_Fournier,
                NaK_Truesdell=NaK_Truesdell, NaK_Giggenbach=NaK_Giggenbach, KMg_Giggenbach=KMg_Giggenbach
                )

# Sampling_Data = dict(WHP = float(sys.argv[1]),
#                        FCV = float(sys.argv[2]),
#              P_MiniSep_barg = float(sys.argv[3]),
#                   T_MiniSep = float(sys.argv[4]),
#                   Elevation = float(sys.argv[5]),
#              Brine_Flowrate = float(sys.argv[6]),
#              Steam_Flowrate = float(sys.argv[7]),
#              Total_Flowrate = float(sys.argv[8]),
#                     Dryness = float(sys.argv[9]),
#                 Enthalpy_Res = 1444.9,
#                #Enthalpy_Res = float(sys.argv[10]),
#                Entalphy_Sat = float(sys.argv[11]),
#                    Temp_Res = float(sys.argv[12]),
#                        )
# Dryness_Total_Flowrate(Sampling_Data)

# Brine_Data  = dict(Brine = dict(
#                             Na =  float(sys.argv[15]),
#                              K =  float(sys.argv[16]),
#                             Ca =  float(sys.argv[17]),
#                             Mg =  float(sys.argv[18]),
#                            NH4 =  float(sys.argv[19]),
#                             Li =  float(sys.argv[20]),
#                       Fe_total =  float(sys.argv[21]),
#                              F =   float(sys.argv[22]),
#                           HCO3 =  float(sys.argv[23]),
#                             CL = float(sys.argv[24]),
#                            SO4 =  float(sys.argv[25]),
#                              B =   float(sys.argv[26]),
#                           SiO2 =  float(sys.argv[27]),
#                             As =    float(sys.argv[28]),
#                             )             ,
#                      pH = float(sys.argv[29]), TDS = float(sys.argv[30]))

# print ('*'*15 + 'Sampling Data' +'*'*15)
# print ('')
# for x, y in Sampling_Data.items():
#   print('%20s ' ' = '' %7.2f' % (x,y))
#   print ('')
## ------------------------ Reservoir Condition -------------------------------

 #print(json.dumps(float(sys.argv[1])))
if float(sys.argv[1]) == 1:
    Sampling_Data = dict(
        WHP = float(sys.argv[2]),
        FCV = float(sys.argv[3]),
        P_MiniSep_barg = float(sys.argv[4]),
        T_MiniSep = float(sys.argv[5]),
        Elevation = float(sys.argv[6]),
        Brine_Flowrate = float(sys.argv[7]),
        Steam_Flowrate = float(sys.argv[8]),
        Total_Flowrate = float(sys.argv[9]),
        Dryness = float(sys.argv[10]),
        Enthalpy_Res = float(sys.argv[11]),
        #Enthalpy_Res = float(sys.argv[10]),
        Entalphy_Sat = float(sys.argv[12]),
        Temp_Res = float(sys.argv[13]),
    )
    Dryness_Total_Flowrate(Sampling_Data)
    Brine_Data  = dict(
        Brine = dict(
            Na = float(sys.argv[16]),
            K = float(sys.argv[17]),
            Ca = float(sys.argv[18]),
            Mg = float(sys.argv[19]),
            NH4 = float(sys.argv[20]),
            Li = float(sys.argv[21]),
            Fe_total = float(sys.argv[22]),
            F = float(sys.argv[23]),
            HCO3 = float(sys.argv[24]),
            CL = float(sys.argv[25]),
            SO4 = float(sys.argv[26]),
            B = float(sys.argv[27]),
            SiO2 = float(sys.argv[28]),
            As = float(sys.argv[29]),
        ),
        pH = float(sys.argv[30]),
        TDS = float(sys.argv[31])
    )
    data = ResCond(dict(
        Brine_Data,
          Dryness = Sampling_Data['Dryness'],
        Elevation = Sampling_Data['Elevation'],
        Enthalpy = Sampling_Data['Enthalpy_Res'],
        P_sampl_barg = Sampling_Data['P_MiniSep_barg'],
        P_Sampl_bara = None
    ))
    print(json.dumps(data))
elif float(sys.argv[1]) == 2:
    data = Geothermometer_cation(dict(
        SiO2 = float(sys.argv[2]),
        Ca = float(sys.argv[3]),
        K = float(sys.argv[4]),
        Na = float(sys.argv[5]),
        Mg = float(sys.argv[6]),
    ))
    print(json.dumps(data))
