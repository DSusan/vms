import json
import sys
from pyXSteam.XSteam import XSteam
import math
from math import log10
steamTable = XSteam(XSteam.UNIT_SYSTEM_MKS)

NCG = dict(
	CO2 = float(sys.argv[1]),
	H2S = float(sys.argv[2]),
	NH3 = float(sys.argv[3]),
	Ar = float(sys.argv[4]),
	N2 = float(sys.argv[5]),
	CH4 = float(sys.argv[6]),
	H2 = float(sys.argv[7]),
	He = float(sys.argv[8]),
	CO = float(sys.argv[9]),
	O2 = float(sys.argv[10])
)

# NCG = dict(
# 	CO2 = 92.90,
# 	H2S = 2.46,
# 	NH3 = 0.140,
# 	Ar = 0.0420,
# 	N2 = 4.1,
# 	CH4 = 0.14,
# 	H2 = 0.40 ,
# 	He = 0.00,
# 	CO =  0.00,
# 	O2 = 0.086
# 	)
# NCG[CO2] = [ 92.90  ]
# NCG[H2S] = [  2.46  ]
# NCG[NH3] = [  0.140 ]
# NCG[Ar]  = [  0.0420]
# NCG[N2]  = [  4.1   ]
# NCG[CH4] = [  0.14  ]
# NCG[H2]  = [  0.40  ]
# NCG['He']  = [  0.00  ]
# NCG[CO]  = [  0.00  ]
# NCG[O2]  = [  0.086 ]

molecular_weight_CO2 = 44
molecular_weight_H2S = 34
molecular_weight_NH3 = 17
molecular_weight_Ar  = 16
molecular_weight_N2  = 28
molecular_weight_CH4 = 40
molecular_weight_H2  = 2
molecular_weight_He  = 4
molecular_weight_CO  = 28
molecular_weight_O2  = 1

wt_percent = 1.64
air_contam = 0
mole_percent = 0
GS10pangkat6 = 0

airO2Analysis = 0.01 * NCG['O2'] / 0.21
datsum = [NCG['CO2'], NCG['H2S'], NCG['NH3'], NCG['Ar'], NCG['N2'], NCG['CH4'], NCG['H2'],NCG['He'], NCG['CO'], NCG['O2']]
sum_gas = sum(datsum)

if NCG['O2']==0:
    airN2=(0.78*air_contam)/(0.78*air_contam+NCG['N2']* sum_gas/(air_contam+sum_gas))
else:
    airN2=(0.78*airO2Analysis)/NCG['N2']

if NCG['O2']==0:
    airAr=(0.00934*air_contam)/(0.00934*NCG['air_contam']+ abs(NCG['Ar'])*sum_gas/(air_contam+sum_gas))
else:
    airAr=(0.00934*airO2Analysis)/abs(NCG['Ar'])

#sum of percentages=CO2+H2S+NH3+CH4+N2+Ar+H2+He+CO

Hee = abs(NCG['He']) * molecular_weight_He / 100
Arr = abs(NCG['Ar']) * ( 1 - airAr) * molecular_weight_Ar
NN2 = abs(NCG['N2']) * ( 1 - airAr ) * molecular_weight_N2
CC02 = abs(NCG['CO2']) * molecular_weight_CO2
H2SS = abs(NCG['H2S']) * molecular_weight_H2S
NHH3 = abs(NCG['NH3']) * molecular_weight_NH3
CHH4 = abs(NCG['CH4']) * molecular_weight_CH4

avemolwt = (Hee + Arr + NN2 + CC02 + H2SS + NHH3 + CHH4) / 100

if mole_percent == 0:
	avemolwt = ((wt_percent/avemolwt)/(wt_percent/avemolwt+(100-wt_percent)/18))*100
else:
	avemolwt = (mole_percent/100)

if mole_percent==0:
    molepercentNCG=(wt_percent/avemolwt)/(wt_percent/avemolwt+(100-wt_percent)/18)*100
else:
    molepercentNCG=mole_percent/100

if GS10pangkat6==0:
    GSrationUncorr=(molepercentNCG/100)/(1-(molepercentNCG/100))
else:
    GSrationUncorr=GS10pangkat6/1000000

if airO2Analysis==0:
    GSrationAirCorr=GSrationUncorr
else:
    GSrationAirCorr=GSrationUncorr*(1-0.01*airO2Analysis)

#if ii =='He':
#	if NCG["Gas"][ii]==0:
#		He=GSrationAirCorr*abs(NCG["Gas"][ii][0])/100
#	else:
#		He=GSrationAirCorr*(abs(NCG["Gas"][ii][0])-0.00000524*airO2Analysis)/100
#if ii == 'Ar':
#	if NCG["Gas"][ii]==0:
#		Ar=GSrationAirCorr*abs(NCG["Gas"][ii][0])/100
#	else:
#		Ar=GSrationAirCorr*(abs(NCG["Gas"][ii][0]*(1-airAr))/100)
#if ii == 'N2':
#	if NCG["Gas"][ii]==0:
#		N2=GSrationAirCorr*abs(NCG["Gas"][ii][0])/100
#	else:
#		N2=GSrationAirCorr*abs(NCG["Gas"][ii][0]*(1-airN2))/100
#if ii == 'H2' or 'CO':
#	globals()[ii]=GSrationAirCorr*abs(NCG["Gas"][ii][0]/100)
#else:
#	globals()[ii]=GSrationAirCorr*abs(NCG["Gas"][ii][0])/100

if NCG['He'] == 0:
	He = GSrationAirCorr*abs(NCG['He'])/100
else:
	He = GSrationAirCorr*(abs(NCG['He'])-0.00000524*airO2Analysis)/100

if NCG['Ar'] == 0:
	Ar = GSrationAirCorr*abs(NCG['Ar'])/100
else:
	Ar = GSrationAirCorr*(abs(NCG['Ar']*(1-airAr))/100)

if NCG['N2'] == 0:
	N2 = GSrationAirCorr*abs(NCG['N2'])/100
else:
	N2 = GSrationAirCorr*abs(NCG['N2']*(1-airN2))/100

H2 = GSrationAirCorr * abs(NCG['H2'] / 100)
CO = GSrationAirCorr * abs(NCG['CO'] / 100)
CO2 = GSrationAirCorr * abs(NCG['CO2'] / 100)
H2S = GSrationAirCorr * abs(NCG['H2S'] / 100)
NH3 = GSrationAirCorr * abs(NCG['NH3'] / 100)
CH4 = GSrationAirCorr * abs(NCG['CH4'] / 100)
CO  = GSrationAirCorr * abs(NCG['CO'] / 100)

if NCG['CO2'] < 75:
	DAP = ((24775/((log10(CH4/CO2))-(log10(H2/CO2))-(log10(H2S/CO2))+(log10(0.1))+36.05))-273.15)
else:
	DAP = ((24775/((log10(CH4/CO2))-(log10(H2/CO2))-(log10(H2S/CO2))+(log10(1))+36.05))-273.15)

H2XAR = 70*(2.5+log10(H2/Ar))
CO2XN2 = 173.2+(48.751*log10(CO2/N2))+(7.6*((log10(CO2/N2))**2))+(1.739*((log10(CO2/N2))**3))
H2SXAr = 137.6+(42.265*log10(H2S/Ar))+(4.108*((log10(H2S/Ar))**2))
CH4XCO2 = (4625/(10.4+log10(CH4/CO2)))-273.15
CO2XH2 = 341.7-(28.57*log10(CO2/H2))
H2SXH2 = 304.1-(39.48*log10(H2S/H2))

data = dict(H2XAR=H2XAR,CO2XN2=CO2XN2,H2SXAr=H2SXAr,CH4XCO2=CH4XCO2, CO2XH2=CO2XH2, H2SXH2=H2SXH2,DAP=DAP)
print(json.dumps(data))
