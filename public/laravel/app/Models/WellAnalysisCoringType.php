<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WellAnalysisCoringType extends Model
{
	use SoftDeletes;

	protected $table = 'well_analysis_coring_type';
	protected $dates = ['deleted_at'];
}
