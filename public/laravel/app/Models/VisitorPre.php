<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VisitorPre extends Model
{
    use SoftDeletes;

	protected $table = 'visitor_pre';
	protected $dates = ['deleted_at'];
}
