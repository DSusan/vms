<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WellAnalysisCuttingType extends Model
{
	use SoftDeletes;

	protected $table = 'well_analysis_cutting_type';
	protected $dates = ['deleted_at'];
}
