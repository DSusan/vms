<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
	use SoftDeletes;

	protected $table = 'users';
	protected $dates = ['deleted_at'];

	public function privilege()
	{
		return $this->hasMany('App\Models\UserPrivilege', 'id_users', 'id');
	}
}
