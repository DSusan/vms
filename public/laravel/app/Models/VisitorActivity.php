<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VisitorActivity extends Model
{
    use SoftDeletes;

	protected $table = 'visitor_activity';
	protected $dates = ['deleted_at'];
}
