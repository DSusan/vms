<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPrivilege extends Model
{
	use SoftDeletes;

	protected $table = 'users_privileges';
	protected $dates = ['deleted_at'];

	public function user()
	{
		return $this->belongsTo('App\Models\User', 'id_users', 'id');
	}

	public function module()
	{
		return $this->belongsTo('App\Models\Module', 'id_modules', 'id');
	}

	public function action()
	{
		return $this->belongsTo('App\Models\Action', 'id_actions', 'id');
	}
}
