<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HardwareType extends Model
{
    use SoftDeletes;
    
    protected $table = 'hardware_type';
	protected $dates = ['deleted_at'];
}
