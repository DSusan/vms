<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleDivision extends Model
{
	use SoftDeletes;

	protected $table = 'roles_divisions';
	protected $dates = ['deleted_at'];

	public function privilege()
	{
		return $this->hasMany('App\Models\RolePrivilege', 'id_roles_divisions', 'id');
	}

	public function role()
	{
		return $this->belongsTo('App\Models\Role', 'id_roles', 'id');
	}

	public function division()
	{
		return $this->belongsTo('App\Models\Division', 'id_divisions', 'id');
	}
}
