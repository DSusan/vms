<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RolePrivilege extends Model
{
	use SoftDeletes;

	protected $table = 'roles_privileges';
	protected $dates = ['deleted_at'];

	public function roleDiv()
	{
		return $this->belongsTo('App\Models\RoleDivision', 'id_roles_divisions', 'id');
	}

	public function module()
	{
		return $this->belongsTo('App\Models\Module', 'id_modules', 'id');
	}

	public function action()
	{
		return $this->belongsTo('App\Models\Action', 'id_actions', 'id');
	}
}
