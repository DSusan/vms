<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WellAnalysisLoggingType extends Model
{
	use SoftDeletes;

	protected $table = 'well_analysis_logging_type';
	protected $dates = ['deleted_at'];
}
