<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HardwareInfo extends Model
{
    use SoftDeletes;
    
    protected $table = 'hardware_info';
	protected $dates = ['deleted_at'];
}
