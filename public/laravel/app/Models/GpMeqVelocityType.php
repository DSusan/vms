<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GpMeqVelocityType extends Model
{
	use SoftDeletes;

	protected $table = 'gp_meq_velocity_type';
	protected $dates = ['deleted_at'];
}
