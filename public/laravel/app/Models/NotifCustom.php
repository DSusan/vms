<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotifCustom extends Model
{
    use SoftDeletes;

	protected $table = 'notification';
	protected $dates = ['deleted_at'];

}
