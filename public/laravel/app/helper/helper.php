<?php
namespace App\helper;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\UserPrivilege;
use App\Models\RolePrivilege;
use App\Models\Module;
use App\Models\Action;
use App\Models\UserLevel;
use App\Models\NotifCustom;
use App\Mail\NotifEmail;

class notifC {
	/* public static function notifCx($user_id){
		$notif = NotifCustom::select('id')
		->where('target', '=', $user_id)
		->whereNull('read')
		->get();
		$Countnotif = count($notif);
		return("<span class='btn btn-success btn-sm btn-bold btn-font-md'> ".$Countnotif." New Message</span>");
	}
	public static function notifDtl($user_id){
		//$user_id = $request->user_id;
		$notif = NotifCustom::select('id')
		->where('target', '=', $user_id)
		->whereNull('read')
		->get();
		return view('notif.index', compact('notif'));
	}*/
	public static function notifx($from, $content, $link){
		# cek atasannya
		#$user = User::findOrFail($from);
		$users = DB::table('users AS a')
		->leftjoin('roles_divisions AS b', 'b.id', '=', 'a.id_roles_divisions')
		->leftjoin('roles AS c', 'c.id', '=', 'b.id_roles')
		->leftjoin('divisions', 'divisions.id', '=', 'b.id_divisions')
		->select('a.id AS userId', 'a.username AS username', 'a.password AS password', 'a.id_roles_divisions AS userRoleDivId', 'b.id_roles AS userRoleId', 'b.id_roles_divisions_parent','b.id_divisions AS userDivId', 'c.name AS roleName', 'divisions.name AS divName')
		->where('a.id','=',$from)
		->orderBy('a.username', 'ASC')
		->get();

		foreach ($users as $key => $value)
		{
			// define var
			$idRoleDivParent  = $value->id_roles_divisions_parent;
			$userId = $value->userId;

			//cek user untuk parentnya
			$usesParent = DB::table('users AS a')
			->select('a.id as userIdParent', 'a.username AS userName')
			->where('a.id_roles_divisions', '=', $idRoleDivParent)
			->get();

			$mailTo = array();
			// $xx = "";

			foreach ($usesParent as $keyParent => $valueParent)
			{
				// define var
				$userIdParent = $valueParent->userIdParent;
				$usernameParent = $valueParent->userName;

				// push ke array TO, untuk email
				$mailTo[] = $valueParent->userName;

				// masukin ke table notif
				$notifCustom = new NotifCustom;
				$notifCustom->from = $userId;
				$notifCustom->target = $userIdParent;
				$notifCustom->content = $content;
				$notifCustom->link = $link;
				//$notifCustom->content = $usernameParent;
				$notifCustom->save();
			}
		}
		$mailContent = ['message' => 'This is a test!'];
		$arrayTo = array("satrioad@gmail.com");
		Mail::to($arrayTo)->send(new NotifEmail($mailContent));
	   // return($userId);
	}
}
