<?php
return [

	/*
	|--------------------------------------------------------------------------
	| Directory Paths
	|--------------------------------------------------------------------------
	|
	| This value determines the directory path for the assets you use such as
	| CSS, js, plug-ins, etc.
	| 
	*/

	'path' => [
		// 'public' => '', //activate this if your domain has already included public directory
		'public' => '/vms/public',
		'uploads' => '/vms/public/uploads',
		'plugins' => '/vms/public/laravel/assets/plugins',
		'js' => '/vms/public/laravel/assets/js',
		'css' => '/vms/public/laravel/assets/css',
		'media' => '/vms/public/laravel/assets/media'
	],

];