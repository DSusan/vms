<?php

return [
	
	// table 1
	'student' => [
		'name', 'address' // column name
	],
	
	// table 2
	'resmdaily' => [
		'id_well_track', 'id_well_status', 'date', 'whp', 'fcv', 'time_open', 'total_mass_flow', 'enthalpy', 'steam_rate', 'remark'
	],

	// 'glstation' => 'gl_station',

	'gl_station' => [
		'name', 'date', 'northing', 'easting', 'elevation', 'location', 'sample'
	],
];

