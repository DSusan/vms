<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Login
Route::get('/', 'Auth\LoginController@showLoginForm');

//Logout
Route::get('/logout', 'Auth\LoginController@getLogout');

Auth::routes();

Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	//report
	Route::get('/report','ReportController@index')->name('editor.report.index');
	Route::get('/report/getData','ReportController@getData')->name('editor.report.getData');
	Route::get('/report/export_query','ReportController@export_query')->name('editor.report.export_query');

	//excel
	Route::get('/visitor/export','VisitorController@export_excel')->name('editor.visitor.export');
	//Route::get('/siswa/export_excel', 'SiswaController@export_excel');
	//Home
	Route::get('/', ['uses' => 'UserController@dashboard']);
	//dashboard
	Route::get('/dashboard', ['as' => 'editor.visitor.dashboard', 'uses' => 'UserController@dashboard']);
	//test
	Route::get('/test', ['uses' => 'TestController@index']);

	//visitor OCR
	Route::get('/visitor/ocr','VisitorOcrController@index')->name('editor.visitorocr.index');
	Route::post('/visitor/ocr/upload','VisitorOcrController@upload')->name('editor.visitorocr.upload');
	
	
	//visitor pre-register
	Route::get('/visitor/preregister','VisitorController@preregister')->name('editor.visitor.preregister');
	Route::get('/visitor/preregister/getData','VisitorPreController@getData')->name('editor.visitorpre.getData');
	Route::get('/visitor/preregister/getDataDtl','VisitorPreController@getDataDtl')->name('editor.visitorpre.preregistergetDataDtl');
	Route::post('/visitor/preregisternew','VisitorPreController@insert')->name('editor.visitorpre.preregisternew');
	Route::post('/visitor/preregisteredit','VisitorPreController@edit')->name('editor.visitorpre.preregisteredit');
	Route::post('/visitor/preregisterapp','VisitorPreController@app')->name('editor.visitorpre.preregisterapp');
	//visitor 
	Route::get('/visitor', ['as' => 'editor.visitor.index', 'uses' => 'VisitorController@index']);
	Route::get('/visitor','VisitorController@index')->name('editor.visitor.index');
	Route::get('/visitor/getData','VisitorController@getData')->name('editor.visitor.getData');
	Route::post('/visitor/insertData','VisitorController@insertData')->name('editor.visitor.insertData');
	Route::post('/visitor/updateData','VisitorController@updateData')->name('editor.visitor.updateData');
	Route::post('/visitor/cekoutData','VisitorController@cekoutData')->name('editor.visitor.cekoutData');
	Route::post('/visitor/cekinData','VisitorController@cekinData')->name('editor.visitor.cekinData');
	Route::get('/visitor/getDataDtl','VisitorController@getDataDtl')->name('editor.visitor.getDataDtl');
	Route::post('/visitor/resetPassword','VisitorController@resetPassword')->name('editor.visitor.resetPassword');
	Route::get('/visitor/tes','VisitorController@tes')->name('editor.visitor.tes');
	Route::post('/visitor/upload','VisitorController@uploadsxxx')->name('editor.visitor.uploads');

	Route::get('/visitor/getFloor','VisitorController@getFloor')->name('editor.visitor.getFloor');

	
	#Route::post('/visitor/upload', ['as' => 'editor.visitor.upload', 'uses' => 'VisitorController@uploads']);
	// visitor cek in / out
	Route::get('/visitor/activity','VisitorActivityController@index')->name('editor.visitoractivity.index');
	Route::get('/visitor/activity/getData','VisitorActivityController@getData')->name('editor.visitoractivity.getData');
	Route::get('/visitor/activity/getVisitor','VisitorActivityController@getVisitor')->name('editor.visitoractivity.getVisitor');
	Route::get('/visitor/activity/getVisitorDtl','VisitorActivityController@getVisitorDtl')->name('editor.visitoractivity.getVisitorDtl');
	Route::post('/visitor/activity/insertData','VisitorActivityController@insertData')->name('editor.visitoractivity.insertData');
	
	//visitor block
	Route::get('/visitor/block','VisitorController@block')->name('editor.visitor.block');
	Route::get('/visitor/block/getData','VisitorBlockController@getData')->name('editor.visitorblock.getData');
	Route::get('/visitor/block/getDatac','VisitorBlockController@getDatac')->name('editor.visitorblock.getDatac');
	Route::get('/visitor/block/getDataDtl','VisitorBlockController@getDataDtl')->name('editor.visitorblock.getDataDtl');
	Route::post('/visitor/block/blockThis','VisitorBlockController@blockThis')->name('editor.visitorblock.blockThis');
	
	// visitor face Recognition
	Route::get('/visitor/fc','VisitorController@fc')->name('editor.visitor.fc');
	//Alat
	//Alat
	Route::get('/hardwareinfo', ['as' => 'editor.hardware.index', 'uses' => 'HardwareInfoController@index']);
	Route::post('/hardwareinfo', ['as' => 'editor.hardware.store', 'uses' => 'HardwareInfoController@store']);
	Route::put('/hardwareinfo/{id}/update', ['as' => 'editor.hardware.update', 'uses' => 'HardwareInfoController@update']);
	Route::delete('/hardwareinfo/{id}/delete', ['as' => 'editor.hardware.delete', 'uses' => 'HardwareInfoController@delete']);
	

	
	
	//gc_Smapling
	Route::post('/gcsampling/getStatus', ['as' => 'editor.gcsampling.getStatus', 'uses' => 'GcSamplingDataController@getStatus']);
	Route::post('/gcsampling/getStatusDtl', ['as' => 'editor.gcsampling.getStatusDtl', 'uses' => 'GcSamplingDataController@getStatusDtl']);
	Route::post('/gcsampling/approvexx', ['as' => 'editor.gcsampling.approvexx', 'uses' => 'GcSamplingDataController@approvexx']);
	Route::post('/gcsampling/cancelapprove', ['as' => 'editor.gcsampling.cancelapprove', 'uses' => 'GcSamplingDataController@cancelapprove']);
	//gc_isotope well
	Route::post('/gcisotopewell/getSampling', ['as' => 'editor.gcisotope.getSampling', 'uses' => 'GcIsotopeDataController@getSampling']);
	Route::post('/gcisotopesf/getSampling', ['as' => 'editor.gcisotopesf.indexSf', 'uses' => 'GcIsotopeDataController@getSampling']);
    //gc_ssi_calc
    Route::get('/gcssicalc', ['as' => 'editor.gcssicalc.index', 'uses' => 'GcSsiCalcController@index']);
	//gcelementvselement
	Route::get('/gcelementvselement', ['as' => 'editor.gcelementvselement.index', 'uses' => 'GcElementVsElementController@index']);
	//ncg baru
	Route::get('/gcncgwell/cariGraf', ['as' => 'editor.gcncg.cariGraf', 'uses' => 'GcNcgDataController@cariGraf']);
	Route::get('/gcncgwell/viewGraf', ['as' => 'editor.gcncg.viewGraf', 'uses' => 'GcNcgDataController@viewGraf']);
	Route::post('/gcncgwell/getSampling', ['as' => 'editor.gcncg.getSampling', 'uses' => 'GcNcgDataController@getSampling']);
	Route::post('/gcncg/approved', ['as' => 'editor.gcncg.approved', 'uses' => 'GcNcgDataController@approved']);
	//GasThermometerCalc
	Route::get('/gc_gas_geothermometer_calc/index', ['as' => 'editor.gcgasgeothermometercalc.index', 'uses' => 'GcGasGeothermometerCalcController@index']);
	//BrineThermometer
	Route::get('/gc_brine_geothermometer_calc/index', ['as' => 'editor.gcbrinegeothermometercalc.index', 'uses' => 'GcBrineGeothermometerCalcController@index']);

	

	//Notification
	Route::post('/notif-count', ['as' => 'editor.notif.count', 'uses' => 'NotifController@notifCek']);
	Route::post('/notif-dtl',['as' => 'editor.notif.dtl', 'uses' => 'NotifController@notifCekDtl']);
	Route::put('/notifread/{id}',['as' => 'editor.notif.read', 'uses' => 'NotifController@notifRead']);

	//Graf
	Route::get('/graf', ['as' => 'editor.graf.index', 'uses' => 'GrafController@index']);
	Route::post('/grafAjax', ['as' => 'editor.graf.ajax', 'uses' => 'GrafController@GetAllTable']);
	Route::post('/grafColumn', ['as' => 'editor.graf.column', 'uses' => 'GrafController@GetAllColumn']);
	Route::post('/grafShow',['as' => 'editor.graf.show', 'uses' => 'GrafController@show']);

	// Import
	Route::post('/read-file', 'ImportController@readFile');
	Route::post('/import-file/{table}', 'ImportController@importFile');
	Route::get('/list/{table}', 'ImportController@getList');

	// Export
	Route::get('/{table}/export/pdf', 'ImportController@exportPdf');
	Route::get('/{table}/export/xls', 'ImportController@exportXls');
	Route::get('/{table}/export/csv', 'ImportController@exportCsv');

	//User
	// Route::get('/user', ['middleware' => ['role:user|read'], 'as' => 'editor.user.index', 'uses' => 'UserController@index']);
	Route::get('/user', ['as' => 'editor.user.index', 'uses' => 'UserController@index']);
	Route::post('/user',['as' => 'editor.user.store', 'uses' => 'UserController@store']);
	Route::put('/user/{id}/update', ['as' => 'editor.user.update', 'uses' => 'UserController@update']);
	Route::delete('/user/{id}/delete', ['as' => 'editor.user.delete', 'uses' => 'UserController@delete']);
	Route::get('/uprivilege/{id}/edit', ['as' => 'editor.uprivilege.edit', 'uses' => 'UserController@editPrivilege']);
	Route::put('/uprivilege/{id}/edit', ['as' => 'editor.uprivilege.update', 'uses' => 'UserController@updatePrivilege']);

	//Division
	Route::get('/division', ['as' => 'editor.division.index', 'uses' => 'DivisionController@index']);
	Route::post('/division', ['as' => 'editor.division.store', 'uses' => 'DivisionController@store']);
	Route::put('/division/{id}/update', ['as' => 'editor.division.update', 'uses' => 'DivisionController@update']);
	Route::delete('/division/{id}/delete', ['as' => 'editor.division.delete', 'uses' => 'DivisionController@delete']);

	//Role
	Route::get('/role', ['as' => 'editor.role.index', 'uses' => 'RoleController@index']);
	Route::post('/role', ['as' => 'editor.role.store', 'uses' => 'RoleController@store']);
	Route::put('/role/{id}/update', ['as' => 'editor.role.update', 'uses' => 'RoleController@update']);
	Route::delete('/role/{id}/delete', ['as' => 'editor.role.delete', 'uses' => 'RoleController@delete']);

	//Action
	Route::get('/action', ['as' => 'editor.action.index', 'uses' => 'ActionController@index']);
	Route::post('/action', ['as' => 'editor.action.store', 'uses' => 'ActionController@store']);
	Route::put('/action/{id}/update', ['as' => 'editor.action.update', 'uses' => 'ActionController@update']);
	Route::delete('/action/{id}/delete', ['as' => 'editor.action.delete', 'uses' => 'ActionController@delete']);

	//Module
	Route::get('/module', ['as' => 'editor.module.index', 'uses' => 'ModuleController@index']);
	Route::post('/module', ['as' => 'editor.module.store', 'uses' => 'ModuleController@store']);
	Route::put('/module/{id}/update', ['as' => 'editor.module.update', 'uses' => 'ModuleController@update']);
	Route::delete('/module/{id}/delete', ['as' => 'editor.module.delete', 'uses' => 'ModuleController@delete']);

	//Role Division (Position)
	Route::get('/rolediv', ['as' => 'editor.rolediv.index', 'uses' => 'RoleDivController@index']);
	Route::post('/rolediv', ['as' => 'editor.rolediv.store', 'uses' => 'RoleDivController@store']);
	Route::put('/rolediv/{id}/update', ['as' => 'editor.rolediv.update', 'uses' => 'RoleDivController@update']);
	Route::delete('/rolediv/{id}/delete', ['as' => 'editor.rolediv.delete', 'uses' => 'RoleDivController@delete']);
	Route::get('/rprivilege/{id}/edit', ['as' => 'editor.rprivilege.edit', 'uses' => 'RoleDivController@editPrivilege']);
	Route::put('/rprivilege/{id}/edit', ['as' => 'editor.rprivilege.update', 'uses' => 'RoleDivController@updatePrivilege']);









	//GEOLOGY
	//Mapping
	/* Route::get('/glmapping', ['as' => 'editor.glmapping.index', 'uses' => 'GlMappingController@index']);
	Route::post('/glmapping', ['as' => 'editor.glmapping.store', 'uses' => 'GlMappingController@store']);
	Route::put('/glmapping/{id}/update', ['as' => 'editor.glmapping.update', 'uses' => 'GlMappingController@update']);
	Route::delete('/glmapping/{id}/delete', ['as' => 'editor.glmapping.delete', 'uses' => 'GlMappingController@delete']);

	//Mapping Station
	Route::get('/glmappingstation/{mid}', ['as' => 'editor.glmappingstation.index', 'uses' => 'GlMappingStationController@index']);
	Route::post('/glmappingstation/{mid}', ['as' => 'editor.glmappingstation.store', 'uses' => 'GlMappingStationController@store']);
	Route::put('/glmappingstation/{mid}/{id}/update', ['as' => 'editor.glmappingstation.update', 'uses' => 'GlMappingStationController@update']);
	Route::delete('/glmappingstation/{id}/delete', ['as' => 'editor.glmappingstation.delete', 'uses' => 'GlMappingStationController@delete']);
	Route::post('/glmappingstation/{id}/download', ['as' => 'editor.glmappingstation.download', 'uses' => 'GlMappingStationController@download']);

	//Mapping Anlysis
	Route::get('/glmappinganalysis/{mid}', ['as' => 'editor.glmappinganalysis.index', 'uses' => 'GlMappingAnalysisController@index']);
	Route::post('/glmappinganalysis/{mid}', ['as' => 'editor.glmappinganalysis.store', 'uses' => 'GlMappingAnalysisController@store']);
	Route::put('/glmappinganalysis/{mid}/{id}/update', ['as' => 'editor.glmappinganalysis.update', 'uses' => 'GlMappingAnalysisController@update']);
	Route::delete('/glmappinganalysis/{id}/delete', ['as' => 'editor.glmappinganalysis.delete', 'uses' => 'GlMappingAnalysisController@delete']);
	Route::post('/glmappinganalysis/{id}/download', ['as' => 'editor.glmappinganalysis.download', 'uses' => 'GlMappingAnalysisController@download']);

	//Mapping Report & Map
	Route::get('/glmappingreportmap/{mid}', ['as' => 'editor.glmappingreportmap.index', 'uses' => 'GlMappingReportMapController@index']);
	Route::post('/glmappingreportmap/{mid}', ['as' => 'editor.glmappingreportmap.store', 'uses' => 'GlMappingReportMapController@store']);
	Route::put('/glmappingreportmap/{mid}/{id}/update', ['as' => 'editor.glmappingreportmap.update', 'uses' => 'GlMappingReportMapController@update']);
	Route::delete('/glmappingreportmap/{id}/delete', ['as' => 'editor.glmappingreportmap.delete', 'uses' => 'GlMappingReportMapController@delete']);
	Route::post('/glmappingreportmap/{id}/download', ['as' => 'editor.glmappingreportmap.download', 'uses' => 'GlMappingReportMapController@download']);

	//Hazard
	Route::get('/glhazard', ['as' => 'editor.glhazard.index', 'uses' => 'GlHazardController@index']);
	Route::post('/glhazard', ['as' => 'editor.glhazard.store', 'uses' => 'GlHazardController@store']);
	Route::put('/glhazard/{id}/update', ['as' => 'editor.glhazard.update', 'uses' => 'GlHazardController@update']);
	Route::delete('/glhazard/{id}/delete', ['as' => 'editor.glhazard.delete', 'uses' => 'GlHazardController@delete']);

	//Hazard Anlysis
	Route::get('/glhazardanalysis/{mid}', ['as' => 'editor.glhazardanalysis.index', 'uses' => 'GlHazardAnalysisController@index']);
	Route::post('/glhazardanalysis/{mid}', ['as' => 'editor.glhazardanalysis.store', 'uses' => 'GlHazardAnalysisController@store']);
	Route::put('/glhazardanalysis/{mid}/{id}/update', ['as' => 'editor.glhazardanalysis.update', 'uses' => 'GlHazardAnalysisController@update']);
	Route::delete('/glhazardanalysis/{id}/delete', ['as' => 'editor.glhazardanalysis.delete', 'uses' => 'GlHazardAnalysisController@delete']);
	Route::post('/glhazardanalysis/{id}/download', ['as' => 'editor.glhazardanalysis.download', 'uses' => 'GlHazardAnalysisController@download']);

	//Hazard Report & Map
	Route::get('/glhazardreportmap/{mid}', ['as' => 'editor.glhazardreportmap.index', 'uses' => 'GlHazardReportMapController@index']);
	Route::post('/glhazardreportmap/{mid}', ['as' => 'editor.glhazardreportmap.store', 'uses' => 'GlHazardReportMapController@store']);
	Route::put('/glhazardreportmap/{mid}/{id}/update', ['as' => 'editor.glhazardreportmap.update', 'uses' => 'GlHazardReportMapController@update']);
	Route::delete('/glhazardreportmap/{id}/delete', ['as' => 'editor.glhazardreportmap.delete', 'uses' => 'GlHazardReportMapController@delete']);
	Route::post('/glhazardreportmap/{id}/download', ['as' => 'editor.glhazardreportmap.download', 'uses' => 'GlHazardReportMapController@download']);

	//Well
	Route::get('/well', ['as' => 'editor.well.index', 'uses' => 'WellController@index']);
	Route::post('/well', ['as' => 'editor.well.store', 'uses' => 'WellController@store']);
	Route::put('/well/{id}/update', ['as' => 'editor.well.update', 'uses' => 'WellController@update']);
	Route::delete('/well/{id}/delete', ['as' => 'editor.well.delete', 'uses' => 'WellController@delete']);

	//Well Directional Survey
	Route::get('/welldirsurvey', ['as' => 'editor.welldirsurvey.index', 'uses' => 'WellDirSurveyController@index']);
	Route::post('/welldirsurvey', ['as' => 'editor.welldirsurvey.store', 'uses' => 'WellDirSurveyController@store']);
	Route::put('/welldirsurvey/{id}/update', ['as' => 'editor.welldirsurvey.update', 'uses' => 'WellDirSurveyController@update']);
	Route::delete('/welldirsurvey/{id}/delete', ['as' => 'editor.welldirsurvey.delete', 'uses' => 'WellDirSurveyController@delete']);

	//Well Report Daily
	Route::get('/wellreportdaily', ['as' => 'editor.wellreportdaily.index', 'uses' => 'WellReportDailyController@index']);
	Route::post('/wellreportdaily', ['as' => 'editor.wellreportdaily.store', 'uses' => 'WellReportDailyController@store']);
	Route::put('/wellreportdaily/{id}/update', ['as' => 'editor.wellreportdaily.update', 'uses' => 'WellReportDailyController@update']);
	Route::delete('/wellreportdaily/{id}/delete', ['as' => 'editor.wellreportdaily.delete', 'uses' => 'WellReportDailyController@delete']);
	Route::post('/wellreportdaily/{id}/download', ['as' => 'editor.wellreportdaily.download', 'uses' => 'WellReportDailyController@download']);

	//Well Report WSG Morning
	Route::get('/wellreportwsgmorning', ['as' => 'editor.wellreportwsgmorning.index', 'uses' => 'WellReportWsgMorningController@index']);
	Route::post('/wellreportwsgmorning', ['as' => 'editor.wellreportwsgmorning.store', 'uses' => 'WellReportWsgMorningController@store']);
	Route::put('/wellreportwsgmorning/{id}/update', ['as' => 'editor.wellreportwsgmorning.update', 'uses' => 'WellReportWsgMorningController@update']);
	Route::delete('/wellreportwsgmorning/{id}/delete', ['as' => 'editor.wellreportwsgmorning.delete', 'uses' => 'WellReportWsgMorningController@delete']);
	Route::post('/wellreportwsgmorning/{id}/download', ['as' => 'editor.wellreportwsgmorning.download', 'uses' => 'WellReportWsgMorningController@download']);

	//Well Report Non Daily
	Route::get('/wellreportnondaily', ['as' => 'editor.wellreportnondaily.index', 'uses' => 'WellReportNonDailyController@index']);
	Route::post('/wellreportnondaily', ['as' => 'editor.wellreportnondaily.store', 'uses' => 'WellReportNonDailyController@store']);
	Route::put('/wellreportnondaily/{id}/update', ['as' => 'editor.wellreportnondaily.update', 'uses' => 'WellReportNonDailyController@update']);
	Route::delete('/wellreportnondaily/{id}/delete', ['as' => 'editor.wellreportnondaily.delete', 'uses' => 'WellReportNonDailyController@delete']);
	Route::post('/wellreportnondaily/{id}/download', ['as' => 'editor.wellreportnondaily.download', 'uses' => 'WellReportNonDailyController@download']);

	//Well Data Logging
	Route::get('/welldatalogging', ['as' => 'editor.welldatalogging.index', 'uses' => 'WellDataLoggingController@index']);
	Route::post('/welldatalogging', ['as' => 'editor.welldatalogging.store', 'uses' => 'WellDataLoggingController@store']);
	Route::put('/welldatalogging/{id}/update', ['as' => 'editor.welldatalogging.update', 'uses' => 'WellDataLoggingController@update']);
	Route::delete('/welldatalogging/{id}/delete', ['as' => 'editor.welldatalogging.delete', 'uses' => 'WellDataLoggingController@delete']);
	Route::post('/welldatalogging/{id}/download', ['as' => 'editor.welldatalogging.download', 'uses' => 'WellDataLoggingController@download']);

	//Well Data Coring
	Route::get('/welldatacoring', ['as' => 'editor.welldatacoring.index', 'uses' => 'WellDataCoringController@index']);
	Route::post('/welldatacoring', ['as' => 'editor.welldatacoring.store', 'uses' => 'WellDataCoringController@store']);
	Route::put('/welldatacoring/{id}/update', ['as' => 'editor.welldatacoring.update', 'uses' => 'WellDataCoringController@update']);
	Route::delete('/welldatacoring/{id}/delete', ['as' => 'editor.welldatacoring.delete', 'uses' => 'WellDataCoringController@delete']);
	Route::post('/welldatacoring/{id}/download', ['as' => 'editor.welldatacoring.download', 'uses' => 'WellDataCoringController@download']);

	//Well Analysis Cutting
	Route::get('/wellanalysiscutting/{tid}', ['as' => 'editor.wellanalysiscutting.index', 'uses' => 'WellAnalysisCuttingController@index']);
	Route::post('/wellanalysiscutting/{tid}', ['as' => 'editor.wellanalysiscutting.store', 'uses' => 'WellAnalysisCuttingController@store']);
	Route::put('/wellanalysiscutting/{tid}/{id}/update', ['as' => 'editor.wellanalysiscutting.update', 'uses' => 'WellAnalysisCuttingController@update']);
	Route::delete('/wellanalysiscutting/{id}/delete', ['as' => 'editor.wellanalysiscutting.delete', 'uses' => 'WellAnalysisCuttingController@delete']);
	Route::post('/wellanalysiscutting/{id}/download', ['as' => 'editor.wellanalysiscutting.download', 'uses' => 'WellAnalysisCuttingController@download']);

	//Well Analysis Logging
	Route::get('/wellanalysislogging/{tid}', ['as' => 'editor.wellanalysislogging.index', 'uses' => 'WellAnalysisLoggingController@index']);
	Route::post('/wellanalysislogging/{tid}', ['as' => 'editor.wellanalysislogging.store', 'uses' => 'WellAnalysisLoggingController@store']);
	Route::put('/wellanalysislogging/{tid}/{id}/update', ['as' => 'editor.wellanalysislogging.update', 'uses' => 'WellAnalysisLoggingController@update']);
	Route::delete('/wellanalysislogging/{id}/delete', ['as' => 'editor.wellanalysislogging.delete', 'uses' => 'WellAnalysisLoggingController@delete']);
	Route::post('/wellanalysislogging/{id}/download', ['as' => 'editor.wellanalysislogging.download', 'uses' => 'WellAnalysisLoggingController@download']);

	//Well Analysis Coring
	Route::get('/wellanalysiscoring/{tid}', ['as' => 'editor.wellanalysiscoring.index', 'uses' => 'WellAnalysisCoringController@index']);
	Route::post('/wellanalysiscoring/{tid}', ['as' => 'editor.wellanalysiscoring.store', 'uses' => 'WellAnalysisCoringController@store']);
	Route::put('/wellanalysiscoring/{tid}/{id}/update', ['as' => 'editor.wellanalysiscoring.update', 'uses' => 'WellAnalysisCoringController@update']);
	Route::delete('/wellanalysiscoring/{id}/delete', ['as' => 'editor.wellanalysiscoring.delete', 'uses' => 'WellAnalysisCoringController@delete']);
	Route::post('/wellanalysiscoring/{id}/download', ['as' => 'editor.wellanalysiscoring.download', 'uses' => 'WellAnalysisCoringController@download']);

	//GEOCHEMISTRY
	//Sampling Data Well
	Route::get('/gcsamplingwell', ['as' => 'editor.gcsampling.indexWell', 'uses' => 'GcSamplingDataController@indexWell']);
	Route::post('/gcsamplingwell', ['as' => 'editor.gcsampling.storeWell', 'uses' => 'GcSamplingDataController@storeWell']);
	Route::put('/gcsamplingwell/{id}/update', ['as' => 'editor.gcsampling.updateWell', 'uses' => 'GcSamplingDataController@updateWell']);
	Route::delete('/gcsamplingwell/{id}/delete', ['as' => 'editor.gcsampling.deleteWell', 'uses' => 'GcSamplingDataController@deleteWell']);
	Route::put('/gcsamplingwell/{id}/approve', ['as' => 'editor.gcsampling.approveWell', 'uses' => 'GcSamplingDataController@approveWell']);

	//Sampling Data Well History
	Route::get('/gcsamplingwell/{wid}/history', ['as' => 'editor.gcsamplinghistory.indexWell', 'uses' => 'GcSamplingDataHistoryController@indexWell']);
	Route::post('/gcsamplingwell/{wid}/history', ['as' => 'editor.gcsamplinghistory.storeWell', 'uses' => 'GcSamplingDataHistoryController@storeWell']);
	Route::put('/gcsamplingwell/{wid}/history/{did}/update', ['as' => 'editor.gcsamplinghistory.updateWell', 'uses' => 'GcSamplingDataHistoryController@updateWell']);
	Route::delete('/gcsamplingwell/{wid}/history/{did}/delete', ['as' => 'editor.gcsamplinghistory.deleteWell', 'uses' => 'GcSamplingDataHistoryController@deleteWell']);
	Route::put('/gcsamplingwell/{wid}/history/{did}/approve', ['as' => 'editor.gcsamplinghistory.approveWell', 'uses' => 'GcSamplingDataHistoryController@approveWell']);

	//Sampling Data SF
	Route::get('/gcsamplingsf', ['as' => 'editor.gcsampling.indexSf', 'uses' => 'GcSamplingDataController@indexSf']);
	Route::post('/gcsamplingsf', ['as' => 'editor.gcsampling.storeSf', 'uses' => 'GcSamplingDataController@storeSf']);
	Route::put('/gcsamplingsf/{id}/update', ['as' => 'editor.gcsampling.updateSf', 'uses' => 'GcSamplingDataController@updateSf']);
	Route::delete('/gcsamplingsf/{id}/delete', ['as' => 'editor.gcsampling.deleteSf', 'uses' => 'GcSamplingDataController@deleteSf']);
	Route::put('/gcsamplingsf/{id}/approve', ['as' => 'editor.gcsampling.approveSf', 'uses' => 'GcSamplingDataController@approveSf']);

	//Sampling Data SF History
	Route::get('/gcsamplingsf/{sfid}/history', ['as' => 'editor.gcsamplinghistory.indexSf', 'uses' => 'GcSamplingDataHistoryController@indexSf']);
	Route::post('/gcsamplingsf/{sfid}/history', ['as' => 'editor.gcsamplinghistory.storeSf', 'uses' => 'GcSamplingDataHistoryController@storeSf']);
	Route::put('/gcsamplingsf/{sfid}/history/{did}/update', ['as' => 'editor.gcsamplinghistory.updateSf', 'uses' => 'GcSamplingDataHistoryController@updateSf']);
	Route::delete('/gcsamplingsf/{sfid}/history/{did}/delete', ['as' => 'editor.gcsamplinghistory.deleteSf', 'uses' => 'GcSamplingDataHistoryController@deleteSf']);
	Route::put('/gcsamplingsf/{sfid}/history/{did}/approve', ['as' => 'editor.gcsamplinghistory.approveSf', 'uses' => 'GcSamplingDataHistoryController@approveSf']);

	//NCG Data Well
	Route::get('/gcncgwell', ['as' => 'editor.gcncg.indexWell', 'uses' => 'GcNcgDataController@indexWell']);
	Route::post('/gcncgwell', ['as' => 'editor.gcncg.storeWell', 'uses' => 'GcNcgDataController@storeWell']);
	Route::put('/gcncgwell/{id}/update', ['as' => 'editor.gcncg.updateWell', 'uses' => 'GcNcgDataController@updateWell']);
	Route::delete('/gcncgwell/{id}/delete', ['as' => 'editor.gcncg.deleteWell', 'uses' => 'GcNcgDataController@deleteWell']);

	//NCG Data Well History
	Route::get('/gcncgwell/{wid}/history', ['as' => 'editor.gcncghistory.indexWell', 'uses' => 'GcNcgDataHistoryController@indexWell']);
	Route::post('/gcncgwell/{wid}/history', ['as' => 'editor.gcncghistory.storeWell', 'uses' => 'GcNcgDataHistoryController@storeWell']);
	Route::put('/gcncgwell/{wid}/history/{did}/update', ['as' => 'editor.gcncghistory.updateWell', 'uses' => 'GcNcgDataHistoryController@updateWell']);
	Route::delete('/gcncgwell/{wid}/history/{did}/delete', ['as' => 'editor.gcncghistory.deleteWell', 'uses' => 'GcNcgDataHistoryController@deleteWell']);
	Route::put('/gcncgwell/{wid}/history/{did}/approve', ['as' => 'editor.gcncghistory.approveWell', 'uses' => 'GcNcgDataHistoryController@approveWell']);

	//NCG Data SF
	Route::get('/gcncgsf', ['as' => 'editor.gcncg.indexSf', 'uses' => 'GcNcgDataController@indexSf']);
	Route::post('/gcncgsf', ['as' => 'editor.gcncg.storeSf', 'uses' => 'GcNcgDataController@storeSf']);
	Route::put('/gcncgsf/{id}/update', ['as' => 'editor.gcncg.updateSf', 'uses' => 'GcNcgDataController@updateSf']);
	Route::delete('/gcncgsf/{id}/delete', ['as' => 'editor.gcncg.deleteSf', 'uses' => 'GcNcgDataController@deleteSf']);

	//NCG Data SF History
	Route::get('/gcncgsf/{sfid}/history', ['as' => 'editor.gcncghistory.indexSf', 'uses' => 'GcNcgDataHistoryController@indexSf']);
	Route::post('/gcncgsf/{sfid}/history', ['as' => 'editor.gcncghistory.storeSf', 'uses' => 'GcNcgDataHistoryController@storeSf']);
	Route::put('/gcncgsf/{sfid}/history/{did}/update', ['as' => 'editor.gcncghistory.updateSf', 'uses' => 'GcNcgDataHistoryController@updateSf']);
	Route::delete('/gcncgsf/{sfid}/history/{did}/delete', ['as' => 'editor.gcncghistory.deleteSf', 'uses' => 'GcNcgDataHistoryController@deleteSf']);
	Route::put('/gcncgsf/{sfid}/history/{did}/approve', ['as' => 'editor.gcncghistory.approveSf', 'uses' => 'GcNcgDataHistoryController@approveSf']);

	//Brine Data Well
	Route::get('/gcbrinewell', ['as' => 'editor.gcbrine.indexWell', 'uses' => 'GcBrineDataController@indexWell']);
	Route::post('/gcbrinewell', ['as' => 'editor.gcbrine.storeWell', 'uses' => 'GcBrineDataController@storeWell']);
	Route::put('/gcbrinewell/{id}/update', ['as' => 'editor.gcbrine.updateWell', 'uses' => 'GcBrineDataController@updateWell']);
	Route::delete('/gcbrinewell/{id}/delete', ['as' => 'editor.gcbrine.deleteWell', 'uses' => 'GcBrineDataController@deleteWell']);
	Route::put('/gcbrinewell/{id}/approve', ['as' => 'editor.gcbrine.approveWell', 'uses' => 'GcBrineDataController@approveWell']);
	//brine baru
	Route::post('/gcbrinewell/getSampling', ['as' => 'editor.gcbrine.getSampling', 'uses' => 'GcBrineDataController@getSampling']);

	//Brine Data Well History
	Route::get('/gcbrinewell/{wid}/history', ['as' => 'editor.gcbrinehistory.indexWell', 'uses' => 'GcBrineDataHistoryController@indexWell']);
	Route::post('/gcbrinewell/{wid}/history', ['as' => 'editor.gcbrinehistory.storeWell', 'uses' => 'GcBrineDataHistoryController@storeWell']);
	Route::put('/gcbrinewell/{wid}/history/{did}/update', ['as' => 'editor.gcbrinehistory.updateWell', 'uses' => 'GcBrineDataHistoryController@updateWell']);
	Route::delete('/gcbrinewell/{wid}/history/{did}/delete', ['as' => 'editor.gcbrinehistory.deleteWell', 'uses' => 'GcBrineDataHistoryController@deleteWell']);
	Route::put('/gcbrinewell/{wid}/history/{did}/approve', ['as' => 'editor.gcbrinehistory.approveWell', 'uses' => 'GcBrineDataHistoryController@approveWell']);

	//Brine Data SF
	Route::get('/gcbrinesf', ['as' => 'editor.gcbrine.indexSf', 'uses' => 'GcBrineDataController@indexSf']);
	Route::post('/gcbrinesf', ['as' => 'editor.gcbrine.storeSf', 'uses' => 'GcBrineDataController@storeSf']);
	Route::put('/gcbrinesf/{id}/update', ['as' => 'editor.gcbrine.updateSf', 'uses' => 'GcBrineDataController@updateSf']);
	Route::delete('/gcbrinesf/{id}/delete', ['as' => 'editor.gcbrine.deleteSf', 'uses' => 'GcBrineDataController@deleteSf']);
	Route::put('/gcbrinesf/{id}/approve', ['as' => 'editor.gcbrine.approveSf', 'uses' => 'GcBrineDataController@approveSf']);

	//Brine Data SF History
	Route::get('/gcbrinesf/{sfid}/history', ['as' => 'editor.gcbrinehistory.indexSf', 'uses' => 'GcBrineDataHistoryController@indexSf']);
	Route::post('/gcbrinesf/{sfid}/history', ['as' => 'editor.gcbrinehistory.storeSf', 'uses' => 'GcBrineDataHistoryController@storeSf']);
	Route::put('/gcbrinesf/{sfid}/history/{did}/update', ['as' => 'editor.gcbrinehistory.updateSf', 'uses' => 'GcBrineDataHistoryController@updateSf']);
	Route::delete('/gcbrinesf/{sfid}/history/{did}/delete', ['as' => 'editor.gcbrinehistory.deleteSf', 'uses' => 'GcBrineDataHistoryController@deleteSf']);
	Route::put('/gcbrinesf/{sfid}/history/{did}/approve', ['as' => 'editor.gcbrinehistory.approveSf', 'uses' => 'GcBrineDataHistoryController@approveSf']);

	//Condensate Data Well
	Route::get('/gccondensatewell', ['as' => 'editor.gccondensate.indexWell', 'uses' => 'GcCondensateDataController@indexWell']);
	Route::post('/gccondensatewell', ['as' => 'editor.gccondensate.storeWell', 'uses' => 'GcCondensateDataController@storeWell']);
	Route::put('/gccondensatewell/{id}/update', ['as' => 'editor.gccondensate.updateWell', 'uses' => 'GcCondensateDataController@updateWell']);
	Route::delete('/gccondensatewell/{id}/delete', ['as' => 'editor.gccondensate.deleteWell', 'uses' => 'GcCondensateDataController@deleteWell']);
	//Condensate baru
	Route::post('/gccondensatewell/getSampling', ['as' => 'editor.gccondensate.getSampling', 'uses' => 'GcCondensateDataController@getSampling']);

	//Condensate Data Well History
	Route::get('/gccondensatewell/{wid}/history', ['as' => 'editor.gccondensatehistory.indexWell', 'uses' => 'GcCondensateDataHistoryController@indexWell']);
	Route::post('/gccondensatewell/{wid}/history', ['as' => 'editor.gccondensatehistory.storeWell', 'uses' => 'GcCondensateDataHistoryController@storeWell']);
	Route::put('/gccondensatewell/{wid}/history/{did}/update', ['as' => 'editor.gccondensatehistory.updateWell', 'uses' => 'GcCondensateDataHistoryController@updateWell']);
	Route::delete('/gccondensatewell/{wid}/history/{did}/delete', ['as' => 'editor.gccondensatehistory.deleteWell', 'uses' => 'GcCondensateDataHistoryController@deleteWell']);
	Route::put('/gccondensatewell/{wid}/history/{did}/approve', ['as' => 'editor.gccondensatehistory.approveWell', 'uses' => 'GcCondensateDataHistoryController@approveWell']);

	//Condensate Data SF
	Route::get('/gccondensatesf', ['as' => 'editor.gccondensate.indexSf', 'uses' => 'GcCondensateDataController@indexSf']);
	Route::post('/gccondensatesf', ['as' => 'editor.gccondensate.storeSf', 'uses' => 'GcCondensateDataController@storeSf']);
	Route::put('/gccondensatesf/{id}/update', ['as' => 'editor.gccondensate.updateSf', 'uses' => 'GcCondensateDataController@updateSf']);
	Route::delete('/gccondensatesf/{id}/delete', ['as' => 'editor.gccondensate.deleteSf', 'uses' => 'GcCondensateDataController@deleteSf']);

	//Condensate Data SF History
	Route::get('/gccondensatesf/{sfid}/history', ['as' => 'editor.gccondensatehistory.indexSf', 'uses' => 'GcCondensateDataHistoryController@indexSf']);
	Route::post('/gccondensatesf/{sfid}/history', ['as' => 'editor.gccondensatehistory.storeSf', 'uses' => 'GcCondensateDataHistoryController@storeSf']);
	Route::put('/gccondensatesf/{sfid}/history/{did}/update', ['as' => 'editor.gccondensatehistory.updateSf', 'uses' => 'GcCondensateDataHistoryController@updateSf']);
	Route::delete('/gccondensatesf/{sfid}/history/{did}/delete', ['as' => 'editor.gccondensatehistory.deleteSf', 'uses' => 'GcCondensateDataHistoryController@deleteSf']);
	Route::put('/gccondensatesf/{sfid}/history/{did}/approve', ['as' => 'editor.gccondensatehistory.approveSf', 'uses' => 'GcCondensateDataHistoryController@approveSf']);

	//Weirbox Data Well
	Route::get('/gcweirboxwell', ['as' => 'editor.gcweirbox.indexWell', 'uses' => 'GcWeirboxDataController@indexWell']);
	Route::post('/gcweirboxwell', ['as' => 'editor.gcweirbox.storeWell', 'uses' => 'GcWeirboxDataController@storeWell']);
	Route::put('/gcweirboxwell/{id}/update', ['as' => 'editor.gcweirbox.updateWell', 'uses' => 'GcWeirboxDataController@updateWell']);
	Route::delete('/gcweirboxwell/{id}/delete', ['as' => 'editor.gcweirbox.deleteWell', 'uses' => 'GcWeirboxDataController@deleteWell']);
	//weirbox baru
	Route::post('/gcweirboxwell/getSampling', ['as' => 'editor.gcweirbox.getSampling', 'uses' => 'GcWeirboxDataController@getSampling']);

	//Weirbox Data Well History
	Route::get('/gcweirboxwell/{wid}/history', ['as' => 'editor.gcweirboxhistory.indexWell', 'uses' => 'GcWeirboxDataHistoryController@indexWell']);
	Route::post('/gcweirboxwell/{wid}/history', ['as' => 'editor.gcweirboxhistory.storeWell', 'uses' => 'GcWeirboxDataHistoryController@storeWell']);
	Route::put('/gcweirboxwell/{wid}/history/{did}/update', ['as' => 'editor.gcweirboxhistory.updateWell', 'uses' => 'GcWeirboxDataHistoryController@updateWell']);
	Route::delete('/gcweirboxwell/{wid}/history/{did}/delete', ['as' => 'editor.gcweirboxhistory.deleteWell', 'uses' => 'GcWeirboxDataHistoryController@deleteWell']);
	Route::put('/gcweirboxwell/{wid}/history/{did}/approve', ['as' => 'editor.gcweirboxhistory.approveWell', 'uses' => 'GcWeirboxDataHistoryController@approveWell']);

	//Weirbox Data SF
	Route::get('/gcweirboxsf', ['as' => 'editor.gcweirbox.indexSf', 'uses' => 'GcWeirboxDataController@indexSf']);
	Route::post('/gcweirboxsf', ['as' => 'editor.gcweirbox.storeSf', 'uses' => 'GcWeirboxDataController@storeSf']);
	Route::put('/gcweirboxsf/{id}/update', ['as' => 'editor.gcweirbox.updateSf', 'uses' => 'GcWeirboxDataController@updateSf']);
	Route::delete('/gcweirboxsf/{id}/delete', ['as' => 'editor.gcweirbox.deleteSf', 'uses' => 'GcWeirboxDataController@deleteSf']);

	//Weirbox Data SF History
	Route::get('/gcweirboxsf/{sfid}/history', ['as' => 'editor.gcweirboxhistory.indexSf', 'uses' => 'GcWeirboxDataHistoryController@indexSf']);
	Route::post('/gcweirboxsf/{sfid}/history', ['as' => 'editor.gcweirboxhistory.storeSf', 'uses' => 'GcWeirboxDataHistoryController@storeSf']);
	Route::put('/gcweirboxsf/{sfid}/history/{did}/update', ['as' => 'editor.gcweirboxhistory.updateSf', 'uses' => 'GcWeirboxDataHistoryController@updateSf']);
	Route::delete('/gcweirboxsf/{sfid}/history/{did}/delete', ['as' => 'editor.gcweirboxhistory.deleteSf', 'uses' => 'GcWeirboxDataHistoryController@deleteSf']);
	Route::put('/gcweirboxsf/{sfid}/history/{did}/approve', ['as' => 'editor.gcweirboxhistory.approveSf', 'uses' => 'GcWeirboxDataHistoryController@approveSf']);

	//Isotope Data Well
	Route::get('/gcisotopewell', ['as' => 'editor.gcisotope.indexWell', 'uses' => 'GcIsotopeDataController@indexWell']);
	Route::post('/gcisotopewell', ['as' => 'editor.gcisotope.storeWell', 'uses' => 'GcIsotopeDataController@storeWell']);
	Route::put('/gcisotopewell/{id}/update', ['as' => 'editor.gcisotope.updateWell', 'uses' => 'GcIsotopeDataController@updateWell']);
	Route::delete('/gcisotopewell/{id}/delete', ['as' => 'editor.gcisotope.deleteWell', 'uses' => 'GcIsotopeDataController@deleteWell']);
	//Isotope baru
	Route::post('/gcisotopewell/getSampling', ['as' => 'editor.gcisotope.getSampling', 'uses' => 'GcIsotopeDataController@getSampling']);
	//Weirbox Data Well History
	Route::get('/gcisotopewell/{wid}/history', ['as' => 'editor.gcisotopehistory.indexWell', 'uses' => 'GcIsotopeDataHistoryController@indexWell']);
	Route::post('/gcisotopewell/{wid}/history', ['as' => 'editor.gcisotopehistory.storeWell', 'uses' => 'GcIsotopeDataHistoryController@storeWell']);
	Route::put('/gcisotopewell/{wid}/history/{did}/update', ['as' => 'editor.gcisotopehistory.updateWell', 'uses' => 'GcIsotopeDataHistoryController@updateWell']);
	Route::delete('/gcisotopewell/{wid}/history/{did}/delete', ['as' => 'editor.gcisotopehistory.deleteWell', 'uses' => 'GcIsotopeDataHistoryController@deleteWell']);
	Route::put('/gcisotopewell/{wid}/history/{did}/approve', ['as' => 'editor.gcisotopehistory.approveWell', 'uses' => 'GcIsotopeDataHistoryController@approveWell']);

	//Isotope Data SF
	Route::get('/gcisotopesf', ['as' => 'editor.gcisotope.indexSf', 'uses' => 'GcIsotopeDataController@indexSf']);
	Route::post('/gcisotopesf', ['as' => 'editor.gcisotope.storeSf', 'uses' => 'GcIsotopeDataController@storeSf']);
	Route::put('/gcisotopesf/{id}/update', ['as' => 'editor.gcisotope.updateSf', 'uses' => 'GcIsotopeDataController@updateSf']);
	Route::delete('/gcisotopesf/{id}/delete', ['as' => 'editor.gcisotope.deleteSf', 'uses' => 'GcIsotopeDataController@deleteSf']);

	//Isotope Data SF History
	Route::get('/gcisotopesf/{sfid}/history', ['as' => 'editor.gcisotopehistory.indexSf', 'uses' => 'GcIsotopeDataHistoryController@indexSf']);
	Route::post('/gcisotopesf/{sfid}/history', ['as' => 'editor.gcisotopehistory.storeSf', 'uses' => 'GcIsotopeDataHistoryController@storeSf']);
	Route::put('/gcisotopesf/{sfid}/history/{did}/update', ['as' => 'editor.gcisotopehistory.updateSf', 'uses' => 'GcIsotopeDataHistoryController@updateSf']);
	Route::delete('/gcisotopesf/{sfid}/history/{did}/delete', ['as' => 'editor.gcisotopehistory.deleteSf', 'uses' => 'GcIsotopeDataHistoryController@deleteSf']);
	Route::put('/gcisotopesf/{sfid}/history/{did}/approve', ['as' => 'editor.gcisotopehistory.approveSf', 'uses' => 'GcIsotopeDataHistoryController@approveSf']);

	//SF Mointoring
	Route::get('/gcsfmonitor', ['as' => 'editor.gcsfmonitor.index', 'uses' => 'GcSfMonitorDataController@index']);
	Route::post('/gcsfmonitor', ['as' => 'editor.gcsfmonitor.store', 'uses' => 'GcSfMonitorDataController@store']);
	Route::put('/gcsfmonitor/{id}/update', ['as' => 'editor.gcsfmonitor.update', 'uses' => 'GcSfMonitorDataController@update']);
	Route::delete('/gcsfmonitor/{id}/delete', ['as' => 'editor.gcsfmonitor.delete', 'uses' => 'GcSfMonitorDataController@delete']);

	//SF Mointoring History
	Route::get('/gcsfmonitor/{sfid}/history', ['as' => 'editor.gcsfmonitorhistory.index', 'uses' => 'GcSfMonitorDataHistoryController@index']);
	Route::post('/gcsfmonitor/{sfid}/history', ['as' => 'editor.gcsfmonitorhistory.store', 'uses' => 'GcSfMonitorDataHistoryController@store']);
	Route::put('/gcsfmonitor/{sfid}/history/{did}/update', ['as' => 'editor.gcsfmonitorhistory.update', 'uses' => 'GcSfMonitorDataHistoryController@update']);
	Route::delete('/gcsfmonitor/{sfid}/history/{did}/delete', ['as' => 'editor.gcsfmonitorhistory.delete', 'uses' => 'GcSfMonitorDataHistoryController@delete']);
	Route::put('/gcsfmonitor/{sfid}/history/{did}/approve', ['as' => 'editor.gcsfmonitorhistory.approve', 'uses' => 'GcSfMonitorDataHistoryController@approve']);

	//Steam Quality Data
	Route::get('/gcsteam', ['as' => 'editor.gcsteam.index', 'uses' => 'GcSteamDataController@index']);
	Route::post('/gcsteam', ['as' => 'editor.gcsteam.store', 'uses' => 'GcSteamDataController@store']);
	Route::put('/gcsteam/{id}/update', ['as' => 'editor.gcsteam.update', 'uses' => 'GcSteamDataController@update']);
	Route::delete('/gcsteam/{id}/delete', ['as' => 'editor.gcsteam.delete', 'uses' => 'GcSteamDataController@delete']);

	//Steam Quality Data History
	Route::get('/gcsteam/{sfid}/history', ['as' => 'editor.gcsteamhistory.index', 'uses' => 'GcSteamDataHistoryController@index']);
	Route::post('/gcsteam/{sfid}/history', ['as' => 'editor.gcsteamhistory.store', 'uses' => 'GcSteamDataHistoryController@store']);
	Route::put('/gcsteam/{sfid}/history/{did}/update', ['as' => 'editor.gcsteamhistory.update', 'uses' => 'GcSteamDataHistoryController@update']);
	Route::delete('/gcsteam/{sfid}/history/{did}/delete', ['as' => 'editor.gcsteamhistory.delete', 'uses' => 'GcSteamDataHistoryController@delete']);
	Route::put('/gcsteam/{sfid}/history/{did}/approve', ['as' => 'editor.gcsteamhistory.approve', 'uses' => 'GcSteamDataHistoryController@approve']);

	//Corrosion Coupon
	Route::get('/gccorrosioncoupon', ['as' => 'editor.gccorrosioncoupon.index', 'uses' => 'GcCorrosionCouponController@index']);
	Route::post('/gccorrosioncoupon', ['as' => 'editor.gccorrosioncoupon.store', 'uses' => 'GcCorrosionCouponController@store']);
	Route::put('/gccorrosioncoupon/{id}/update', ['as' => 'editor.gccorrosioncoupon.update', 'uses' => 'GcCorrosionCouponController@update']);
	Route::delete('/gccorrosioncoupon/{id}/delete', ['as' => 'editor.gccorrosioncoupon.delete', 'uses' => 'GcCorrosionCouponController@delete']);

	//Corrosion Coupon History
	Route::get('/gccorrosioncoupon/{sfid}/history', ['as' => 'editor.gccorrosioncouponhistory.index', 'uses' => 'GcCorrosionCouponHistoryController@index']);
	Route::post('/gccorrosioncoupon/{sfid}/history', ['as' => 'editor.gccorrosioncouponhistory.store', 'uses' => 'GcCorrosionCouponHistoryController@store']);
	Route::put('/gccorrosioncoupon/{sfid}/history/{did}/update', ['as' => 'editor.gccorrosioncouponhistory.update', 'uses' => 'GcCorrosionCouponHistoryController@update']);
	Route::delete('/gccorrosioncoupon/{sfid}/history/{did}/delete', ['as' => 'editor.gccorrosioncouponhistory.delete', 'uses' => 'GcCorrosionCouponHistoryController@delete']);
	Route::put('/gccorrosioncoupon/{sfid}/history/{did}/approve', ['as' => 'editor.gccorrosioncouponhistory.approve', 'uses' => 'GcCorrosionCouponHistoryController@approve']);

	//Mineral Saturation
	Route::get('/gcmineralsat', ['as' => 'editor.gcmineralsat.index', 'uses' => 'GcMineralSatDataController@index']);
	Route::post('/gcmineralsat', ['as' => 'editor.gcmineralsat.store', 'uses' => 'GcMineralSatDataController@store']);
	Route::put('/gcmineralsat/{id}/update', ['as' => 'editor.gcmineralsat.update', 'uses' => 'GcMineralSatDataController@update']);
	Route::delete('/gcmineralsat/{id}/delete', ['as' => 'editor.gcmineralsat.delete', 'uses' => 'GcMineralSatDataController@delete']);

	//Mineral Saturation History
	Route::get('/gcmineralsat/{wid}/history', ['as' => 'editor.gcmineralsathistory.index', 'uses' => 'GcMineralSatDataHistoryController@index']);
	Route::post('/gcmineralsat/{wid}/history', ['as' => 'editor.gcmineralsathistory.store', 'uses' => 'GcMineralSatDataHistoryController@store']);
	Route::put('/gcmineralsat/{wid}/history/{did}/update', ['as' => 'editor.gcmineralsathistory.update', 'uses' => 'GcMineralSatDataHistoryController@update']);
	Route::delete('/gcmineralsat/{wid}/history/{did}/delete', ['as' => 'editor.gcmineralsathistory.delete', 'uses' => 'GcMineralSatDataHistoryController@delete']);
	Route::put('/gcmineralsat/{wid}/history/{did}/approve', ['as' => 'editor.gcmineralsathistory.approve', 'uses' => 'GcMineralSatDataHistoryController@approve']);

	//Ion Balance Well
	Route::get('/gcionbalancewell', ['as' => 'editor.gcionbalance.indexWell', 'uses' => 'GcIonBalanceController@indexWell']);

	//Ion Balance SF
	Route::get('/gcionbalancesf', ['as' => 'editor.gcionbalance.indexSf', 'uses' => 'GcIonBalanceController@indexSf']);

	//Reservoir Condition well
	Route::get('/gcrsvcondt', ['as' => 'editor.gcrsvcondt.index', 'uses' => 'GcRsvCondtController@index']);
	Route::post('/gcrsvcondt/getSampling', ['as' => 'editor.gcrsvcondt.getSampling', 'uses' => 'GcRsvCondtController@getSampling']);
	Route::post('/gcrsvcondt/getStatus', ['as' => 'editor.gcrsvcondt.getStatus', 'uses' => 'GcRsvCondtController@getStatus']);
	Route::post('/gcrsvcondt/getStatusDtl', ['as' => 'editor.gcrsvcondt.getStatusDtl', 'uses' => 'GcRsvCondtController@getStatusDtl']);
	Route::post('/gcrsvcondt/SubmitWell', ['as' => 'editor.gcrsvcondt.SubmitWell', 'uses' => 'GcRsvCondtController@SubmitWell']);

	//Reservoir Condition sf
	Route::get('/gcrsvcondtsf', ['as' => 'editor.gcrsvcondt.indexsf', 'uses' => 'GcRsvCondtController@indexsf']);
	Route::post('/gcrsvcondt/SubmitSf', ['as' => 'editor.gcrsvcondt.SubmitSf', 'uses' => 'GcRsvCondtController@SubmitSf']);

	//GEOPHYSIC
	//MT Report
	Route::get('/gpmtreport', ['as' => 'editor.gpmtreport.index', 'uses' => 'GpMtReportController@index']);
	Route::post('/gpmtreport', ['as' => 'editor.gpmtreport.store', 'uses' => 'GpMtReportController@store']);
	Route::put('/gpmtreport/{id}/update', ['as' => 'editor.gpmtreport.update', 'uses' => 'GpMtReportController@update']);
	Route::delete('/gpmtreport/{id}/delete', ['as' => 'editor.gpmtreport.delete', 'uses' => 'GpMtReportController@delete']);
	Route::post('/gpmtreport/{id}/download', ['as' => 'editor.gpmtreport.download', 'uses' => 'GpMtReportController@download']);

	//MT Station
	Route::get('/gpmtstation', ['as' => 'editor.gpmtstation.index', 'uses' => 'GpMtStationController@index']);
	Route::post('/gpmtstation', ['as' => 'editor.gpmtstation.store', 'uses' => 'GpMtStationController@store']);
	Route::put('/gpmtstation/{id}/update', ['as' => 'editor.gpmtstation.update', 'uses' => 'GpMtStationController@update']);
	Route::delete('/gpmtstation/{id}/delete', ['as' => 'editor.gpmtstation.delete', 'uses' => 'GpMtStationController@delete']);
	Route::post('/gpmtstation/{id}/download', ['as' => 'editor.gpmtstation.download', 'uses' => 'GpMtStationController@download']);

	//MT Project Files
	Route::get('/gpmtproject', ['as' => 'editor.gpmtproject.index', 'uses' => 'GpMtProjectController@index']);
	Route::post('/gpmtproject', ['as' => 'editor.gpmtproject.store', 'uses' => 'GpMtProjectController@store']);
	Route::put('/gpmtproject/{id}/update', ['as' => 'editor.gpmtproject.update', 'uses' => 'GpMtProjectController@update']);
	Route::delete('/gpmtproject/{id}/delete', ['as' => 'editor.gpmtproject.delete', 'uses' => 'GpMtProjectController@delete']);
	Route::post('/gpmtproject/{id}/download', ['as' => 'editor.gpmtproject.download', 'uses' => 'GpMtProjectController@download']);

	//MT Resistivity Map
	Route::get('/gpmtresmp', ['as' => 'editor.gpmtres.indexMp', 'uses' => 'GpMtResController@indexMp']);
	Route::post('/gpmtresmp', ['as' => 'editor.gpmtres.storeMp', 'uses' => 'GpMtResController@storeMp']);
	Route::put('/gpmtresmp/{id}/update', ['as' => 'editor.gpmtres.updateMp', 'uses' => 'GpMtResController@updateMp']);
	Route::delete('/gpmtresmp/{id}/delete', ['as' => 'editor.gpmtres.deleteMp', 'uses' => 'GpMtResController@deleteMp']);
	Route::post('/gpmtresmp/{id}/download', ['as' => 'editor.gpmtres.downloadMp', 'uses' => 'GpMtResController@downloadMp']);

	//MT Resistivity Cross Section
	Route::get('/gpmtrescs', ['as' => 'editor.gpmtres.indexCs', 'uses' => 'GpMtResController@indexCs']);
	Route::post('/gpmtrescs', ['as' => 'editor.gpmtres.storeCs', 'uses' => 'GpMtResController@storeCs']);
	Route::put('/gpmtrescs/{id}/update', ['as' => 'editor.gpmtres.updateCs', 'uses' => 'GpMtResController@updateCs']);
	Route::delete('/gpmtrescs/{id}/delete', ['as' => 'editor.gpmtres.deleteCs', 'uses' => 'GpMtResController@deleteCs']);
	Route::post('/gpmtrescs/{id}/download', ['as' => 'editor.gpmtres.downloadCs', 'uses' => 'GpMtResController@downloadCs']);

	//MG Report
	Route::get('/gpmgreport', ['as' => 'editor.gpmgreport.index', 'uses' => 'GpMgReportController@index']);
	Route::post('/gpmgreport', ['as' => 'editor.gpmgreport.store', 'uses' => 'GpMgReportController@store']);
	Route::put('/gpmgreport/{id}/update', ['as' => 'editor.gpmgreport.update', 'uses' => 'GpMgReportController@update']);
	Route::delete('/gpmgreport/{id}/delete', ['as' => 'editor.gpmgreport.delete', 'uses' => 'GpMgReportController@delete']);
	Route::post('/gpmgreport/{id}/download', ['as' => 'editor.gpmgreport.download', 'uses' => 'GpMgReportController@download']);

	//MG Benchmark
	Route::get('/gpmgbench', ['as' => 'editor.gpmgbench.index', 'uses' => 'GpMgBenchController@index']);
	Route::post('/gpmgbench', ['as' => 'editor.gpmgbench.store', 'uses' => 'GpMgBenchController@store']);
	Route::put('/gpmgbench/{id}/update', ['as' => 'editor.gpmgbench.update', 'uses' => 'GpMgBenchController@update']);
	Route::delete('/gpmgbench/{id}/delete', ['as' => 'editor.gpmgbench.delete', 'uses' => 'GpMgBenchController@delete']);
	Route::post('/gpmgbench/{id}/download', ['as' => 'editor.gpmgbench.download', 'uses' => 'GpMgBenchController@download']);

	//MG Calibration
	Route::get('/gpmgcalib', ['as' => 'editor.gpmgcalib.index', 'uses' => 'GpMgCalibController@index']);
	Route::post('/gpmgcalib', ['as' => 'editor.gpmgcalib.store', 'uses' => 'GpMgCalibController@store']);
	Route::put('/gpmgcalib/{id}/update', ['as' => 'editor.gpmgcalib.update', 'uses' => 'GpMgCalibController@update']);
	Route::delete('/gpmgcalib/{id}/delete', ['as' => 'editor.gpmgcalib.delete', 'uses' => 'GpMgCalibController@delete']);
	Route::post('/gpmgcalib/{id}/download', ['as' => 'editor.gpmgcalib.download', 'uses' => 'GpMgCalibController@download']);

	//MG Calibration Detail
	Route::get('/gpmgcalib/{id}/detail', ['as' => 'editor.gpmgcalibdetail.index', 'uses' => 'GpMgCalibDetailController@index']);
	Route::post('/gpmgcalib/{id}/detail', ['as' => 'editor.gpmgcalibdetail.store', 'uses' => 'GpMgCalibDetailController@store']);
	Route::put('/gpmgcalib/{id}/detail/{did}/update', ['as' => 'editor.gpmgcalibdetail.update', 'uses' => 'GpMgCalibDetailController@update']);
	Route::delete('/gpmgcalib/{id}/detail/{did}/delete', ['as' => 'editor.gpmgcalibdetail.delete', 'uses' => 'GpMgCalibDetailController@delete']);

	//MG Acquisition
	Route::get('/gpmgacq', ['as' => 'editor.gpmgacq.index', 'uses' => 'GpMgAcqController@index']);
	Route::post('/gpmgacq', ['as' => 'editor.gpmgacq.store', 'uses' => 'GpMgAcqController@store']);
	Route::put('/gpmgacq/{id}/update', ['as' => 'editor.gpmgacq.update', 'uses' => 'GpMgAcqController@update']);
	Route::put('/gpmgacq/{id}/updatebench', ['as' => 'editor.gpmgacq.updatebench', 'uses' => 'GpMgAcqController@updateBench']);
	Route::delete('/gpmgacq/{id}/delete', ['as' => 'editor.gpmgacq.delete', 'uses' => 'GpMgAcqController@delete']);
	Route::delete('/gpmgacq/{id}/deletebench', ['as' => 'editor.gpmgacq.deletebench', 'uses' => 'GpMgAcqController@deleteBench']);

	//MG Acquisition Detail
	Route::get('/gpmgacq/{id}/detail', ['as' => 'editor.gpmgacqdetail.index', 'uses' => 'GpMgAcqDetailController@index']);
	Route::post('/gpmgacq/{id}/detail', ['as' => 'editor.gpmgacqdetail.store', 'uses' => 'GpMgAcqDetailController@store']);
	Route::put('/gpmgacq/{id}/detail/{did}/update', ['as' => 'editor.gpmgacqdetail.update', 'uses' => 'GpMgAcqDetailController@update']);
	Route::delete('/gpmgacq/{id}/detail/{did}/delete', ['as' => 'editor.gpmgacqdetail.delete', 'uses' => 'GpMgAcqDetailController@delete']);
	Route::post('/gpmgacq/{did}/detail/download', ['as' => 'editor.gpmgacqdetail.download', 'uses' => 'GpMgAcqDetailController@download']);

	//MG Processing
	Route::get('/gpmgprocess', ['as' => 'editor.gpmgprocess.index', 'uses' => 'GpMgProcessController@index']);
	Route::post('/gpmgprocess', ['as' => 'editor.gpmgprocess.store', 'uses' => 'GpMgProcessController@store']);
	Route::put('/gpmgprocess/{id}/update', ['as' => 'editor.gpmgprocess.update', 'uses' => 'GpMgProcessController@update']);
	Route::delete('/gpmgprocess/{id}/delete', ['as' => 'editor.gpmgprocess.delete', 'uses' => 'GpMgProcessController@delete']);

	//MG Processing Detail
	Route::get('/gpmgprocess/{id}/detail', ['as' => 'editor.gpmgprocessdetail.index', 'uses' => 'GpMgProcessDetailController@index']);
	Route::post('/gpmgprocess/{id}/detail', ['as' => 'editor.gpmgprocessdetail.store', 'uses' => 'GpMgProcessDetailController@store']);
	Route::put('/gpmgprocess/{id}/detail/{did}/update', ['as' => 'editor.gpmgprocessdetail.update', 'uses' => 'GpMgProcessDetailController@update']);
	Route::delete('/gpmgprocess/{id}/detail/{did}/delete', ['as' => 'editor.gpmgprocessdetail.delete', 'uses' => 'GpMgProcessDetailController@delete']);
	Route::post('/gpmgprocess/{did}/detail/download', ['as' => 'editor.gpmgprocessdetail.download', 'uses' => 'GpMgProcessDetailController@download']);

	//MG Leveling
	Route::get('/gpmglevel', ['as' => 'editor.gpmglevel.index', 'uses' => 'GpMgLevelController@index']);
	Route::post('/gpmglevel', ['as' => 'editor.gpmglevel.store', 'uses' => 'GpMgLevelController@store']);
	Route::put('/gpmglevel/{id}/update', ['as' => 'editor.gpmglevel.update', 'uses' => 'GpMgLevelController@update']);
	Route::delete('/gpmglevel/{id}/delete', ['as' => 'editor.gpmglevel.delete', 'uses' => 'GpMgLevelController@delete']);

	//MG Leveling Detail
	Route::get('/gpmglevel/{id}/detail', ['as' => 'editor.gpmgleveldetail.index', 'uses' => 'GpMgLevelDetailController@index']);
	Route::post('/gpmglevel/{id}/detail', ['as' => 'editor.gpmgleveldetail.store', 'uses' => 'GpMgLevelDetailController@store']);
	Route::put('/gpmglevel/{id}/detail/{did}/update', ['as' => 'editor.gpmgleveldetail.update', 'uses' => 'GpMgLevelDetailController@update']);
	Route::delete('/gpmglevel/{id}/detail/{did}/delete', ['as' => 'editor.gpmgleveldetail.delete', 'uses' => 'GpMgLevelDetailController@delete']);
	Route::post('/gpmglevel/{did}/detail/download', ['as' => 'editor.gpmgleveldetail.download', 'uses' => 'GpMgLevelDetailController@download']);

	//MEQ Report
	Route::get('/gpmeqreport', ['as' => 'editor.gpmeqreport.index', 'uses' => 'GpMeqReportController@index']);
	Route::post('/gpmeqreport', ['as' => 'editor.gpmeqreport.store', 'uses' => 'GpMeqReportController@store']);
	Route::put('/gpmeqreport/{id}/update', ['as' => 'editor.gpmeqreport.update', 'uses' => 'GpMeqReportController@update']);
	Route::delete('/gpmeqreport/{id}/delete', ['as' => 'editor.gpmeqreport.delete', 'uses' => 'GpMeqReportController@delete']);
	Route::post('/gpmeqreport/{id}/download', ['as' => 'editor.gpmeqreport.download', 'uses' => 'GpMeqReportController@download']);

	//MEQ Station
	Route::get('/gpmeqstation', ['as' => 'editor.gpmeqstation.index', 'uses' => 'GpMeqStationController@index']);
	Route::post('/gpmeqstation', ['as' => 'editor.gpmeqstation.store', 'uses' => 'GpMeqStationController@store']);
	Route::put('/gpmeqstation/{id}/update', ['as' => 'editor.gpmeqstation.update', 'uses' => 'GpMeqStationController@update']);
	Route::delete('/gpmeqstation/{id}/delete', ['as' => 'editor.gpmeqstation.delete', 'uses' => 'GpMeqStationController@delete']);
	Route::post('/gpmeqstation/{id}/download', ['as' => 'editor.gpmeqstation.download', 'uses' => 'GpMeqStationController@download']);

	//MEQ Event
	Route::get('/gpmeqevent', ['as' => 'editor.gpmeqevent.index', 'uses' => 'GpMeqEventController@index']);
	Route::post('/gpmeqevent', ['as' => 'editor.gpmeqevent.store', 'uses' => 'GpMeqEventController@store']);
	Route::put('/gpmeqevent/{id}/update', ['as' => 'editor.gpmeqevent.update', 'uses' => 'GpMeqEventController@update']);
	Route::delete('/gpmeqevent/{id}/delete', ['as' => 'editor.gpmeqevent.delete', 'uses' => 'GpMeqEventController@delete']);

	//MEQ Event Detail
	Route::get('/gpmeqevent/{id}/detail', ['as' => 'editor.gpmeqeventdetail.index', 'uses' => 'GpMeqEventDetailController@index']);
	Route::post('/gpmeqevent/{id}/detail', ['as' => 'editor.gpmeqeventdetail.store', 'uses' => 'GpMeqEventDetailController@store']);
	Route::put('/gpmeqevent/{id}/detail/{did}/update', ['as' => 'editor.gpmeqeventdetail.update', 'uses' => 'GpMeqEventDetailController@update']);
	Route::delete('/gpmeqevent/{id}/detail/{did}/delete', ['as' => 'editor.gpmeqeventdetail.delete', 'uses' => 'GpMeqEventDetailController@delete']);
	Route::post('/gpmeqevent/{did}/detail/download', ['as' => 'editor.gpmeqeventdetail.download', 'uses' => 'GpMeqEventDetailController@download']);

	//MEQ Velocity
	Route::get('/gpmeqvelocity/{tid}', ['as' => 'editor.gpmeqvelocity.index', 'uses' => 'GpMeqVelocityController@index']);
	Route::post('/gpmeqvelocity/{tid}', ['as' => 'editor.gpmeqvelocity.store', 'uses' => 'GpMeqVelocityController@store']);
	Route::put('/gpmeqvelocity/{tid}/{id}/update', ['as' => 'editor.gpmeqvelocity.update', 'uses' => 'GpMeqVelocityController@update']);
	Route::delete('/gpmeqvelocity/{id}/delete', ['as' => 'editor.gpmeqvelocity.delete', 'uses' => 'GpMeqVelocityController@delete']);
	Route::post('/gpmeqvelocity/{id}/download', ['as' => 'editor.gpmeqvelocity.download', 'uses' => 'GpMeqVelocityController@download']);

	//RESRVOIR
	//SM Daily Monitoring
	Route::get('/resmdaily', ['as' => 'editor.resmdaily.index', 'uses' => 'ReSmDailyController@index']);
	Route::post('/resmdaily', ['as' => 'editor.resmdaily.index', 'uses' => 'ReSmDailyController@index']);

	//SM Daily Well Production
	// Route::post('/resmdailywellprod', ['as' => 'editor.resmdaily.storeWellProd', 'uses' => 'ReSmDailyController@storeWellProd']);
	Route::put('/resmdailywellprod/{id}/update', ['as' => 'editor.resmdaily.updateWellProd', 'uses' => 'ReSmDailyController@updateWellProd']);
	Route::delete('/resmdailywellprod/{id}/delete', ['as' => 'editor.resmdaily.deleteWellProd', 'uses' => 'ReSmDailyController@deleteWellProd']);

	//SM Daily Well Injection
	// Route::post('/resmdailywellinj', ['as' => 'editor.resmdaily.storeWellInj', 'uses' => 'ReSmDailyController@storeWellInj']);
	Route::put('/resmdailywellinj/{id}/update', ['as' => 'editor.resmdaily.updateWellInj', 'uses' => 'ReSmDailyController@updateWellInj']);
	Route::delete('/resmdailywellinj/{id}/delete', ['as' => 'editor.resmdaily.deleteWellInj', 'uses' => 'ReSmDailyController@deleteWellInj']);

	//SM Daily SGS Separator (Steam Supply)
	// Route::post('/resmdailysgssep', ['as' => 'editor.resmdaily.storeSgsSep', 'uses' => 'ReSmDailyController@storeSgsSep']);
	Route::put('/resmdailysgssep/{id}/update', ['as' => 'editor.resmdaily.updateSgsSep', 'uses' => 'ReSmDailyController@updateSgsSep']);
	Route::delete('/resmdailysgssep/{id}/delete', ['as' => 'editor.resmdaily.deleteSgsSep', 'uses' => 'ReSmDailyController@deleteSgsSep']);

	//SM Production Test
	Route::get('/resmprodtest', ['as' => 'editor.resmprodtest.index', 'uses' => 'ReSmProdTestController@index']);
	Route::post('/resmprodtest', ['as' => 'editor.resmprodtest.store', 'uses' => 'ReSmProdTestController@store']);
	Route::put('/resmprodtest/{id}/update', ['as' => 'editor.resmprodtest.update', 'uses' => 'ReSmProdTestController@update']);
	Route::delete('/resmprodtest/{id}/delete', ['as' => 'editor.resmprodtest.delete', 'uses' => 'ReSmProdTestController@delete']);

	//SM Production Test History
	Route::get('/resmprodtest/{wid}/history', ['as' => 'editor.resmprodtesthistory.index', 'uses' => 'ReSmProdTestHistoryController@index']);
	Route::post('/resmprodtest/{wid}/history', ['as' => 'editor.resmprodtesthistory.store', 'uses' => 'ReSmProdTestHistoryController@store']);
	Route::put('/resmprodtest/{wid}/history/{did}/update', ['as' => 'editor.resmprodtesthistory.update', 'uses' => 'ReSmProdTestHistoryController@update']);
	Route::delete('/resmprodtest/{wid}/history/{did}/delete', ['as' => 'editor.resmprodtesthistory.delete', 'uses' => 'ReSmProdTestHistoryController@delete']);

	//SM Production Test Data
	Route::get('/resmprodtest/{id}/data', ['as' => 'editor.resmprodtestdata.index', 'uses' => 'ReSmProdTestDataController@index']);
	Route::post('/resmprodtest/{id}/data', ['as' => 'editor.resmprodtestdata.store', 'uses' => 'ReSmProdTestDataController@store']);
	Route::put('/resmprodtest/{id}/data/{did}/update', ['as' => 'editor.resmprodtestdata.update', 'uses' => 'ReSmProdTestDataController@update']);
	Route::delete('/resmprodtest/{id}/data/{did}/delete', ['as' => 'editor.resmprodtestdata.delete', 'uses' => 'ReSmProdTestDataController@delete']);

	//SM Injection Test
	Route::get('/resminjtest', ['as' => 'editor.resminjtest.index', 'uses' => 'ReSmInjTestController@index']);
	Route::post('/resminjtest', ['as' => 'editor.resminjtest.store', 'uses' => 'ReSmInjTestController@store']);
	Route::put('/resminjtest/{id}/update', ['as' => 'editor.resminjtest.update', 'uses' => 'ReSmInjTestController@update']);
	Route::delete('/resminjtest/{id}/delete', ['as' => 'editor.resminjtest.delete', 'uses' => 'ReSmInjTestController@delete']);

	//SM Injection Test History
	Route::get('/resminjtest/{wid}/history', ['as' => 'editor.resminjtesthistory.index', 'uses' => 'ReSmInjTestHistoryController@index']);
	Route::post('/resminjtest/{wid}/history', ['as' => 'editor.resminjtesthistory.store', 'uses' => 'ReSmInjTestHistoryController@store']);
	Route::put('/resminjtest/{wid}/history/{did}/update', ['as' => 'editor.resminjtesthistory.update', 'uses' => 'ReSmInjTestHistoryController@update']);
	Route::delete('/resminjtest/{wid}/history/{did}/delete', ['as' => 'editor.resminjtesthistory.delete', 'uses' => 'ReSmInjTestHistoryController@delete']);

	//SM Injection Test Data
	Route::get('/resminjtest/{id}/data', ['as' => 'editor.resminjtestdata.index', 'uses' => 'ReSmInjTestDataController@index']);
	Route::post('/resminjtest/{id}/data', ['as' => 'editor.resminjtestdata.store', 'uses' => 'ReSmInjTestDataController@store']);
	Route::put('/resminjtest/{id}/data/{did}/update', ['as' => 'editor.resminjtestdata.update', 'uses' => 'ReSmInjTestDataController@update']);
	Route::delete('/resminjtest/{id}/data/{did}/delete', ['as' => 'editor.resminjtestdata.delete', 'uses' => 'ReSmInjTestDataController@delete']);

	//DHP Data
	Route::get('/redhpdata', ['as' => 'editor.redhpdata.index', 'uses' => 'ReDhpDataController@index']);
	Route::post('/redhpdata', ['as' => 'editor.redhpdata.store', 'uses' => 'ReDhpDataController@store']);
	Route::put('/redhpdata/{id}/update', ['as' => 'editor.redhpdata.update', 'uses' => 'ReDhpDataController@update']);
	Route::delete('/redhpdata/{id}/delete', ['as' => 'editor.redhpdata.delete', 'uses' => 'ReDhpDataController@delete']);

	//DHP Data History
	Route::get('/redhpdata/{wid}/history', ['as' => 'editor.redhpdatahistory.index', 'uses' => 'ReDhpDataHistoryController@index']);
	Route::post('/redhpdata/{wid}/history', ['as' => 'editor.redhpdatahistory.store', 'uses' => 'ReDhpDataHistoryController@store']);
	Route::put('/redhpdata/{wid}/history/{did}/update', ['as' => 'editor.redhpdatahistory.update', 'uses' => 'ReDhpDataHistoryController@update']);
	Route::delete('/redhpdata/{wid}/history/{did}/delete', ['as' => 'editor.redhpdatahistory.delete', 'uses' => 'ReDhpDataHistoryController@delete']);

	//LM analisis sellection
	Route::get('/lm', ['as' => 'editor.lm.indexLm', 'uses' => 'LmController@index']);
	Route::get('/lm/insertVLm', ['as' => 'editor.lm.insertVLm', 'uses' => 'LmController@insertVLm']);
	Route::post('/lm/insert', ['as' => 'editor.lm.insert', 'uses' => 'LmController@insert']);
	Route::get('/lm/editVLm', ['as' => 'editor.lm.editVLm', 'uses' => 'LmController@editVLm']);
	Route::post('/lm/edit', ['as' => 'editor.lm.edit', 'uses' => 'LmController@edit']);
	Route::delete('/lm/{id}/delete', ['as' => 'editor.lm.delete', 'uses' => 'LmController@delete']);
	Route::post('/UploadWellcnd', ['as' => 'editor.lm.UploadWellcnd','uses' => 'LmController@UploadWellcnd']);
	Route::post('/UploadLogging', ['as' => 'editor.lm.UploadLogging','uses' => 'LmController@UploadLogging']);
	Route::post('/UploadActivity', ['as' => 'editor.lm.UploadActivity','uses' => 'LmController@UploadActivity']);
	Route::post('/lm/GetWellTrack', ['as' => 'editor.lm.GetWellTrack','uses' => 'LmController@GetWellTrack']);
	Route::post('/lm/GetReLogSurveyType', ['as' => 'editor.lm.GetReLogSurveyType','uses' => 'LmController@GetReLogSurveyType']);
	Route::post('/lm/GetReLogTool', ['as' => 'editor.lm.GetReLogTool','uses' => 'LmController@GetReLogTool']);
	Route::post('/lm/GetReLogCable', ['as' => 'editor.lm.GetReLogCable','uses' => 'LmController@GetReLogCable']);

	//GeocheMagic
    Route::get('/gm', ['as' => 'editor.gm.indexGm', 'uses' => 'GmController@indexGm']);
    Route::get('/gm/mod', ['as' => 'editor.gm.mod', 'uses' => 'GmController@indexMod']);
    Route::get('/gm/carhar', ['as' => 'editor.gm.indexcarhar', 'uses' => 'GmController@indexcarhar']);
    Route::get('/gm/ft', ['as' => 'editor.gm.indexFT', 'uses' => 'GmController@indexFT']);
    Route::get('/gm/ftco2', ['as' => 'editor.gm.indexftco2', 'uses' => 'GmController@indexftco2']);
    Route::get('/gm/fth2s', ['as' => 'editor.gm.indexfth2s', 'uses' => 'GmController@indexfth2s']);
    Route::get('/gm/cocochco', ['as' => 'editor.gm.indexcocochco', 'uses' => 'GmController@indexcocochco']);
    //COCOCHCO2
    Route::get('/gm/cocochcoviewstore', ['as' => 'editor.gm.cocochcoviewstore', 'uses' => 'GmController@cocochcoviewstore']);
    Route::post('/gm/ftco/upload01', ['as' => 'editor.gm.upload01','uses' => 'GmController@upload01']);
    Route::post('/gm/ftco/upload02', ['as' => 'editor.gm.upload02','uses' => 'GmController@upload02']);
    Route::post('/gm/cocochcostore', ['as' => 'editor.gm.cocochcostore', 'uses' => 'GmController@cocochcostore']);
    //FTCO2
    Route::get('/gm/ftcoviewstore', ['as' => 'editor.gm.ftcoviewstore', 'uses' => 'GmController@ftcoviewstore']);
    Route::post('/gm/ftcostore', ['as' => 'editor.gm.ftcostore', 'uses' => 'GmController@ftcostore']);
    Route::post('/gm/ftco/uploadftcolog', ['as' => 'editor.gm.uploadftcolog','uses' => 'GmController@uploadftcolog']);
    Route::post('/gm/ftco/uploadftcoval', ['as' => 'editor.gm.uploadftcoval','uses' => 'GmController@uploadftcoval']);
    Route::post('/gm/ftco/uploadco2log', ['as' => 'editor.gm.uploadco2log','uses' => 'GmController@uploadco2log']);
    Route::get('/gm/ftco/ftcoviewgraf', ['as' => 'editor.gm.ftcoviewgraf', 'uses' => 'GmController@ftcoviewgraf']);
    //Graft
    Route::get('/gm/NewGrafCreate', ['as' => 'editor.gm.NewGrafCreate', 'uses' => 'GmController@NewGrafCreate']);
    Route::get('/gm/NewGrafDataMart', ['as' => 'editor.gm.NewGrafDataMart', 'uses' => 'GmController@NewGrafDataMart']);
    //FT
    Route::get('/gm/FTViewstore', ['as' => 'editor.gm.FTViewstore', 'uses' => 'GmController@FTViewstore']);
    Route::post('/gm/FTstore', ['as' => 'editor.gm.FTstore', 'uses' => 'GmController@FTstore']);
    Route::post('/gm/FTstore/UploadFTLog', ['as' => 'editor.gm.UploadFTLog','uses' => 'GmController@UploadFTLog']);
	Route::post('/gm/FTstore/uploadfthshval', ['as' => 'editor.gm.uploadfthshval','uses' => 'GmController@uploadfthshval']);
    Route::get('/gm/fthshviewgraf', ['as' => 'editor.gm.fthshviewgraf', 'uses' => 'GmController@fthshviewgraf']);
    //H2S-H2
    Route::get('/gm/h2sh2Viewstore', ['as' => 'editor.gm.h2sh2Viewstore', 'uses' => 'GmController@h2sh2Viewstore']);
    Route::post('/gm/h2sh2store', ['as' => 'editor.gm.h2sh2store', 'uses' => 'GmController@h2sh2store']);
    Route::post('/gm/h2sh2store/uploadh2sh2Log', ['as' => 'editor.gm.uploadh2sh2Log','uses' => 'GmController@uploadh2sh2Log']);
    //CarHar
    Route::get('/gm/CarHarViewstore', ['as' => 'editor.gm.CarHarViewstore', 'uses' => 'GmController@CarHarViewstore']);
    Route::post('/gm/CarHarstore', ['as' => 'editor.gm.CarHarstore', 'uses' => 'GmController@CarHarstore']);
	Route::post('/gm/CarHarstore/UploadCarHarErrorLog', ['as' => 'editor.gm.UploadCarHarErrorLog','uses' => 'GmController@UploadCarHarErrorLog']);
	Route::post('/gm/CarHarstore/UploadCarHarLog', ['as' => 'editor.gm.UploadCarHarLog','uses' => 'GmController@UploadCarHarLog']);
	Route::post('/gm/CarHarstore/UploadCarHarVal', ['as' => 'editor.gm.UploadCarHarVal','uses' => 'GmController@UploadCarHarVal']);
	Route::get('/gm/CarHarViewGraf', ['as' => 'editor.gm.CarHarViewGraf', 'uses' => 'GmController@CarHarViewGraf']);
	*/
});
