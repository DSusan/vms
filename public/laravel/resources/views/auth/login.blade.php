<!DOCTYPE html>

<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 8
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

	<!-- begin::Head -->
	<head>
		<base href="../../../">
		<meta charset="utf-8" />
		<title>VMS</title>
		<meta name="description" content="Login page example">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

		<!--end::Fonts -->

		<!--begin::Page Custom Styles(used by this page) -->
		<link href="{{Config::get('constants.path.css')}}/pages/login/login-2.css" rel="stylesheet" type="text/css" />

		<!--end::Page Custom Styles -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="{{Config::get('constants.path.css')}}/style.bundle.css" rel="stylesheet" type="text/css" />

		<!--begin:: Vendor Plugins for custom pages -->
		<link href="{{Config::get('constants.path.plugins')}}/custom/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
		<link href="{{Config::get('constants.path.plugins')}}/custom/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
		<!--end:: Vendor Plugins for custom pages -->

		<!--end::Global Theme Styles -->

		<link rel="shortcut icon" href="{{Config::get('constants.path.media')}}/logos/favicon.ico" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize kt-page--loading">
		<!-- begin:: Page -->
		<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
			<div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url({{Config::get('constants.path.media')}}/bg/bg-3.jpg);">
					<div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
						<div class="kt-login__container">
							<div class="kt-login__logo">
								<a href="#">
									 <img src="{{Config::get('constants.path.media')}}/logos/logo-16.png">
								</a>
							</div>
							<div class="kt-login__signin">
								<div class="kt-login__head">
									<h3 class="kt-login__title" style="color: black;">Log In To VMS</h3>
									@if(Auth::check())
									<script type="text/javascript">
    window.location = "{ url('/editor/') }";//here double curly bracket
</script>
	@else
@endif
								</div>
								<form class="kt-form" action="{{ url('/login') }}" method="POST">
									{{ csrf_field() }}
									<div class="input-group">
										<input class="form-control" type="text" placeholder="Username" name="username" autocomplete="off">
									</div>
									<div class="input-group">
										<input class="form-control" type="password" placeholder="Password" name="password">
									</div>
									<div class="kt-login__actions">
										<button id="kt_login_signin_submit" class="btn btn-brand btn-elevate kt-login__btn-primary" style="background-color: #1dc9b7">Sign In</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- end:: Page -->

		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#22b9ff",
						"light": "#ffffff",
						"dark": "#282a3c",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>

		<!-- end::Global Config -->

		<!--begin::Global Theme Bundle(used by all pages) -->

		<!--begin:: Vendor Plugins -->
		<script src="{{Config::get('constants.path.plugins')}}/general/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="{{Config::get('constants.path.plugins')}}/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="{{Config::get('constants.path.plugins')}}/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="{{Config::get('constants.path.plugins')}}/general/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="{{Config::get('constants.path.plugins')}}/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
		<script src="{{Config::get('constants.path.plugins')}}/general/js/global/integration/plugins/bootstrap-switch.init.js" type="text/javascript"></script>
		<script src="{{Config::get('constants.path.plugins')}}/general/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
		<script src="{{Config::get('constants.path.plugins')}}/general/js/global/integration/plugins/bootstrap-notify.init.js" type="text/javascript"></script>
		<script src="{{Config::get('constants.path.plugins')}}/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
		<script src="{{Config::get('constants.path.plugins')}}/general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
		<script src="{{Config::get('constants.path.plugins')}}/general/js/global/integration/plugins/jquery-validation.init.js" type="text/javascript"></script>
		<script src="{{Config::get('constants.path.plugins')}}/general/jquery.repeater/src/lib.js" type="text/javascript"></script>
		<script src="{{Config::get('constants.path.plugins')}}/general/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
		<script src="{{Config::get('constants.path.plugins')}}/general/jquery.repeater/src/repeater.js" type="text/javascript"></script>

		<!--end:: Vendor Plugins -->
		<script src="{{Config::get('constants.path.js')}}/scripts.bundle.js" type="text/javascript"></script>

		<!--begin:: Vendor Plugins for custom pages -->
		<script src="{{Config::get('constants.path.plugins')}}/custom/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
		<!--end:: Vendor Plugins for custom pages -->

		<!--end::Global Theme Bundle -->

		<!--begin::Page Scripts(used by this page) 
		<script src="{{Config::get('constants.path.js')}}/pages/custom/login/login-general.js" type="text/javascript"></script>
-->
		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>