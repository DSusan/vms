@extends('layouts.template')
@section('content')
@actionStart('action', 'read')
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				Action List
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
					{{-- <div class="dropdown dropdown-inline">
						<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="la la-download"></i> Export
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<ul class="kt-nav">
								<li class="kt-nav__section kt-nav__section--first">
									<span class="kt-nav__section-text">Choose an option</span>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon la la-print"></i>
										<span class="kt-nav__link-text">Print</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon la la-copy"></i>
										<span class="kt-nav__link-text">Copy</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon la la-file-excel-o"></i>
										<span class="kt-nav__link-text">Excel</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon la la-file-text-o"></i>
										<span class="kt-nav__link-text">CSV</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon la la-file-pdf-o"></i>
										<span class="kt-nav__link-text">PDF</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
					&nbsp; --}}
					@actionStart('action', 'create')
					<a href="#add" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal">
						<i class="la la-plus"></i>
						Add New
					</a>
					@actionEnd
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
			<thead>
				<tr>
					<th>No</th>
					<th>Name</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($actions as $key => $action)
					<tr>
						<td>{{$number++}}</td>
						<td>{{$action->name}}</td>
						<td>{{$action->desc}}</td>
						<td>
							@actionStart('action', 'edit')
							<a href="#edit{{$action->id}}" class="btn btn-sm btn-primary btn-icon btn-icon-md" title="Edit" data-toggle="modal">
								<i class="la la-edit"></i>
							</a>
							@actionEnd
							
							@actionStart('action', 'delete')
							<a href="#delete{{$action->id}}" class="btn btn-sm btn-google btn-icon btn-icon-md" title="Delete" data-toggle="modal">
								<i class="la la-trash-o"></i>
							</a>
							@actionEnd
						</td>
					</tr>
					
					{{-- BEGIN MODAL EDIT --}}
					<div class="modal fade" id="edit{{$action->id}}" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
						<div class="modal-dialog">
							{!! Form::open(array('route' => ['editor.action.update', $action->id], 'method' => 'PUT'))!!}
							{{ csrf_field() }}
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Edit Action</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									</button>
								</div>
								<div class="modal-body">
									<div class="form-group">
										<label>Action</label>
										<input type="text" class="form-control" name="actionEdit" value="{{$action->name}}">
										<span class="help-block">*lowercase only</span>
									</div>
									<p>&nbsp;</p>
									<div class="form-group">
										<label>Description</label>
										<textarea class="form-control" rows="3" name="descEdit">{{$action->desc}}</textarea>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary">Save</button>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
					{{-- END MODAL EDIT --}}

					{{-- BEGIN MODAL DELETE --}}
					<div class="modal fade" id="delete{{$action->id}}" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
						<div class="modal-dialog">
							{!! Form::open(array('route' => ['editor.action.delete', $action->id], 'method' => 'delete'))!!}
							{{ csrf_field() }}
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Delete Data</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									</button>
								</div>
								<div class="modal-body">
									Are you sure want to delete this data?
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
									<button type="submit" class="btn btn-primary">Yes</button>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
					{{-- END MODAL DELETE --}}
				@endforeach
			</tbody>
		</table>

		<!--end: Datatable -->
	</div>

	{{-- BEGIN MODAL ADD --}}
	<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog" role="document">
			{!! Form::open(array('route' => ['editor.action.store'], 'method' => 'POST'))!!}
			{{ csrf_field() }}
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel"> Action</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Action</label>
						<input type="text" class="form-control" name="action">
						<span class="help-block">*lowercase only</span>
					</div>
					<p>&nbsp;</p>
					<div class="form-group">
						<label>Description</label>
						<textarea class="form-control" rows="3" name="desc"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary" name="saveAdd" value="1">Save</button>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
	{{-- END MODAL ADD --}}
</div>
@actionEnd
@endsection