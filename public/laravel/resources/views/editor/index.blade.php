@extends('layouts.template')
@section('content')

<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">
        User Management <button type="button" id="insertButton" class="btn btn-block bg-gradient-primary btn-sm">Primary</button>
      </h3>
    </div>
    <div class="kt-portlet__head-toolbar">
      <div class="kt-portlet__head-wrapper">
        <div class="kt-portlet__head-actions">
        </div>
      </div>
    </div>
  </div>
  <div class="kt-portlet__body">
    <div class="row">
           <div class="col-12">
              <div id="TUser"></div>
        </div>    </div>
  </div>
</div>
<!-- update password form -->
<form id="UpdatePasswordForm" name="UpdatePasswordForm" method="post" >
{{ csrf_field() }}
<div class="modal fade" id="UpdatePasswordModal" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Reset Password</h4>
            </div>
            <div class="modal-body">
              <div class="alert alert-danger print-error-msg" style="display:none">
                  <ul></ul>
              </div>
              <div class="form-group">
                <input type="hidden" id="updateId" name="id" class="form-control">
                <label>Password:</label>
                <input type="text" id="updatePassword" name="password" class="form-control" placeholder="password">
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" id="updatePasswordUser" class="btn btn-primary">Submit</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
</form>
<!-- update form -->
<form id="UpdateForm" name="UpdateForm" method="post" >
  {{ csrf_field() }}
<div class="modal fade" id="UpdateModal" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Update</h4>
            </div>
            <div class="modal-body">
              <div class="alert alert-danger print-error-msg" style="display:none">
                  <ul></ul>
              </div>
              <div class="form-group">
          <label>Name:</label>
                <input type="text" id="updateName" name="name" class="form-control" placeholder="name">
              </div>
               <div class="form-group">
                <label>Address:</label>
                <input type="text" id="updateAddress" name="address" class="form-control" placeholder="address">
              </div>
              <div class="form-group">
                <label>Password:</label>
                <input type="text" id="updateMobile" name="mobile" class="form-control" placeholder="mobile">
              </div>
              <div class="form-group">
                <strong>Company:</strong>
                <input type="text" id="updateCompany" name="company" class="form-control" placeholder="company">
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" id="updateUser" class="btn btn-primary">Submit</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
</form>
<form id="insertForm" name="insertForm" method="post" >
{{ csrf_field() }}
<div class="modal fade" id="insertModal" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Insert New Visitor</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>Name:</label>
                <input type="text" id="insertName" name="name" class="form-control" placeholder="name">
              </div>
               <div class="form-group">
                <label>Address:</label>
                <input type="text" id="insertAddress" name="address" class="form-control" placeholder="address">
              </div>
              <div class="form-group">
                <label>Password:</label>
                <input type="text" id="insertMobile" name="mobile" class="form-control" placeholder="mobile">
              </div>
              <div class="form-group">
                <strong>Company:</strong>
                <input type="text" id="insertCompany" name="company" class="form-control" placeholder="company">
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" id="submitNewUser" class="btn btn-primary">Submit</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
</form>
@endsection
@section('script')
<script type="text/javascript">
    function printErrorMsg (msg) {
      $.each( msg, function( key, value ) {
        toastr.error(value);
      });
    }
    $(document).ready(function (e) {
      
      $("#insertButton").click(function(e){
        $("#insertModal").modal("show");
      });
      $("#submitNewUser").click(function(e){ 
        var postData = $( "#insertForm" ).serialize();
        // postData.append("_token", "{{ csrf_token() }}");
        $.ajax({
          url: "{{ URL::route('editor.visitor.insertData') }}",
          data: postData,
          type: "POST",
          dataType: "json",
          cache: false,
          success: function(data) {
            if($.isEmptyObject(data.error)){
              toastr.success(data.success);
              $('#insertForm')[0].reset();
              $("#insertModal").modal("hide");
              $("#TUser").jsGrid("loadData");
            }else{
              printErrorMsg(data.error);
            }
          }
        });
      });
      $("#TUser").jsGrid({
        width: "100%",
        height: "500px",
        paging: true,   
        autoload: true,
        pageSize: 10,
        pageButtonCount: 3,
        pageLoading: true,
        updateOnResize: true,
        controller: {
            loadData: function(filter) {
            var d = $.Deferred();
            $.ajax({    
              url: "{{ route('editor.visitor.getData') }}",
              data: filter,
              dataType: "json"
            }).done(function(response){
              d.resolve(response);
            });
            return d.promise();
          }
        },
        fields: [
          { name: "id", type: "text", width: 30 },
          { name: "name", type: "text", width: 100 },
          { name: "address", type: "text", width: 70 },
          { name: "mobile", type: "text", width: 70 },
{ name: "company", type: "text", width: 70 },

          { name: "id", title:"token", type: "text", width: 70 ,
            itemTemplate: function(value, item) {
              var idData = item.id;
              var customEditButton = $("<button>").text("Edit").on("click", function() {
                  $("#UpdateForm .modal-title").empty().append("Update User "+idData);
                  $.ajax({
                    url: "{{ URL::route('editor.visitor.getDataDtl') }}",
                    data: {
                      id: idData,
                      "_token": "{{ csrf_token() }}",
                    },
                    type: "POST",
                    dataType: 'JSON',
                    cache: false,
                    success: function(data) {
                      $.each(data, function(i, e) {
                        $("#UpdateForm #updateId").empty().val(e.id);
                        $("#UpdateForm #updateUsername").empty().val(e.username);
                        $("#UpdateForm #updateName").empty().val(e.name);
                        $("#UpdateForm #updateEmail").empty().val(e.email);
                      });
                    }
                  });
                  $("#UpdateModal").modal("show");
              });
               var UpdatePasswordButton = $("<button>").text("password").on("click", function() {
                  $("#UpdatePasswordForm .modal-title").empty().append("Reset Password "+idData);
                  $("#UpdatePasswordForm #updatePassword").val("");
                  $("#UpdatePasswordForm #updateId").val(idData);
                  $("#UpdatePasswordModal").modal("show");
              });
             return $("<div>").append(customEditButton).append(" ").append(UpdatePasswordButton);
            }
          },
        ]
      });
      $("#updateUser").click(function(e){
        var postData = $( "#UpdateForm" ).serialize();
        $.ajax({
          url: "{{ URL::route('editor.visitor.updateData') }}",
          data: postData,
          type: "POST",
          dataType: "json",
          cache: false,
          success: function(data) {
            if($.isEmptyObject(data.error)){
              toastr.success(data.success);
              // refresh grid with new data
               $("#TUser").jsGrid("loadData");
            // alert(data.success);
            }else{
              printErrorMsg(data.error);
            }
          }
        });
      });
      $("#updatePasswordUser").click(function(e){
        var postData = $( "#UpdatePasswordForm" ).serialize();
        $.ajax({
          url: "{{ URL::route('editor.visitor.resetPassword') }}",
          data: postData,
          type: "POST",
          dataType: "json",
          cache: false,
          success: function(data) {
            if($.isEmptyObject(data.error)){
              toastr.success(data.success);
              // refresh grid with new data
               $("#TUser").jsGrid("loadData");
            // alert(data.success);
            }else{
              printErrorMsg(data.error);
            }
          }
        });
      });

      /* $("#datagrid").bootgrid({
        ajaxSettings: {
            method: "POST",
            cache: false
        },
        ajax: true,
        url: "{{ route('editor.visitor.getData') }}",
        templates: {search: "",},
        rowCount: [10, 20, 30, 50, -1],
        requestHandler : function (request) {
          request._token = "{{ csrf_token() }}";
          request.nomor = $('#nomor').val();
          request.tipe = $('#tipe').val();
          request.base = $('#base').val();
          return request;
        }
      });
      $("#cari").click(function(e){
        $('#datagrid').bootgrid('reload');
      });*/
    });
</script>
@endsection