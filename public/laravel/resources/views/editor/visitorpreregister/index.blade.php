@extends('layouts.template')
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-space-xl nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#kt_user_edit_tab_1" role="tab">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24" />
                                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                </g>
                            </svg> Add Visitor
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_user_edit_tab_2" role="tab">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24" />
                                    <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero" />
                                    <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3" />
                                </g>
                            </svg> Pre-Register List
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="kt-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                        <div class="kt-form kt-form--label-right">
                            <div class="kt-form__body">
                                <div class="kt-section kt-section--first">
                                    <div class="kt-section__body">
                                        <div class="row">
                                            <label class="col-xl-3"></label>
                                            <div class="col-lg-9 col-xl-6">
                                                <h3 class="kt-section__title kt-section__title-sm">Guest Info:</h3>
                                            </div>
                                        </div>
                                        <form id="formCekIn" name="formCekIn" method="post" >
                                        {{ csrf_field() }}
                                        <input type='hidden' readonly id='act' name='act' value='n'>
                                        <input type='hidden' readonly id='visitor_pre_id' name='visitor_pre_id'>
                                        <input type="hidden" readonly id='user_id' name="user_id" value="{{ Auth::user()->id }}">
                                        <h3>Visit Detail</h3>
                                        <br>
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-xl-3">Visitor Date</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-daterange input-group">
                                                    <input type="text" class="tgl form-control col-md-3" id="period_from" name="period_from" placeholder="Start" readonly/>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                                    </div>
                                                    <input type="text" class="tgl form-control col-md-3" id="period_to" name="period_to" placeholder="End" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <h3>Visitor Detail</h3>
                                        <br>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Visitor Name</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" type="text" name="name" id="name" placeholder="Enter Visitor Full Name">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Visitor ID</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" type="text" name="vid" id="vid"  placeholder="*Mandatory">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Visitor Address</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" type="text"  name="address" id="address"  placeholder="Enter Complete Address">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Mobile Number</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Phone" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Company</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" type="text" name="company" id="company" placeholder="Enter Company Name">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">PIC</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" type="text" name="pic" id="pic" placeholder="Enter PIC Name">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Purpose</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" type="text" name="purpose" id="purpose" placeholder="Describe the purpose of arrival">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Floor</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" type="text" name="floor" id="floor" placeholder="Enter floor number">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Remarks</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <textarea class="form-control" style="height: 100px">
                                                    
                                                </textarea>
                                            </div>
                                        </div>
                                        <div class="kt-form__actions">
                                            <div class="col-lg-9 ml-lg-auto" style="padding-left: 8px;">
                                                <button type="button" id="submit" name="submit" class="btn btn-brand">Submit</button>
                                            </div>	
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="kt_user_edit_tab_2" role="tabpanel">
                        <div class="col-md-12">
                            <div class="kt-margin-t-10 kt-margin-l-10">
                                <table id="TUser" class="table table-striped- table-bordered table-hover table-checkable">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Date</th>
                                            <th>Name</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

<!-- end:: Content -->
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function (e) {
    var dataTuser = $('#TUser').DataTable({
        'searching': true,
        "processing": true,
        "serverSide": true,
        "pagingType": "full_numbers",
        'fnDrawCallback': function (oSettings) {
        $('.dataTables_filter').each(function () {
            $(this).empty().append(
               /*  '<input type="text" id="fromDate" placeholder="From..." class="TANGGAL form-control form-control-sm">&nbsp :&nbsp<input type="text" id="toDate" placeholder="To..." class="TANGGAL form-control form-control-sm"> &nbsp'
                +'<input type="button"  class="btn btn-default xs" id="carix" value="search"> &nbsp<br /><br />'
                +'<select id="ssec" class="col-sm-2" name="ssec"><option value="bypdf">pdf</option><option value="byexcel">excel</option></select> &nbsp'
                +'<button type="button" id="insertButton" class="btn btn-primary xs">Export</button> &nbsp'
                */
            );
            $("#ssec").select2({width:"100px"});
            $('.TANGGAL').datepicker({
            format: 'yyyy-mm-dd'
            });
            $("#insertButton").click(function (e) {
            var favorite = [];
            $("input:checkbox[name=tipe]:checked").each(function(){
                favorite.push($(this).val());
            });
            var url = "{{ URL::route('editor.report.export_query') }}?id=" + favorite;

            window.location = url;
            });
            $("#carix").click(function(e){
                var searchByName = $('#searchByName').val();
                var ssec = $("#ssec").val();
                var from = $("#fromDate").val();
                var to = $("#toDate").val();
                dataTuser.ajax.reload();
            });
        });
        },
        "ajax": {
        "url" : "{{ route('editor.visitorpre.getData') }}",
        "data": function(data){
            data.searchByName = function() { return $('#searchByName').val() };
            data.ssec = function() { return $('#ssec').val() };
            data.from = function () { return $("#fromDate").val() };
            data.to = function () { return $("#toDate").val() };
        }
        },
        "columns": [
            { "data": "id", "width": 10, "orderable": false },
            { "data": "period_from" , "orderable": false},
            { "data": "name" , "orderable": false},
            { "data":  "status", "orderable": false, 
                render: function ( data, type, row ) {
                    var status = row.status;
                    var RowId = row.id;
                    if( status == 1){
                        return "<button class='cekin' id='"+RowId+"'>Edit "+RowId+"</button> <button class='app' id='"+RowId+"'>Need Approvel</button>";
                    } else if( status == 2 ){
                        return "<a href='#'>Approved</a>";
                    }
                }
            }
        ],
    });
    $('#TUser tbody').on( 'click', '.cekin', function (e) {
		var data = dataTuser.row( $(this).parents('tr') ).data();
		$('#formCekIn')[0].reset();
        $('.nav-tabs a[href="#kt_user_edit_tab_1"]').tab('show');
        $("#act").val("e");
        $.ajax({
            url: "{{ URL::route('editor.visitorpre.preregistergetDataDtl') }}",
            data: { visitor_pre_id: data.id},
            type: "get",
            dataType: "json",
            cache: false,
            success: function(data) {
                $("#visitor_pre_id").val(data.VisitorPre[0].id);
                $("#name").val(data.VisitorPre[0].name);
                $("#address").val(data.VisitorPre[0].address);
                $("#mobile").val(data.VisitorPre[0].mobile);
                $("#company").val(data.VisitorPre[0].company);
                $("#pic").val(data.VisitorPre[0].pic);
                $("#purpose").val(data.VisitorPre[0].purpose);
                $("#floor").val(data.VisitorPre[0].floor);
                $("#period_from").val(data.VisitorPre[0].period_from);
                $("#period_to").val(data.VisitorPre[0].period_to);
                $("#vid").val(data.VisitorPre[0].vid);
            }
        });
    });
    $('#TUser tbody').on( 'click', '.app', function (e) {
		var data = dataTuser.row( $(this).parents('tr') ).data();
        $.ajax({
            url: "{{ URL::route('editor.visitorpre.preregisterapp') }}",
            data: { visitor_pre_id: data.id, "_token": "{{ csrf_token() }}", user_id:"{{ Auth::user()->id }}"},
            type: "post",
            dataType: "json",
            cache: false,
            success: function(data) {
                if($.isEmptyObject(data.error)){
                        toastr.success(data.msg);
                        dataTuser.ajax.reload( null, false );
                }else{
                    printErrorMsg(data.error);
                }
            }
        });
    });
    $('.tgl').datepicker({
        format: 'yyyy-mm-dd'
    });
    $("#submit").click(function(e){
        var postData = $( "#formCekIn" ).serialize();
        var act = $("#act").val();
        if(act == "n"){
            $.ajax({
                url: "{{ URL::route('editor.visitorpre.preregisternew') }}",
                data: postData,
                type: "post",
                dataType: "json",
                cache: false,
                success: function(data) {
                    if($.isEmptyObject(data.error)){
                        toastr.success(data.msg);
                        dataTuser.ajax.reload( null, false );
                    }else{
                        printErrorMsg(data.error);
                    }
                }
            });
        } else if(act == "e"){
            $.ajax({
                url: "{{ URL::route('editor.visitorpre.preregisteredit') }}",
                data: postData,
                type: "post",
                dataType: "json",
                cache: false,
                success: function(data) {
                    if($.isEmptyObject(data.error)){
                        toastr.success(data.msg);
                        dataTuser.ajax.reload( null, false );
                    }else{
                        printErrorMsg(data.error);
                    }
                }
            });
        }
        $('#formCekIn')[0].reset();
        $("#act").val("n");
        $("#user_id").val("{{ Auth::user()->id }}");
    });
});
</script>
@endsection