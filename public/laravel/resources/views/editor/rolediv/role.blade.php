<h5 class="kt-portlet__head-title">Role Setting</h5><br>
<!--begin: Role -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="roleTable">
	<thead>
		<tr>
			<th></th>
			<th>No</th>
			<th>Name</th>
			<th>Description</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($roles as $key => $role)
			<tr>
				<td></td>
				<td>{{$numberRole++}}</td>
				<td>{{$role->name}}</td>
				<td>{{$role->desc}}</td>
				<td>
					@actionStart('role', 'edit')
					<a href="#editRole{{$role->id}}" class="btn btn-sm btn-primary btn-icon btn-icon-md" title="Edit" data-toggle="modal">
						<i class="la la-edit"></i>
					</a>
					@actionEnd
					
					@actionStart('role', 'delete')
					<a href="#deleteRole{{$role->id}}" class="btn btn-sm btn-google btn-icon btn-icon-md" title="Delete" data-toggle="modal">
						<i class="la la-trash-o"></i>
					</a>
					@actionEnd
				</td>
			</tr>
			
			{{-- BEGIN MODAL EDIT --}}
			<div class="modal fade" id="editRole{{$role->id}}" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog">
					{!! Form::open(array('route' => ['editor.role.update', $role->id], 'method' => 'PUT'))!!}
					{{ csrf_field() }}
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Edit Role</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label>Role</label>
								<input type="text" class="form-control" name="roleEdit" value="{{$role->name}}">
							</div>
							
							<div class="form-group">
								<label>Description</label>
								<textarea class="form-control" rows="3" name="descEdit">{{$role->desc}}</textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
			{{-- END MODAL EDIT --}}

			{{-- BEGIN MODAL DELETE --}}
			<div class="modal fade" id="deleteRole{{$role->id}}" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog">
					{!! Form::open(array('route' => ['editor.role.delete', $role->id], 'method' => 'delete'))!!}
					{{ csrf_field() }}
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Delete Data</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>
						<div class="modal-body">
							Are you sure want to delete this data?
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
							<button type="submit" class="btn btn-primary">Yes</button>
						</div>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
			{{-- END MODAL DELETE --}}
		@endforeach
	</tbody>
</table>

<!--end: Role -->