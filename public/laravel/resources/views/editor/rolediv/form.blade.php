@extends('layouts.template')
@section('content')
@actionStart('position', 'read')
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">Position Privilege</h3>
		</div>
	</div>
	<div class="kt-portlet__body">
		<div class="row">
			<div class="col-xl-3"></div>
			<div class="col-xl-6">
				<div class="kt-section">
					<div class="kt-section__body">
						<div class="form-group row">
							<label class="col-3 col-form-label">Position</label>
							<div class="col-9">
								<input class="form-control" type="text" value="{{$roleDiv->roleName}}&nbsp;{{$roleDiv->divName}}" disabled="disabled">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-3 col-form-label">Privilege</label>
							<div class="col-9">
								{!! Form::model($roleDiv, array('route' => ['editor.rprivilege.update', $roleDiv->id], 'method' => 'PUT'))!!}
								{{ csrf_field() }}
								<table class="table">
									<thead>
										<tr>
											<th></th>
											@foreach($actionList as $actionKey => $action)
												<th>{{$action}}</th>
											@endforeach
										</tr>
									</thead>
									<tbody>
									@foreach($moduleList as $moduleKey => $module)
										<tr>
											<td>{{$module}}</td>
											@foreach($actionList as $actionKey => $action)
												<td>
													<label class="kt-checkbox kt-checkbox--success">
														<input type="checkbox" name="privilege[{{$moduleKey}}][{{$actionKey}}]" id="privilege_{{$moduleKey}}_{{$actionKey}}" value="1">
														<span></span>
													</label>
												</td>
											@endforeach
										</tr>
									@endforeach
									</tbody>
								</table>

								<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@actionEnd
@endsection

@section('script')
@if(isset($roleDiv))
<script>
jQuery.each({!! $rolePriv->privilege !!}, function(key, value)
{
	$('#privilege_'+value['id_modules']+'_'+value['id_actions']).attr('checked', true);
});
</script>
@endif
@endsection