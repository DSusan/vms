<h5 class="kt-portlet__head-title">Division Setting</h5><br>
<!--begin: Division -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="divisionTable">
	<thead>
		<tr>
			<th></th>
			<th>No</th>
			<th>Name</th>
			<th>Description</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($divisions as $key => $division)
			<tr>
				<td></td>
				<td>{{$numberDiv++}}</td>
				<td>{{$division->name}}</td>
				<td>{{$division->desc}}</td>
				<td>
					@actionStart('division', 'edit')
					<a href="#editDivision{{$division->id}}" class="btn btn-sm btn-primary btn-icon btn-icon-md" title="Edit" data-toggle="modal">
						<i class="la la-edit"></i>
					</a>
					@actionEnd
					
					@actionStart('division', 'delete')
					<a href="#deleteDivision{{$division->id}}" class="btn btn-sm btn-google btn-icon btn-icon-md" title="Delete" data-toggle="modal">
						<i class="la la-trash-o"></i>
					</a>
					@actionEnd
				</td>
			</tr>
			
			{{-- BEGIN MODAL EDIT --}}
			<div class="modal fade" id="editDivision{{$division->id}}" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog">
					{!! Form::open(array('route' => ['editor.division.update', $division->id], 'method' => 'PUT'))!!}
					{{ csrf_field() }}
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Edit Division</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label>Division</label>
								<input type="text" class="form-control" name="divisionEdit" value="{{$division->name}}">
							</div>
							
							<div class="form-group">
								<label>Description</label>
								<textarea class="form-control" rows="3" name="descEdit">{{$division->desc}}</textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
			{{-- END MODAL EDIT --}}

			{{-- BEGIN MODAL DELETE --}}
			<div class="modal fade" id="deleteDivision{{$division->id}}" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog">
					{!! Form::open(array('route' => ['editor.division.delete', $division->id], 'method' => 'delete'))!!}
					{{ csrf_field() }}
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Delete Data</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>
						<div class="modal-body">
							Are you sure want to delete this data?
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
							<button type="submit" class="btn btn-primary">Yes</button>
						</div>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
			{{-- END MODAL DELETE --}}
		@endforeach
	</tbody>
</table>

<!--end: Division -->