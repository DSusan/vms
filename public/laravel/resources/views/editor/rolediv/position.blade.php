<h5 class="kt-portlet__head-title">Position Setting</h5><br>
<!--begin: Position Setting -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="positionTable">
	<thead>
		<tr>
			<th></th>
			<th>No</th>
			<th>Parent</th>
			<th>Position</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($roleDivsList as $key => $roleDivList)
			<tr>
				<td></td>
				<td>{{$number++}}</td>
				<td>{{$parentPosition[$roleDivList->id]['name']}}</td>
				<td>{{$roleDivList->roleName}} {{$roleDivList->divName}}</td>
				<td>
					@actionStart('rprivilege', 'access')
					<a href="{{ URL::route('editor.rprivilege.edit',$roleDivList->id) }}" class="btn btn-sm btn-warning btn-icon btn-icon-md" title="Privilege"><i class="la la-key"></i></a>
					@actionEnd
					
					@actionStart('position', 'edit')
					<a href="#edit{{$roleDivList->id}}" data-toggle="modal" class="btn btn-sm btn-primary btn-icon btn-icon-md" title="Edit"><i class="la la-edit"></i></a>
					@actionEnd
					
					@actionStart('position', 'delete')
					<a href="#delete{{$roleDivList->id}}" class="btn btn-sm btn-google btn-icon btn-icon-md" title="Delete" data-toggle="modal">
						<i class="la la-trash-o"></i>
					</a>
					@actionEnd
				</td>
			</tr>
			
			{{-- BEGIN MODAL EDIT --}}
			<div class="modal fade" id="edit{{$roleDivList->id}}" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog">
					{!! Form::open(array('route' => ['editor.rolediv.update', $roleDivList->id], 'method' => 'PUT'))!!}
					{{ csrf_field() }}
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Edit Position</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label>Parent Position</label>
								<select name="id_roles_divisions_parent" class="form-control">
									<option value=""></option>
									@foreach($parentLists as $keyParentLists => $parentList)
										@if($parentList->id == $roleDivList->id_roles_divisions_parent)
											<option value="{{$parentList->id}}" selected="selected">{{$parentList->name}}</option>
										@else
											<option value="{{$parentList->id}}">{{$parentList->name}}</option>
										@endif
									@endforeach
								</select>
							</div>

							<div class="form-group">
								<label>Role</label>
								{{Form::select('id_roles', $roleList, $roleDivList->roleId, array('class' => 'form-control'))}}
							</div>
							
							<div class="form-group">
								<label>Division</label>
								{{ Form::select('id_divisions', $divList, $roleDivList->divId, array('class' => 'form-control')) }}
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
			{{-- END MODAL EDIT --}}

			{{-- BEGIN MODAL DELETE --}}
			<div class="modal fade" id="delete{{$roleDivList->id}}" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog">
					{!! Form::open(array('route' => ['editor.rolediv.delete', $roleDivList->id], 'method' => 'delete'))!!}
					{{ csrf_field() }}
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Delete Data</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>
						<div class="modal-body">
							Are you sure want to delete this data?
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
							<button type="submit" class="btn btn-primary">Yes</button>
						</div>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
			{{-- END MODAL DELETE --}}
		@endforeach
	</tbody>
</table>

<!--end: Position Setting -->