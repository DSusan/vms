@extends('layouts.template')
@section('content')
@actionStart('position', 'read')
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				Position Management
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				{{-- <div class="kt-portlet__head-actions">
					@actionStart('position', 'create')
					<a href="#add" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal">
						<i class="la la-plus"></i>
						Add New
					</a>
					@actionEnd
				</div> --}}
				<div class="kt-portlet__head-actions">
					<div class="dropdown dropdown-inline">
						<button type="button" class="btn btn-brand btn-elevate btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Add New
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<ul class="kt-nav">
								<li class="kt-nav__item">
									<a href="#addPosition" class="kt-nav__link" data-toggle="modal">
										<span class="kt-nav__link-text">Position Setting</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#addRole" class="kt-nav__link" data-toggle="modal">
										<span class="kt-nav__link-text">Role Setting</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#addDivision" class="kt-nav__link" data-toggle="modal">
										<span class="kt-nav__link-text">Division Setting</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">

		<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-2x nav-tabs-line-success" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" data-toggle="tab" href="#position" role="tab">Position</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#role" role="tab">Role</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#division" role="tab">Division</a>
			</li>
		</ul>

		<div class="tab-content">

			<!--begin: Position -->
			<div class="tab-pane active" id="position" role="tabpanel">

				@include('editor.rolediv.position')

			</div>
			<!--end: Position -->

			<!--begin: Role -->
			<div class="tab-pane" id="role" role="tabpanel">

				@include('editor.rolediv.role')
				
			</div>
			<!--end: Role -->

			<!--begin: Division -->
			<div class="tab-pane" id="division" role="tabpanel">

				@include('editor.rolediv.division')
				
			</div>
			<!--end: Division -->

		</div>
	</div>
</div>

{{-- BEGIN MODAL ADD POSITION --}}
<div class="modal fade" id="addPosition" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		{!! Form::open(array('route' => ['editor.rolediv.store'], 'method' => 'POST'))!!}
		{{ csrf_field() }}
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New Position</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Parent Position</label>
					<select name="id_roles_divisions_parent" class="form-control">
						@foreach($parentLists as $keyParentLists => $parentList)
							<option value="{{$parentList->id}}">{{$parentList->name}}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<label>Role</label>
					{{Form::select('id_roles', $roleList, null, array('class' => 'form-control'))}}
				</div>
				
				<div class="form-group">
					<label>Division</label>
					{{ Form::select('id_divisions', $divList, null, array('class' => 'form-control')) }}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="saveAdd">Save</button>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
{{-- END MODAL ADD POSITION --}}

{{-- BEGIN MODAL ADD ROLE --}}
<div class="modal fade" id="addRole" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		{!! Form::open(array('route' => ['editor.role.store'], 'method' => 'POST'))!!}
		{{ csrf_field() }}
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New Role</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Role</label>
					<input type="text" class="form-control" name="role">
				</div>
				
				<div class="form-group">
					<label>Description</label>
					<textarea class="form-control" rows="3" name="desc"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="saveAdd" value="1">Save</button>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
{{-- END MODAL ADD ROLE --}}

{{-- BEGIN MODAL ADD DIVISION --}}
<div class="modal fade" id="addDivision" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		{!! Form::open(array('route' => ['editor.division.store'], 'method' => 'POST'))!!}
		{{ csrf_field() }}
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New Division</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Division</label>
					<input type="text" class="form-control" name="division">
				</div>
				
				<div class="form-group">
					<label>Description</label>
					<textarea class="form-control" rows="3" name="desc"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="saveAdd" value="1">Save</button>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
{{-- END MODAL ADD DIVISION --}}

@actionEnd
@endsection