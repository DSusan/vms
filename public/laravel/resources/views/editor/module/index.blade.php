@extends('layouts.template')
@section('content')
@actionStart('module', 'read')
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				Module List
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
					@actionStart('module', 'create')
					<a href="#add" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal">
						<i class="la la-plus"></i>
						Add New
					</a>
					@actionEnd
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
			<thead>
				<tr>
					<th>No</th>
					<th>Name</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($modules as $key => $module)
					<tr>
						<td>{{$number++}}</td>
						<td>{{$module->name}}</td>
						<td>{{$module->desc}}</td>
						<td>
							@actionStart('module', 'edit')
							<a href="#edit{{$module->id}}" class="btn btn-sm btn-primary btn-icon btn-icon-md" title="Edit" data-toggle="modal">
								<i class="la la-edit"></i>
							</a>
							@actionEnd
							
							@actionStart('module', 'delete')
							<a href="#delete{{$module->id}}" class="btn btn-sm btn-google btn-icon btn-icon-md" title="Delete" data-toggle="modal">
								<i class="la la-trash-o"></i>
							</a>
							@actionEnd
						</td>
					</tr>
					
					{{-- BEGIN MODAL EDIT --}}
					<div class="modal fade" id="edit{{$module->id}}" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
						<div class="modal-dialog">
							{!! Form::open(array('route' => ['editor.module.update', $module->id], 'method' => 'PUT'))!!}
							{{ csrf_field() }}
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Edit Module</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									</button>
								</div>
								<div class="modal-body">
									<div class="form-group">
										<label>Module</label>
										<input type="text" class="form-control" name="moduleEdit" value="{{$module->name}}">
										<span class="help-block">*lowercase only</span>
									</div>
									
									<div class="form-group">
										<label>Description</label>
										<textarea class="form-control" rows="3" name="descEdit">{{$module->desc}}</textarea>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary">Save</button>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
					{{-- END MODAL EDIT --}}

					{{-- BEGIN MODAL DELETE --}}
					<div class="modal fade" id="delete{{$module->id}}" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
						<div class="modal-dialog">
							{!! Form::open(array('route' => ['editor.module.delete', $module->id], 'method' => 'delete'))!!}
							{{ csrf_field() }}
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Delete Data</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									</button>
								</div>
								<div class="modal-body">
									Are you sure want to delete this data?
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
									<button type="submit" class="btn btn-primary">Yes</button>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
					{{-- END MODAL DELETE --}}
				@endforeach
			</tbody>
		</table>

		<!--end: Datatable -->
	</div>

	{{-- BEGIN MODAL ADD --}}
	<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog" role="document">
			{!! Form::open(array('route' => ['editor.module.store'], 'method' => 'POST'))!!}
			{{ csrf_field() }}
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add New Module</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Module</label>
						<input type="text" class="form-control" name="module">
						<span class="help-block">*lowercase only</span>
					</div>
					
					<div class="form-group">
						<label>Description</label>
						<textarea class="form-control" rows="3" name="desc"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary" name="saveAdd" value="1">Save</button>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
	{{-- END MODAL ADD --}}
</div>
@actionEnd
@endsection