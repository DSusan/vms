@extends('layouts.template')
@section('content')
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">
        Visitor
      </h3>
    </div>
    <div class="kt-portlet__head-toolbar">
      <div class="kt-portlet__head-wrapper">
        <div class="kt-portlet__head-actions">
        </div>
      </div>
    </div>
  </div>
  <div class="kt-portlet__body">
    <div class="row">
      <div class="col-12">
              <!-- <div id="TUser"></div>-->
        <table id="TUser" class="table table-striped- table-bordered table-hover table-checkable">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>visitor_name</th>
                    <th>Address</th>
                    <th>Company Name</th>
                    <th>Mobile</th>
                    <th>Card</th>
                    <th>Check In</th>
                    <th>Check Out</th>
                    <th>Status</th>
                  </tr>
                </thead>
              </table>
      </div>  
    </div>
  </div>
</div>
<form id="formCekIn" name="formCekIn" method="post" >
	{{ csrf_field() }}
  <div class="modal fade" id="modalCekIn" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><b><u>Visitor Detail Cek In</u></b></h4>
         
        </div>
        <div class="modal-body">
        	<input type="hidden" id="civisitor_id" name="visitor_id">
          <input type="hidden" id="civisitor_activity_id" name="visitor_activity_id">
          <div class="row">
              <div class="col-lg-6">
              <div class="form-group">
                <input type="hidden"  readonly name="user_id" value="{{ Auth::user()->id }}">
                <label for="visitorId">Id</label> 
                <input type="number" step="any" id="civisitor_id" class="form-control form-control-sm" name="visitor_id" placeholder="Ex. 1110501025" disabled>
                <span class="help-block">*auto-generated.</span>
              </div>
              <div class="form-group">
                <label for="visitorName">Visitor Id</label> 
                <input type="text" id="civid" class="form-control form-control-sm" name="vid">
              </div>
              <div class="form-group">
                <label for="visitorName">Visitor Name</label> 
                <input type="text" id="ciname" class="form-control form-control-sm" name="name">
              </div>
              <div class="form-group">

                    <label for="visitorAddress">Visitor Address</label> 
                    <input type="text" id="ciaddress" class="form-control form-control-sm" name="address" placeholder="Enter visitor's home address.">
           
              </div>
              <div class="form-group">
                    <label for="companyName">Company Name</label> 
                    <input type="text" id="cicompany" class="form-control form-control-sm" name="company" placeholder="Enter visitor's company-affiliated name.">
           
              </div>
              <div class="form-group">

                    <label for="companyName">Mobile</label> 
                    <input type="text" id="cimobile" class="form-control form-control-sm" name="mobile" placeholder="Enter mobile phone number, with format : +62 811 888 8888.">
          
              </div>
              
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-6">
                      <img src="{{Config::get('constants.path.media')}}/users/default.jpg" width="100%" height="100%" alt="..." class="img-thumbnail">
                    </div>
                    <div class="col-lg-6">
                      <img src="{{Config::get('constants.path.media')}}/users/default.jpg" width="100%" height="100%" alt="..." class="img-thumbnail">
                    </div>
                  </div>
                </div>
                <div class="form-group">

                    <label for="CardId">Card Id</label> 
                    <input type="text"  class="form-control form-control-sm" name="card_id" id="cicard_id" placeholder="*card Number">
           
                </div>
                <div class="form-group">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  <button type="button" id="submitCekIn" class="btn btn-primary">Check In</button>
                </div>
              </div>  
            </div>
            <div class="row">
              <div class="col-6">
                <div class="form-group">
                  <div class="row">
                    <div class="col-6">
                      <label>Period From:</label>
                      <input type='text' class="form-control form-control-sm" name="period_from" id="cperiod_from" readonly>
                    </div>
                    <div class="col-6">
                      <label>Period To:</label>
                      <input type='text' class="form-control form-control-sm" name="period_to" id="cperiod_to" readonly>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-6">
                      <label>Hours From:</label>
                      <input type='text' class="JAM form-control form-control-sm" name="hour_from" id="chour_from" readonly>
                    </div>
                    <div class="col-6">
                      <label>Hours From:</label>
                      <input type='text' class="JAM form-control form-control-sm" name="hour_to" id="chour_to" readonly>
                    </div>
                  </div>
                </div>
                 <div class="form-group">
                  <label>Host User:</label>
                  <input type='text' class="form-control form-control-sm" name="pic" id="cipic">
                </div>
                <div class="form-group">
                  <label>Purpose:</label>
                  <input type='text' class="form-control form-control-sm" name="purpose" id="cipurpose">
                </div>
                <div class="form-group">
                        <label for="Floor">Floor</label>
                          <select class="pfloor form-control form-control-sm" name="floor" id="cifloor">
                          </select>
                </div> 
              </div>
             
            </div>
        </div>
      </div>
    </div>
  </div>
</form>
<form id="formCekOut" name="formCekOut" method="post" >
  {{ csrf_field() }}
  <div class="modal fade" id="modalCekOut" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><b><u>Visitor Detail Cek Out</u></b></h4>
        </div>
        <div class="modal-body">
        	<input type="hidden" id="visitor_id" name="visitor_id">
          <input type="hidden" id="visitor_activity_id" name="visitor_activity_id">
          <div class="row">
              <div class="col-lg-6">
              <div class="form-group">
                <input type="hidden"  readonly name="user_id" value="{{ Auth::user()->id }}">
                <label for="visitorId">Visitor Id</label> 
                <input type="number" step="any" id="covisitorId" class="form-control form-control-sm" name="visitor_id" placeholder="Ex. 1110501025" disabled>
                <span class="help-block">*auto-generated.</span>
              </div>
              <div class="form-group">
                <label for="visitorName">Visitor Name</label> 
                <input type="text" id="coname" class="form-control form-control-sm" name="name">
              </div>
              <div class="form-group">

                    <label for="visitorAddress">Visitor Address</label> 
                    <input type="text" id="coaddress" class="form-control form-control-sm" name="address" placeholder="Enter visitor's home address.">
           
              </div>
              <div class="form-group">
                    <label for="companyName">Company Name</label> 
                    <input type="text" id="cocompany" class="form-control form-control-sm" name="company" placeholder="Enter visitor's company-affiliated name.">
           
              </div>
              <div class="form-group">

                    <label for="companyName">Mobile</label> 
                    <input type="text" id="comobile" class="form-control form-control-sm" name="mobile" placeholder="Enter mobile phone number, with format : +62 811 888 8888.">
          
              </div>
              
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-6">
                      <img src="{{Config::get('constants.path.media')}}/users/default.jpg" width="100%" height="100%" alt="..." class="img-thumbnail">
                    </div>
                    <div class="col-lg-6">
                      <img src="{{Config::get('constants.path.media')}}/users/default.jpg" width="100%" height="100%" alt="..." class="img-thumbnail">
                    </div>
                  </div>
                </div>
                <div class="form-group">

                    <label for="CardId">Card Id</label> 
                    <input type="text" id="cocard_id" class="form-control form-control-sm" name="card_id" placeholder="*card Number">
           
                </div>
                <div class="form-group">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  <button type="button" id="submitCekOut" class="btn btn-primary">Check Out</button>
                </div>
              </div>  
            </div>
            <div class="row">
              <div class="col-lg-6">
                 <div class="form-group">
                  <label>Host User:</label>
                  <input type='text' class="form-control form-control-sm" name="copic" id="copic">
                </div>
                <div class="form-group">
                  <label>Purpose:</label>
                  <input type='text' class="form-control form-control-sm" name="copurpose" id="copurpose">
                </div>
                <div class="form-group">
                  <label for="Floor">Floor</label>
                  <select class="pfloor form-control form-control-sm" name="cofloor" id="cofloor">
                  </select>
                </div> 
              </div>
             
            </div>
        </div>
      </div>
    </div>
  </div>
</form>
<form id="insertForm" name="insertForm" method="post">
{{ csrf_field() }}
<div class="modal fade" id="insertModal" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">
            <b>REGISTER</b></h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-lg-6">
                    <h2>Visitor Detail</h2>
                    <br>
                      <div class="form-group row">
                        <label for="covisitorId" class="col-sm-4 col-form-label">Id</label>
                        <div class="col-sm-8">
                          <input type="number" step="any" id="covisitorId" class="form-control form-control-sm" name="visitor_id" placeholder="Auto-generated" disabled>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="covisitorId" class="col-sm-4 col-form-label">Visitor Id</label>
                        <div class="col-sm-8">
                          <input type="text" id="covid" name="vid" class="form-control form-control-sm">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="coname" class="col-sm-4 col-form-label">Visitor Name</label>
                        <div class="col-sm-8">
                          <input type="text" id="coname" class="form-control form-control-sm" name="name">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="coaddress" class="col-sm-4 col-form-label">visitor Address</label>
                        <div class="col-sm-8">
                          <input type="text" id="coaddress" class="form-control form-control-sm" name="address" placeholder="Enter visitor's home address.">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="cocompany" class="col-sm-4 col-form-label">company Name</label>
                        <div class="col-sm-8">
                         <input type="text" id="cocompany" class="form-control form-control-sm" name="company" placeholder="Enter visitor's company-affiliated name.">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="comobile" class="col-sm-4 col-form-label">Mobile</label>
                        <div class="col-sm-8">
                         <input type="text" id="comobile" class="form-control form-control-sm" name="mobile" placeholder="Enter mobile phone number, with format : +62 811 888 8888.">
                        </div>
                      </div>
                      <br>
                      <h2>Destination</h2>
                      <br>
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Period</label>
                        <div class="col-sm-4" style="margin-bottom:10px;">
                          <input type='text' placeholder="from..." class="TANGGAL form-control form-control-sm" name="period_from" id="coperiod_from">
                        </div>
                        <div class="col-sm-4">
                          <input type='text' placeholder="to....." class="TANGGAL form-control form-control-sm" name="period_to" id="coperiod_to">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Hours</label>
                        <div class="col-sm-4" style="margin-bottom:10px;">
                          <input type='text' placeholder="from..." class="JAM form-control form-control-sm" name="hour_from" id="cohour_from">
                        </div>
                        <div class="col-sm-4">
                          <input type='text' placeholder="to....." class="JAM form-control form-control-sm" name="hour_to" id="cohour_to">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="HostUser" class="col-sm-4 col-form-label">Host User</label>
                        <div class="col-sm-8">
                          <input type='text' class="form-control form-control-sm" name="pic" id="copic">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="Purpose" class="col-sm-4 col-form-label">Purpose</label>
                        <div class="col-sm-8">
                          <input type='text' class="form-control form-control-sm" name="purpose" id="copurpose">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="Floor" class="col-sm-4 col-form-label">Floor</label>
                        <div class="col-sm-8">
                          <select class="pfloor" name="floor">
                          </select>
                          <!-- <input type='text' class="form-control form-control-sm" name="floor" id="cofloor">-->
                        </div>
                      </div>              
                </div>

                <div class="col-lg-6">
                  <div class="form-group">
                  <div class="row">
                    <div class="col-lg-6">
                      <!-- <input type=button value="Take Snapshot" onClick="take_snapshot()">
                      <div id="camera">Capture</div>
                      <div id="hasil"></div>
              -->
              <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_user_avatar_3">
                          <div class="kt-avatar__holder" style="overflow: hidden;">
                            <img src="{{Config::get('constants.path.media')}}/users/default.jpg" width="100%" height="100%" alt="...">
                          </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                       <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_user_avatar_3">
                          <div class="kt-avatar__holder" style="overflow: hidden;">
                            <img src="{{Config::get('constants.path.media')}}/users/default.jpg" width="100%" height="100%" alt="...">
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                    <label for="CardId" class="col-sm-2 col-form-label">Card Id</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="card_id" name="card_id">
                    </div>
                </div>
                <div class="form-group">
                  <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
                  <button type="button" id="submitNewUser" class="btn btn-success btn-sm cekin">Check In</button>
                </div>                 
                </div>
            </div>
          </div>
            
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
</form>
@endsection
@section('script')
<script src="{{Config::get('constants.path.plugins')}}/webcamjs/webcam.min.js" type="text/javascript"></script>
<script type="text/javascript">
function take_snapshot()  {
  // take snapshot and get image data
    Webcam.snap( function(data_uri) {
      $("#hasil").empty().append('<div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_user_avatar_3">'
        +'<div class="kt-avatar__holder" style="overflow: hidden;">'
            +'<img id="kkk" name="kkk" src="'+data_uri+'" width="100%" height="100%" alt="...">'
        +'</div>'
        +'</div>'
      );
      var url = "{{ URL::route('editor.visitor.uploads') }}";
      // upload foto
      var myImg = document.getElementById("kkk").src;
      Webcam.upload( myImg, url, function(code, text) {} );
     /*  var base64image = document.getElementById("kkk").src;
      Webcam.upload( base64image, "{{ URL::route('editor.visitor.uploads') }}", function(code, text) {
        console.log(text);
      });*/
      // display results in page
     
      /* Webcam.upload( data_uri, 'saveimage.php', function(code, text) {
        document.getElementById('results').innerHTML = 
          '<h2>Here is your image:</h2>' + 
          '<img src="'+text+'"/>';
        } );    
      });*/
    });
  }
$(document).ready(function (e) {
  $(".pfloor").select2({
		ajax: {
			url: "{{ URL::route('editor.visitor.getFloor') }}",
			type: "get",
			placeholder: 'Choose',
      allowClear: true,
			dataType: 'json',
			data: function (params) {
				return {
					searchTerm: params.term,
					"_token": "{{ csrf_token() }}",
		   		};
		   	},
		   	processResults: function (response) {
				return {
		        	results: response
		     	};
		   	},
		   	cache: false
		},
		width:"100%"
	});
   /*  Webcam.set({
      width: 320,
      height: 240,
    image_format: 'png',
    jpeg_quality: 100
  });
  Webcam.attach( '#camera' );
*/

  $('.TANGGAL').datepicker({
    format: 'yyyy-mm-dd',
    //startDate: '-3d'
  });
  $('.JAM').datetimepicker({
    use24hours: true,
    format: 'HH:mm:ss'
  });
  $("#sads").click(function(e){
        $.ajax({
          url: "{{ URL::route('editor.visitor.tes') }}",
          //data: postData,
          type: "get",
          dataType: "json",
          cache: false,
          success: function(data) {
            if($.isEmptyObject(data.error)){
              toastr.success(data.success);
              $('#asda').append(data);
            }else{
              printErrorMsg(data.error);
            }
          }
        });
      });
  $("#submitNewUser").click(function(e){
    var postData = $( "#insertForm" ).serialize();
    $.ajax({
      url: "{{ URL::route('editor.visitor.insertData') }}",
      data: postData,
      type: "post",
      dataType: "json",
      cache: false,
      success: function(data) {
        if($.isEmptyObject(data.error)){
          toastr.success(data.msg);
            $('#insertForm')[0].reset();
            $("#insertModal").modal("hide");
             dataTuser.ajax.reload( null, false );
        }else{
            printErrorMsg(data.error);
        }
      }
    });
  });
  $("#submitCekOut").click(function(e){
    var postData = $( "#formCekOut" ).serialize();
    $.ajax({
      url: "{{ URL::route('editor.visitor.cekoutData') }}",
      data: postData,
      type: "post",
      dataType: "json",
      cache: false,
      success: function(data) {
        if($.isEmptyObject(data.error)){
          toastr.success(data.msg);
            $('#formCekOut')[0].reset();
            $("#modalCekOut").modal("hide");
             dataTuser.ajax.reload( null, false );
        }else{
            printErrorMsg(data.error);
        }
      }
    });
  });
  $("#submitCekIn").click(function(e){
    var postData = $( "#formCekIn" ).serialize();
    $.ajax({
      url: "{{ URL::route('editor.visitor.cekinData') }}",
      data: postData,
      type: "post",
      dataType: "json",
      cache: false,
      success: function(data) {
        if($.isEmptyObject(data.error)){
          toastr.success(data.msg);
            $('#formCekIn')[0].reset();
            $("#modalCekIn").modal("hide");
             dataTuser.ajax.reload( null, false );
        }else{
            printErrorMsg(data.error);
        }
      }
    });
  });
  var dataTuser = $('#TUser').DataTable({
    'searching': true,
    "processing": true,
    "serverSide": true,
    "pagingType": "full_numbers",
    'fnDrawCallback': function (oSettings) {
      $('.dataTables_filter').each(function () {
        $(this).empty().append('<input type="text" id="searchByName" placeholder="Serch..." class="form-control form-control-sm"> '
          +'<select id="ssec" class="col-sm-2" name="ssec"><option value="0">---</option><option value="byCard">Card</option><option value="byName">Visitor</option></select>'
          +'<input type="button"  class="btn btn-default xs" id="carix" value="search"> '
          +'<button type="button" id="insertButton" class="btn btn-primary xs">Add New</button> '
        );
        $("#ssec").select2({width:"100px"});
        $("#insertButton").click(function(e){
          $("#insertModal").modal("show");
        });
        $("#carix").click(function(e){
          var searchByName = $('#searchByName').val();
          var ssec = $("#ssec").val();
          dataTuser.ajax.reload();
         // $("#searchByName").val(searchByName);
        });
        $('#searchByName').keypress(function (e) {
        var key = e.which;
        var searchByName = $('#searchByName').val();
        var ssec = $("#ssec").val();
        if(key == 13)  // the enter key code
          {
            var searchByName = $('#searchByName').val();
            var ssec = $("#ssec").val();
            dataTuser.ajax.reload();
          }
        });  
      });
    },
    "ajax": {
      "url" : "{{ route('editor.visitor.getData') }}",
      "data": function(data){
        data.searchByName = function() { return $('#searchByName').val() };
        data.ssec = function() { return $('#ssec').val() };
      }
    },
    /* <th>Id</th>
                    <th>visitor_name</th>
                    <th>Address</th>
                    <th>Company Name</th>
                    <th>Mobile</th>
                    <th>Card</th>
                    <th>Check In</th>
                    <th>Check Out</th>
                    <th>Status</th>*/
    "columns": [
      { "data": "id", "width": 10, "orderable": true },
      { "data": "name" , "orderable": false, render: function ( data, type, row ) {
          var cekinx = row.cek_in;
          var cekoutx = row.cek_out;
          if( cekinx && cekoutx || cekinx === null && cekoutx === null){
            return "<a href='#' id='"+row.id+"' class='cekin'>"+row.name+"</a>";
          } else if(cekinx && cekoutx === null){
            return "<a href='#' id='"+row.id+"' class='cekout'>"+row.name+"</a>";
          }
        }
      },
      { "data": "address", "orderable": false},
      { "data": "company", "orderable": false },
      { "data": "mobile", "orderable": false },
      { "data": "CardId", "orderable": false },
      { "data": "cek_in", "orderable": false },
      { "data": "cek_out", "orderable": false },
      { "data":  "CekIn", "title" : "status", "orderable": false, render: function ( data, type, row ) {
          var cekinx = row.cek_in;
          var cekoutx = row.cek_out;
          if( cekinx && cekoutx || cekinx === null && cekoutx === null){
            return "Check Out";
          } else if(cekinx && cekoutx === null){
            return "Check In";
          }
        }
      }
    ],
  });

	$('#TUser tbody').on( 'click', '.cekout', function (e) {
		var data = dataTuser.row( $(this).parents('tr') ).data();
		$('#formCekOut')[0].reset();
    $("#modalCekOut").modal("show");
   // alert(data.CekIn);
  	$.ajax({
      url: "{{ URL::route('editor.visitor.getDataDtl') }}",
      data: { visitor_id: data.id, visitor_activity_id: data.CekIn 	},
      type: "get",
      dataType: "json",
      cache: false,
      success: function(data) {
      	$("#visitor_id").val(data.visitor[0].id);
  			$("#visitor_activity_id").val(data.visitor_activity[0].id);
      	$("#covisitorId").val(data.visitor[0].id);
      	$("#coname").val(data.visitor[0].name);
      	$("#coaddress").val(data.visitor[0].address);
      	$("#comobile").val(data.visitor[0].mobile);
      	$("#cocompany").val(data.visitor[0].company);
      	$("#copic").val(data.visitor_activity[0].pic);
      //	$("#cofloor").val(data.visitor_activity[0].floor);
      	$("#copurpose").val(data.visitor_activity[0].purpose);
      	$("#cocard_id").val(data.visitor_activity[0].card_id);
       // $("#cofloor").select2().select2('val', data.visitor_activity[0].floor);
       //  append option
$("#cofloor").append('<option value="'+data.visitor_activity[0].floor+'">'+data.visitor_activity[0].floor+'</option>');
      }
    });
  });
$('#TUser tbody').on( 'click', '.cekin', function (e) {
		var data = dataTuser.row( $(this).parents('tr') ).data();
		$('#formCekIn')[0].reset();
  	$("#modalCekIn").modal("show");
  	$.ajax({
      url: "{{ URL::route('editor.visitor.getDataDtl') }}",
      data: { visitor_id: data.id, visitor_activity_id: 0	},
      type: "get",
      dataType: "json",
      cache: false,
      success: function(data) {
      	$("#civisitor_id").val(data.visitor[0].id);
        $("#civid").val(data.visitor[0].vid);
  			$("#civisitor_activity_id").val(0);
      	$("#ciname").val(data.visitor[0].name);
      	$("#ciaddress").val(data.visitor[0].address);
      	$("#cimobile").val(data.visitor[0].mobile);
      	$("#cicompany").val(data.visitor[0].company);
      }
    });
  });
      $("#updateUser").click(function(e){
        var postData = $( "#UpdateForm" ).serialize();
        $.ajax({
          url: "{{ URL::route('editor.visitor.updateData') }}",
          data: postData,
          type: "post",
          dataType: "json",
          cache: false,
          success: function(data) {
            if($.isEmptyObject(data.error)){
              toastr.success(data.msg);
              // refresh grid with new data
               $("#TUser").jsGrid("loadData");
               $("#UpdateModal").modal("hide");
            }else{
              printErrorMsg(data.error);
            }
          }
        });
      });
      $("#updatePasswordUser").click(function(e){
        var postData = $( "#UpdatePasswordForm" ).serialize();
        $.ajax({
          url: "{{ URL::route('editor.visitor.resetPassword') }}",
          data: postData,
          type: "POST",
          dataType: "json",
          cache: false,
          success: function(data) {
            if($.isEmptyObject(data.error)){
              toastr.success(data.success);
              // refresh grid with new data
               $("#TUser").jsGrid("loadData");
            // alert(data.success);
            }else{
              printErrorMsg(data.error);
            }
          }
        });
        $("#UpdateModal").modal("hide");
      });
    });
</script>
@endsection