@extends('layouts.template')
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

						<!-- begin:: Content Head -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							<div class="kt-container  kt-container--fluid ">
								<div class="kt-subheader__main">
									<h3 class="kt-subheader__title">
										Block User
									</h3>
									<span class="kt-subheader__separator kt-subheader__separator--v"></span>
									<div class="kt-subheader__group" id="kt_subheader_search">
										<form class="kt-margin-l-20" id="kt_subheader_search_form">
											<div class="kt-input-icon kt-input-icon--right kt-subheader__search" style="padding:10px 10px 10px 10px;">
											<select id="sel2" name="sel2" class='sel2 form-control'>
											</select>
												
											</div>
										</form>
									</div>
									<div class="kt-subheader__group kt-hidden" id="kt_subheader_group_actions">
										<div class="kt-subheader__desc"><span id="kt_subheader_group_selected_rows"></span> Selected:</div>
										<div class="btn-toolbar kt-margin-l-20">
											<div class="dropdown" id="kt_subheader_group_actions_status_change">
												<button type="button" class="btn btn-label-brand btn-bold btn-sm dropdown-toggle" data-toggle="dropdown">
													Update Status
												</button>
												<div class="dropdown-menu">
													<ul class="kt-nav">
														<li class="kt-nav__section kt-nav__section--first">
															<span class="kt-nav__section-text">Change status to:</span>
														</li>
														<li class="kt-nav__item">
															<a href="#" class="kt-nav__link" data-toggle="status-change" data-status="1">
																<span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-success kt-badge--inline kt-badge--bold">Approved</span></span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="#" class="kt-nav__link" data-toggle="status-change" data-status="2">
																<span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--bold">Rejected</span></span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="#" class="kt-nav__link" data-toggle="status-change" data-status="3">
																<span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--bold">Pending</span></span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="#" class="kt-nav__link" data-toggle="status-change" data-status="4">
																<span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-info kt-badge--inline kt-badge--bold">On Hold</span></span>
															</a>
														</li>
													</ul>
												</div>
											</div>
											<button class="btn btn-label-success btn-bold btn-sm btn-icon-h" id="kt_subheader_group_actions_fetch" data-toggle="modal" data-target="#kt_datatable_records_fetch_modal">
												Fetch Selected
											</button>
											<button class="btn btn-label-danger btn-bold btn-sm btn-icon-h" id="kt_subheader_group_actions_delete_all">
												Delete All
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- end:: Content Head -->
							<!--begin:: Widgets/Stats-->
							<div class="kt-portlet kt-margin-l-20 kt-margin-r-20">
								<div class="kt-portlet__body  kt-portlet__body--fit">
									<div class="row row-col-separator-lg">
										<div class="col-md-7 col-lg-7 col-xl-7">

											<!--begin::Current Check-In-->
											<div class="kt-widget24">
												<h2>Visitor Detail</h2>
							                    <br>
							                    <form id="blockForm" name="blockForm" method="post">
												{{ csrf_field() }}
												<input type="hidden" readonly id='user_id' name="user_id" value="{{ Auth::user()->id }}">
							                      <div class="form-group row">
													<label class="col-form-label col-sm-4">Visitor Date</label>
													<div class="col-sm-8">
														<div class="input-daterange input-group" id="kt_datepicker_5">
															<input type="text" readonly class="form-control" id="start" name="start" />
															<div class="input-group-append">
																<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
															</div>
															<input type="text" readonly class="form-control" id="end" name="end" />
														</div>
													</div>
												</div>
							                      <div class="form-group row">
							                        <label for="name" class="col-sm-4 col-form-label">Visitor Name</label>
							                        <div class="col-sm-8">
							                          <input type="text" id="name" name="name" class="form-control form-control-sm" placeholder="Enter visitor's Name.">
							                        </div>
							                      </div>
							                      <div class="form-group row">
							                        <label for="visitor_id" class="col-sm-4 col-form-label">visitor ID</label>
							                        <div class="col-sm-8">
							                          <input type="text" id="visitor_id" name="visitor_id" class="form-control form-control-sm" placeholder="Enter visitor's ID.">
							                        </div>
							                      </div>
							                      <div class="form-group row">
							                        <label for="address" class="col-sm-4 col-form-label">visitor Address</label>
							                        <div class="col-sm-8">
							                         <input type="text" id="address" class="form-control form-control-sm" name="address" placeholder="Enter visitor's home address.">
							                        </div>
							                      </div>
							                    </form>
											</div>

											<!--end::Current Check-In-->
										</div>
										<div class="col-md-5 col-lg-5 col-xl-5">

											<!--begin::Pre Register-->
											<div class="kt-widget24">

												<form>
							                      <div class="form-group">
							                  		<div class="row" style="padding-left: 100px;">
							                    		<div class="col-sm-6 col-lg-6">
							                     			<div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_user_avatar_3">
							                          			<div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_user_avatar_3">
																	<div class="kt-avatar__holder" style="background-image: url(assets/media/users/default.jpg)"></div>
																</div>
							                      			</div>
							                      		</div>
							                    		<div class="col-sm-6 col-lg-6">
							                       			<div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_user_avatar_3">
							                         			<div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_user_avatar_3">
																	<div class="kt-avatar__holder" style="background-image: url(assets/media/users/default.jpg)"></div>
																</div>
							                      			</div>
							                    		</div>
							                  		</div>
							                	</div>
							                <div class="form-group row">
							                    <label for="CardId" class="col-sm-2 col-form-label">Remarks</label>
							                    <div class="col-sm-10">
							                      	<textarea class="form-control" style="height: 100px"></textarea>	
							                    </div>
							                </div>

							                <div class="form-group" style="padding-left: 200px;">
							                  <button type="button" id="BlockUser" name="BlockUser" class="btn btn-success btn-sm cekin">Block User</button>
							                </div>
						                    </form>                    
										</div>

											<!--end::Pre Register-->
										</div>
									</div>
								</div>
							</div>
							

							<!--end:: Widgets/Stats-->

							<!--Begin::Row-->
							<div class="row kt-margin-l-10 kt-margin-r-10">
								<div class="col-xl-12 col-lg-12">

									<!--begin:: Check-In Guest-->
									<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
										<div class="kt-portlet__body">

											<!--Begin::Tab Content-->
											<div class="tab-content">

												<div class="kt-margin-t-10 kt-margin-l-10">
												<table id="TUser" class="table table-striped- table-bordered table-hover table-checkable">
													<thead>
														<tr>
															<th>Id</th>
															<th>Date</th>
															<th>Name</th>
														<!--	<th>Status</th> -->
														</tr>
													</thead>
												</table>
												</div>
											<!--End::Tab Content-->
										</div>
									</div>

									<!--end:: Check-In Guest-->
								</div>

							
							<!--End::Row-->

							<!--End::Dashboard 6-->
						</div>

						<!-- end:: Content -->
							</div>


				</div>
									
									</div>
								</div>
@endsection
@section('script')
<script type="text/javascript">
var dataTuser = $('#TUser').DataTable({
        'searching': true,
        "processing": true,
        "serverSide": true,
        "pagingType": "full_numbers",
        'fnDrawCallback': function (oSettings) {
        $('.dataTables_filter').each(function () {
            $(this).empty().append(
               /*  '<input type="text" id="fromDate" placeholder="From..." class="TANGGAL form-control form-control-sm">&nbsp :&nbsp<input type="text" id="toDate" placeholder="To..." class="TANGGAL form-control form-control-sm"> &nbsp'
                +'<input type="button"  class="btn btn-default xs" id="carix" value="search"> &nbsp<br /><br />'
                +'<select id="ssec" class="col-sm-2" name="ssec"><option value="bypdf">pdf</option><option value="byexcel">excel</option></select> &nbsp'
                +'<button type="button" id="insertButton" class="btn btn-primary xs">Export</button> &nbsp'
                */
            );
            $("#ssec").select2({width:"100px"});
            $('.TANGGAL').datepicker({
            format: 'yyyy-mm-dd'
            });
            $("#insertButton").click(function (e) {
            var favorite = [];
            $("input:checkbox[name=tipe]:checked").each(function(){
                favorite.push($(this).val());
            });
            var url = "{{ URL::route('editor.report.export_query') }}?id=" + favorite;

            window.location = url;
            });
            $("#carix").click(function(e){
                var searchByName = $('#searchByName').val();
                var ssec = $("#ssec").val();
                var from = $("#fromDate").val();
                var to = $("#toDate").val();
                dataTuser.ajax.reload();
            });
        });
        },
        "ajax": {
        "url" : "{{ route('editor.visitorblock.getData') }}",
        "data": function(data){
            data.searchByName = function() { return $('#searchByName').val() };
            data.ssec = function() { return $('#ssec').val() };
            data.from = function () { return $("#fromDate").val() };
            data.to = function () { return $("#toDate").val() };
        }
        },
        "columns": [
            { "data": "id", "width": 10, "orderable": false },
			{ "data": "blok_at" , "orderable": false},
            { "data": "name" , "orderable": false},
            /* { "data":  "blok", "orderable": false, 
                render: function ( data, type, row ) {
                    var status = row.blok;
                    var RowId = row.id;
                    if( status == 1){
                        return "<button class='cekin' id='"+RowId+"'>Edit "+RowId+"</button> <button class='app' id='"+RowId+"'>Need Approvel</button>";
                    } 
                }
            }*/
        ],
    });
	$(".sel2").select2({
		ajax: {
			url: "{{ URL::route('editor.visitorblock.getDatac') }}",
			type: "get",
			placeholder: 'Choose',
          	allowClear: true,
			dataType: 'json',
			data: function (params) {
				return {
					searchTerm: params.term,
					"_token": "{{ csrf_token() }}",
		   		};
		   	},
		   	processResults: function (response) {
				return {
		        	results: response
		     	};
		   	},
		   	cache: false
		},
		width:"100%"
	}).on('change', function(e) {
		$.ajax({
            url: "{{ URL::route('editor.visitorblock.getDataDtl') }}",
            data: { visitor_id: $(this).val()},
            type: "get",
            dataType: "json",
            cache: false,
            success: function(data) {
                $("#visitor_id").val(data.Visitor[0].id);
                $("#name").val(data.Visitor[0].name);
                $("#address").val(data.Visitor[0].address);
				$("#start").val(data.Visitor[0].cek_in);
				$("#end").val(data.Visitor[0].cek_in);
            }
        });
    });
	
	$("#BlockUser").click(function(e){
        var postData = $( "#blockForm" ).serialize();
        $.ajax({
			url: "{{ URL::route('editor.visitorblock.blockThis') }}",
			data: postData,
			type: "POST",
			dataType: "json",
			cache: false,
			success: function(data) {
				if($.isEmptyObject(data.error)){
					toastr.success(data.success);
					dataTuser.ajax.reload();
            	}else{
					printErrorMsg(data.error);
            	}
			}
        });
		$('#blockForm')[0].reset();
		dataTuser.ajax.reload();
	});
</script>
@endsection