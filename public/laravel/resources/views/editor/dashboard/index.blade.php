@extends('layouts.template')
@section('content')
<!--Begin::Dashboard 6-->
<br>
							<!--begin:: Widgets/Stats-->
							<div class="kt-portlet">
								<div class="kt-portlet__body  kt-portlet__body--fit">
									<div class="row row-col-separator-lg">
										<div class="col-md-12 col-lg-3 col-xl-3">

											<!--begin::Current Check-In-->
											<div class="kt-widget24">
												<div class="kt-widget24__details">
													<div class="kt-widget24__info">
														<h4 class="kt-widget24__title">
															Check-In
														</h4>
													</div>
													<span class="kt-widget24__stats kt-font-brand">
													{{ $totalin }}
													</span>
												</div>
											</div>

											<!--end::Current Check-In-->
										</div>
										<div class="col-md-12 col-lg-3 col-xl-3">

											<!--begin::Pre Register-->
											<div class="kt-widget24">
												<div class="kt-widget24__details">
													<div class="kt-widget24__info">
														<h4 class="kt-widget24__title">
															Check-Out
														</h4>
													</div>
													<span class="kt-widget24__stats kt-font-warning">
													{{ $totalout }}
													</span>
												</div>
											</div>

											<!--end::Pre Register-->
										</div>
										<div class="col-md-12 col-lg-3 col-xl-3">

											<!--begin::Check-Out-->
											<div class="kt-widget24">
												<div class="kt-widget24__details">
													<div class="kt-widget24__info">
														<h4 class="kt-widget24__title">
															Pre-Register
														</h4>
													</div>
													<span class="kt-widget24__stats kt-font-danger">
													{{ $totalpre }}
													</span>
												</div>
											</div>

											<!--end::Check-Out-->
										</div>
										<div class="col-md-12 col-lg-3 col-xl-3">

											<!--begin::Total Visitor-->
											<div class="kt-widget24">
												<div class="kt-widget24__details">
													<div class="kt-widget24__info">
														<h4 class="kt-widget24__title">
															Total Visitor
														</h4>
													</div>
													<span class="kt-widget24__stats kt-font-success">
														{{ $totalVis }}
													</span>
												</div>
											</div>

											<!--end::Total Visitor-->
										</div>
									</div>
								</div>
							</div>

							<!--end:: Widgets/Stats-->

							<!--Begin::Row-->
							<div class="row">
								<div class="col-xl-6 col-lg-6">

									<!--begin:: Check-In Guest-->
									<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Check-In Guest
												</h3>
											</div>
										</div>
										<div class="kt-portlet__body">

											<!--Begin::Tab Content-->
											<div class="tab-content">

												<!--begin::tab 1 content-->
												<div class="tab-pane active" id="kt_widget11_tab1_content">

													<!--begin::Widget 11-->
													<div class="kt-widget11">
														<div class="table-responsive">
															<table class="table">
																<thead>
																	<tr>
																		<td style="width:1%">#</td>
																		<td style="width:40%">Name</td>
																		<td style="width:20%">Destination</td>
																		<td style="width:20%">Floor</td>
																		<td style="width:20%">Company</td>
																	</tr>
																</thead>
																<tbody>
																@foreach($in as $key => $val)
																<tr>
																		<td>
																			<label class="kt-checkbox kt-checkbox--single">
																				<input type="checkbox"><span></span>
																			</label>
																		</td>
																		<td>{{ $val->names }}</td>
																		<td>{{ $val->purpose }}</td>
																		<td>{{ $val->floor }}</td>
																		<td>{{ $val->company }}</td>
																	</tr>
																@endforeach
																	
																</tbody>
															</table>
														</div>
													</div>

													<!--end::Widget 11-->
												</div>

												<!--end::tab 1 content-->
											</div>

											<!--End::Tab Content-->
										</div>
									</div>

									<!--end:: Check-In Guest-->
								</div>
								<div class="col-xl-6 col-lg-6">

									<!--begin:: Check-In Statistics-->
									<div class="kt-portlet kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Check-In Statistics
												</h3>
											</div>
										</div>
										<div class="kt-portlet__body kt-portlet__body--fluid">
											<div class="kt-widget12">
												<div class="kt-widget12__chart" style="height:250px;">
													<!-- <canvas id="kt_chart_order_statistics"></canvas> -->
													
													<canvas id="myChart"></canvas>
												</div>
											</div>
										</div>
									</div>

									<!--end:: Check-In Statistics-->
								</div>

							<!--End::Row-->
								<div class="col-xl-4 col-lg-4 order-lg-1 order-xl-1">

									<!--begin:: Machines-->
									<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Machines
												</h3>
											</div>
										</div>
										<div class="kt-portlet__body">
											<div class="tab-content">
												<div class="tab-pane active" id="kt_widget4_tab1_content">
													<div class="kt-widget4">
													@foreach($alat as $key => $val)
														<div class="kt-widget4__item">
															<div class="kt-widget4__pic kt-widget4__pic--pic">
																<img src="{{Config::get('constants.path.media')}}/products/fp1.jpg" alt="">
															</div>
															<div class="kt-widget4__info">
																<a href="#" class="kt-widget4__username">
																	{{ $val['nama_alat'] }}
																</a>
																<p class="kt-widget4__text">
																	{{ $val['nama_alat'] }} Door
																</p>
															</div>
															@if($val['aktif'] =='1')         
																<a href="#" class="btn btn-sm btn-label-brand btn-bold">On</a>       
															@else
																<a href="#" class="btn btn-sm btn-label-warning btn-bold">Off</a>  
															@endif
														</div>
													@endforeach
														
													</div>
												</div>
											</div>
										</div>
									</div>

									<!--end:: Machines-->

									<!--End::Portlet-->
								</div>
								<div class="col-xl-4 col-lg-4 order-lg-1 order-xl-1">

									<!--Begin::Portlet-->
									<div class="kt-portlet kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Recent Activities
												</h3>
											</div>
										</div>
										<div class="kt-portlet__body">

											<!--Begin::Timeline 3 -->
											<div class="kt-timeline-v2">
												<div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">
											@foreach($all as $key => $val)
												@if($val->cek_out == "")
													<div class="kt-timeline-v2__item">
														<span class="kt-timeline-v2__item-time"></span>
														<div class="kt-timeline-v2__item-cricle">
															<i class="fa fa-genderless kt-font-success"></i>
														</div>
														<div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold">
															Check-In User : {{ $val->names}}
														</div>
													</div>
													@else
													<div class="kt-timeline-v2__item">
														<span class="kt-timeline-v2__item-time"></span>
														<div class="kt-timeline-v2__item-cricle">
															<i class="fa fa-genderless kt-font-warning"></i>
														</div>
														<div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold">
															Check-Out User : {{ $val->names}}
														</div>
													</div>  
												@endif
											@endforeach
												</div>
											</div>
													<!--End::Timeline 3 -->
												</div>
											</div>
										</div>
										<div class="col-xl-4 col-lg-4 order-lg-1 order-xl-1">

									<!--Begin::Portlet-->
									<!--begin:: Widgets/Latest Updates-->
									<div class="kt-portlet kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Latest Updates
												</h3>
											</div>
										</div>
										<div class="kt-portlet__body">

											<!--begin::widget 12-->
											<div class="kt-widget4">
												<div class="kt-widget4__item">
													<span class="kt-widget4__icon">
														<i class="flaticon2-notification kt-font-warning"></i>
													</span>
													<a href="#" class="kt-widget4__title kt-widget4__title--light">
														VMS has been arrived!
													</a>
												</div>
												<div class="kt-widget4__item">
													<span class="kt-widget4__icon">
														<i class="flaticon2-rocket kt-font-info"></i>
													</span>
													<a href="#" class="kt-widget4__title kt-widget4__title--light">
														VMS will be landing soon...
													</a>
												</div>
												<div class="kt-widget4__item">
													<span class="kt-widget4__icon">
														<i class="flaticon2-file kt-font-brand"></i>
													</span>
													<a href="#" class="kt-widget4__title kt-widget4__title--light">
														VMS is in progress. Stay tuned!
													</a>
												</div>
											</div>

											<!--end::Widget 12-->
										</div>
									</div>

									<!--end:: Widgets/Last Updates-->
								</div>
							</div>

							<!--End::Row-->

							<!--End::Dashboard 6-->
@endsection
@section('script')
<script>
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
	type: 'pie',
    data: {
      labels: ["PreRegister", "In", "Out"],
      datasets: [{
        label: "Population (millions)",
        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
        data: [{{ $totalpre }}, {{ $totalin }}, {{ $totalout }}]
      }]
    },
	options: {
	  legend: { display: true },
	  layout: {
            padding: {
                left: 0,
                right: 0,
                top: 0,
                bottom: 50
            }
        },
      title: {
        display: false,
        text: 'Predicted world population (millions) in 2050'
      }
    }
	/* type: 'bar',
    data: {
      labels: ["Visitor"],
      datasets: [
		{
          label: "#PreRegister ",
          backgroundColor: ["#bcc900"],
          data: [{{ $totalpre }}]
		},
        {
          label: "#In ",
          backgroundColor: ["#00c90d"],
          data: [{{ $totalin }}]
		},
		{
          label: "#Out",
          backgroundColor: ["#ff0000"],
          data: [{{ $totalout }}]
        }
      ]
    },
    options: {
	  legend: { display: true },
	  layout: {
            padding: {
                left: 0,
                right: 0,
                top: 0,
                bottom: 50
            }
        },
      title: {
        display: false,
        text: 'Predicted world population (millions) in 2050'
      }
    }*/
});
</script>
@endsection