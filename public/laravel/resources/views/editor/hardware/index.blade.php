@extends('layouts.template')
@section('content')
{{-- @actionStart('module', 'read') --}}
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				Device Management
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
					{{-- @actionStart('module', 'create') --}}
					<a href="#add" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal">
						<i class="la la-plus"></i>
						Add New
					</a>
					{{-- @actionEnd --}}
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">
		<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
			<thead>
				<tr>
					<th>No</th>
					<th>Name Device</th>
					<th>Floor</th>
					<th>List Ip</th>
                    <th>Device Type</th>
                    <th>Active</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($hardwareInfos as $key => $data)
					<tr>
						<td>{{$number++}}</td>
                        <td>{{$data->name}}</td>
						<td>{{$data->floor}}</td>
                        <td>{{$data->list_ip}}</td>
						<td>{{$data->hardwareTypeName}}</td>
						<td>{{ $data->aktif == 1 ? 'activated' : 'deactivated' }}</td>
						<td>
							{{-- @actionStart('module', 'edit') --}}
							<a href="#edit{{$data->id}}" class="btn btn-sm btn-primary btn-icon btn-icon-md" title="Edit" data-toggle="modal">
								<i class="la la-edit"></i>
							</a>
							{{-- @actionEnd --}}
							
							{{-- @actionStart('module', 'delete') --}}
							<a href="#delete{{$data->id}}" class="btn btn-sm btn-google btn-icon btn-icon-md" title="Delete" data-toggle="modal">
								<i class="la la-trash-o"></i>
							</a>
							{{-- @actionEnd --}}
						</td>
					</tr>
					
					{{-- BEGIN MODAL EDIT --}}
					<div class="modal fade" id="edit{{$data->id}}" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
						<div class="modal-dialog">
							{!! Form::open(array('route' => ['editor.hardware.update', $data->id], 'method' => 'PUT'))!!}
							{{ csrf_field() }}
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Edit Device</h5>
								</div>
								<div class="modal-body">
									<div class="form-group">
	                                    <label><input type="checkbox" id="aktif" name="aktif" value="1" {{ $data->aktif == 1 ? 'checked' : '' }} /> Actived</label>
	                                </div>
	                                <div class="form-group">
	                                    <label>Device Name</label>
	                                    <input type="text" class="form-control col-lg-12" name="name" value="{{$data->name}}">
	                                </div>
									<div class="form-group">
										<label>Device Floor</label>
										<input type="text" class="form-control" name="floor" id="floor" value="{{$data->floor}}">
									</div>
	                                <div class="form-group">
	                                    <label>Device folder</label>
	                                    <input type="text" class="form-control" name="folder" value="{{$data->folder}}">
	                                    <span>** for EntryPass Device **</span>
	                                </div>
	                                <div class="form-group">
	                                    <label>Device IP addres</label>
	                                    <input type="text" class="form-control" name="list_ip" value="{{$data->list_ip}}">
	                                </div>
	                                <div class="form-group">
	                                    <label>Device Type</label>
	                                    {{Form::select('id_hardware_type', $hardwareTypes, $data->id_hardware_type, array('class' => 'form-control'))}}
	                                </div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary">Save</button>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
					{{-- END MODAL EDIT --}}

					{{-- BEGIN MODAL DELETE --}}
					<div class="modal fade" id="delete{{$data->id}}" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
						<div class="modal-dialog">
							{!! Form::open(array('route' => ['editor.hardware.delete', $data->id], 'method' => 'delete'))!!}
							{{ csrf_field() }}
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Delete Data</h5>
								</div>
								<div class="modal-body">
									Are you sure want to delete this data?
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
									<button type="submit" class="btn btn-primary">Yes</button>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
					{{-- END MODAL DELETE --}}
                @endforeach
			</tbody>
		</table>

		<!--end: Datatable -->
	</div>

	{{-- BEGIN MODAL ADD --}}
	<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog" role="document">
			{!! Form::open(array('route' => ['editor.hardware.store'], 'method' => 'POST'))!!}
			{{ csrf_field() }}
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add New </h5>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label><input type="checkbox" id="aktif_baru" name="aktif" value="1"> Actived</label>
					</div>
					<div class="form-group">
						<label>Device Name</label>
						<input type="text" class="form-control" name="name">
                    </div>
					<div class="form-group">
						<label>Device Floor</label>
						<input type="text" class="form-control" name="floor" id="floor">
                    </div>
                    <div class="form-group">
						<label>Device folder</label>
						<input type="text" class="form-control" name="folder">
						<span>** for EntryPass Device **</span>
					</div>
                    <div class="form-group">
						<label>Device IP addres</label>
						<input type="text" class="form-control" name="list_ip">
                    </div>
                    <div class="form-group">
						<label>Device Type</label>
						{{Form::select('id_hardware_type', $hardwareTypes, null, array('class' => 'form-control'))}}
                    </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary" name="saveAdd" value="1">Save</button>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
	{{-- END MODAL ADD --}}
</div>
{{-- @actionEnd --}}<br />

@endsection