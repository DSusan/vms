@extends('layouts.template')
@section('content')
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">
       Face Recognition
      </h3>
    </div>
    <div class="kt-portlet__head-toolbar">
      <div class="kt-portlet__head-wrapper">
        <div class="kt-portlet__head-actions">
        </div>
      </div>
    </div>
  </div>
  <div class="kt-portlet__body">
    <div class="row">
      <div class="col-12">
      <input type="hidden" id="civisitor_id" name="visitor_id">
          <input type="hidden" id="civisitor_activity_id" name="visitor_activity_id">
          <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <input type="hidden"  readonly name="user_id" value="{{ Auth::user()->id }}">
                  <label for="visitorId">Visitor Id</label> 
                  <input type="number" step="any" id="civisitor_id" class="form-control form-control-sm" name="visitor_id" placeholder="Ex. 1110501025" disabled>
                  <span class="help-block">*auto-generated.</span>
                </div>
                <div class="form-group">
                  <label for="visitorName">Visitor Name</label> 
                  <input type="text" id="ciname" class="form-control form-control-sm" name="name">
                </div>
                <div class="form-group">

                      <label for="visitorAddress">Visitor Address</label> 
                      <input type="text" id="ciaddress" class="form-control form-control-sm" name="address" placeholder="Enter visitor's home address.">
            
                </div>
                <div class="form-group">
                      <label for="companyName">Company Name</label> 
                      <input type="text" id="cicompany" class="form-control form-control-sm" name="company" placeholder="Enter visitor's company-affiliated name.">
            
                </div>
                <div class="form-group">

                      <label for="companyName">Mobile</label> 
                      <input type="text" id="cimobile" class="form-control form-control-sm" name="mobile" placeholder="Enter mobile phone number, with format : +62 811 888 8888.">
            
                </div>
              
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-6">
                    <form runat="server">
                      <input type='file' id="imgInp" /><br /><br />
                      <img id="blah" src="#" width="100%" height="100%" alt="..." class="img-thumbnail">
                    </form>
                      <!-- <img src="{{Config::get('constants.path.media')}}/users/default.jpg" width="100%" height="100%" alt="..." class="img-thumbnail"> -->
                    </div>
                    <div class="col-lg-6">
                      <div id="webcam">
                          <input type=button value="Capture" onClick="preview()" class="img-thumbnail">
                      </div>
                      <div id="simpan" style="display:none">
                          <input type=button value="Remove" onClick="batal()">
                          <input type=button value="Save" onClick="simpan()" style="font-weight:bold;">
                      </div>
                      <div id="camera">Capture</div>
    
                      
 
                      <div id="hasil"></div>
                   <!--   <img src="{{Config::get('constants.path.media')}}/users/default.jpg" width="100%" height="100%" alt="..." class="img-thumbnail">
                   -->
                    </div>
                  </div>
                </div>
              </div>  
            </div>
            <div class="row">
              <div class="col-lg-6">
                 <div class="form-group">
                  <label>Host User:</label>
                  <input type='text' class="form-control form-control-sm" name="pic" id="cipic">
                </div>
                <div class="form-group">
                  <label>Floor:</label>
                  <input type='text' class="form-control form-control-sm" name="floor" id="cifloor">
                </div>
                <div class="form-group">
                  <label>Purpose:</label>
                  <input type='text' class="form-control form-control-sm" name="purpose" id="cipurpose">
                </div>
              </div>
             
            </div>
      </div>  
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="{{Config::get('constants.path.plugins')}}/webcamjs/webcam.min.js" type="text/javascript"></script>
<script type="text/javascript">
// konfigursi webcam
Webcam.set({
  width: 320,
  height: 240,
  image_format: 'jpg',
  jpeg_quality: 100
});
Webcam.attach( '#camera' );
function preview() {
  // untuk preview gambar sebelum di upload
  Webcam.freeze();
  // ganti display webcam menjadi none dan simpan menjadi terlihat
  document.getElementById('webcam').style.display = 'none';
  document.getElementById('simpan').style.display = '';
}
function batal() {
  // batal preview
  Webcam.unfreeze();
  // ganti display webcam dan simpan seperti semula
  document.getElementById('webcam').style.display = '';
  document.getElementById('simpan').style.display = 'none';
}
function simpan() {
  // ambil foto
  Webcam.snap( function(data_uri) {
    // upload foto
    //Webcam.upload( data_uri, 'upload.php', function(code, text) {} );
    // tampilkan hasil gambar yang telah di ambil
    document.getElementById('hasil').innerHTML = 
      '<br /><p>Result : </p>' + 
      '<img src="'+data_uri+'" class="img-thumbnail"/>';
    Webcam.unfreeze();
    document.getElementById('webcam').style.display = '';
    document.getElementById('simpan').style.display = 'none';
  });
}
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}
$(document).ready(function(){
  $("#imgInp").change(function() {
    readURL(this);
  });
});
</script>
@endsection