@extends('layouts.template')
@section('content')
<style type="text/css">
  .jsgrid-cell {
    overflow: block;
}

</style>
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">
        Activity Management
      </h3>
    </div>
    <div class="kt-portlet__head-toolbar">
      <div class="kt-portlet__head-wrapper">
        <div class="kt-portlet__head-actions">
        </div>
      </div>
    </div>
  </div>
  <div class="kt-portlet__body">
    <div class="row">
      
      <div class="col-12">
              <!-- <div id="TUser"></div> -->
              <table id="TUser" class="table table-striped- table-bordered table-hover table-checkable">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>visitor_name</th>
                    <th>pic</th>
                    <th>floor</th>
                    <th>card_id</th>
                    <th>cek_in</th>
                    <th>cek_out</th>
                    <th>purpose</th>
                  </tr>
                </thead>
              </table>
      </div>    
    </div>
  </div>
</div>
<!-- update form -->
<form id="UpdateForm" name="UpdateForm" method="post" >
  {{ csrf_field() }}
<div class="modal fade" id="UpdateModal" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Update</h4>
            </div>
            <div class="modal-body">
             <input type='hidden' id="updateId" name='id' readonly>
              <div class="form-group">
              <label>Name:</label>
                <input type="text" id="updateName" name="name" class="form-control" placeholder="name">
              </div>
               <div class="form-group">
                <label>Address:</label>
                <input type="text" id="updateAddress" name="address" class="form-control" placeholder="address">
              </div>
              <div class="form-group">
                <label>Password:</label>
                <input type="text" id="updateMobile" name="mobile" class="form-control" placeholder="mobile">
              </div>
              <div class="form-group">
                <strong>Company:</strong>
                <input type="text" id="updateCompany" name="company" class="form-control" placeholder="company">
              </div>
              <div class="form-group">
                <strong>Card Id:</strong>
                <input type="text" id="updateCardId" name="card_id" class="form-control" placeholder="CardId">
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" id="updateUser" class="btn btn-primary">Submit</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
</form>
<form id="insertForm" name="insertForm" method="post" >
{{ csrf_field() }}
<div class="modal fade" id="insertModal" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          <b><u>Check In Detail</u></b></h4>
        </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <input type="hidden"  readonly name="user_id" value="{{ Auth::user()->id }}">
                  <label>Visitor Detail Info</label>
                  <select id="insertIdVisitor" class="sel2" name="visitor_id"><option value="0">Visitor Id</option></select> 
                </div>
                <div id="detailVisitor"></div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label>Card Id</label>
                  <input type='number' id="insertCardId" name='card_id' class="form-control">
                </div>
                <div class="form-group">
                  <label>Host User:</label>
                  <input type='text' class="form-control" name="pic" id="insertPic">
                </div>
                <div class="form-group">
                  <label>Floor:</label>
                  <input type='text' class="form-control" name="floor" id="insertFloor">
                </div>
                <div class="form-group">
                  <label>Purpose:</label>
                  <input type='text' class="form-control" name="purpose" id="insertPurpose">
                </div>
              </div>
            </div>
          </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" id="submitNew" class="btn btn-primary">Submit</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
</form>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function (e) {
      $(".sel2").select2({
        ajax: { 
          url: "{{ URL::route('editor.visitoractivity.getVisitor') }}",
          type: "get",
          placeholder: 'Choose',
          allowClear: true,
          dataType: 'json',
          data: function (params) {
            return {
              searchTerm: params.term,
              "_token": "{{ csrf_token() }}"
            }
          },
          processResults: function (response) {
            return {
                  results: response
              };
            },
            cache: false
        },
        width:"100%",
      }).on('change', function(e) {
        $("#detailVisitor").empty();
        $.ajax({
          url: "{{ URL::route('editor.visitoractivity.getVisitorDtl') }}",
          data: { id:$(this).val(), "_token": "{{ csrf_token() }}" },
          type: "get",
          dataType: "json",
          cache: false,
          success: function(data) {
            $.each(data, function(i, e) {
              $("#detailVisitor").append(
                "<div class='form-group'>"
                + "<label>Visitor Id</label><input type='text' class='form-control' disabled value='"+e.id+"'>"
                + "</div>"
                + "<div class='form-group'>"
                + "<label>Address</label><input type='text' class='form-control' disabled value='"+e.address+"'>"
                + "</div>"
                + "<div class='form-group'>"
                + "<label>Mobile</label><input type='text' class='form-control' disabled value='"+e.mobile+"'>"
                + "</div>"
                + "<div class='form-group'>"
                + "<label>Company</label><input type='text' class='form-control' disabled value='"+e.company+"'>"
                + "</div>"
              );
            });
          }
        });
      });
      $("#insertButton").click(function(e){
        $("#insertModal").modal("show");
      });
      
      var dataTuser = $('#TUser').DataTable( {
      //  "dom": '<"toolbar">flrtip',
        'searching': true,
        "processing": true,
        "serverSide": true,
        "pagingType": "full_numbers",
        'fnDrawCallback': function (oSettings) {
          $('.dataTables_filter').each(function () {
            $(this).empty().append('<input type="text" id="searchByName" placeholder="Visitor" class="form-control form-control-sm"> <input type="button"  class="btn btn-default xs" id="carix" value="search">');
            $( "#carix" ).click(function(e){
                var searchByName = $('#searchByName').val();
               //.draw();
               dataTuser.ajax.reload();
               $("#searchByName").val(searchByName);
            });
          });
        },
        "ajax": {
          "url" : "{{ route('editor.visitoractivity.getData') }}",
          "data": function(data){
            // Append to data
            data.searchByName = function() { return $('#searchByName').val() };
          }
        },
        
        "columns": [
          { "data": "id", "orderable": true },
          { "data": "visitor_name" , "orderable": false},
          { "data": "pic" , "orderable": false},
          { "data": "floor", "orderable": false },
          { "data": "card_id", "orderable": false },
          { "data": "cek_in", "orderable": false },
          { "data": "cek_out", "orderable": false },
          { "data": "purpose", "orderable": false }
        ]
      });
      $("#submitNew").click(function(e){ 
        var postData = $( "#insertForm" ).serialize();
        $.ajax({
          url: "{{ URL::route('editor.visitoractivity.insertData') }}",
          data: postData,
          type: "POST",
          dataType: "json",
          cache: false,
          success: function(data) {
            if($.isEmptyObject(data.error)){
              toastr.success(data.success);
              $('#insertForm')[0].reset();
              $("#insertModal").modal("hide");
              // $("#TUser").jsGrid("loadData");
              dataTuser.ajax.reload( null, false );
            }else{
              printErrorMsg(data.error);
            }
          }
        });
      });
       
      /* $("#TUser").jsGrid({
        width: "100%",
        height: "500px",
        paging: true,   
        autoload: true,
        pageSize: 10,
        pageButtonCount: 3,
        pageLoading: true,
        updateOnResize: true,
        controller: {
            loadData: function(filter) {
            var d = $.Deferred();
            $.ajax({    
              url: "{{ route('editor.visitoractivity.getData') }}",
              data: filter,
              dataType: "json"
            }).done(function(response){
              d.resolve(response);
            });
            return d.promise();
          }
        },
        fields: [
          { name: "id", type: "text", width: 1 },
          { name: "visitor_name", type: "text", width: 30 },
          { name: "pic", type: "text", width: 10 },
          { name: "floor", type: "text", width: 5 },
          { name: "card_id", type: "text", width: 5 },
          { name: "cek_in", type: "text", width: 30 },
          { name: "cek_out", type: "text", width: 5 },
          { name: "purpose", type: "text", width: 70 },
          { name: "id", title:"token", type: "text", width: 70 ,
            itemTemplate: function(value, item) {
              var idData = item.id;
              var customEditButton = $("<button>").text("Edit").on("click", function() {
                  $("#UpdateForm .modal-title").empty().append("Update User "+idData);
                  $.ajax({
                    url: "{{ URL::route('editor.visitor.getDataDtl') }}",
                    data: {
                      id: idData,
                      "_token": "{{ csrf_token() }}",
                    },
                    type: "POST",
                    dataType: 'JSON',
                    cache: false,
                    success: function(data) {
                      $.each(data, function(i, e) {
                        $("#UpdateForm #updateId").empty().val(e.id);
                        $("#UpdateForm #updateName").empty().val(e.name);
                        $("#UpdateForm #updateAddress").empty().val(e.address);
                        $("#UpdateForm #updateCompany").empty().val(e.company);
                        $("#UpdateForm #updateMobile").empty().val(e.mobile);
                        $("#UpdateForm #updateCardId").empty().val(e.card_id);
                      });
                    }
                  });
                  $("#UpdateModal").modal("show");
              });
               var UpdatePasswordButton;
             return $("<div>").append(customEditButton).append(" ").append(UpdatePasswordButton);
            }
          },
        ]
      });*/
      $("#updateUser").click(function(e){
        var postData = $( "#UpdateForm" ).serialize();
        $.ajax({
          url: "{{ URL::route('editor.visitor.updateData') }}",
          data: postData,
          type: "post",
          dataType: "json",
          cache: false,
          success: function(data) {
            if($.isEmptyObject(data.error)){
              toastr.success(data.msg);
              // refresh grid with new data
               $("#TUser").jsGrid("loadData");
               $("#UpdateModal").modal("hide");
            }else{
              printErrorMsg(data.error);
            }
          }
        });
      });
      $("#updatePasswordUser").click(function(e){
        var postData = $( "#UpdatePasswordForm" ).serialize();
        $.ajax({
          url: "{{ URL::route('editor.visitor.resetPassword') }}",
          data: postData,
          type: "POST",
          dataType: "json",
          cache: false,
          success: function(data) {
            if($.isEmptyObject(data.error)){
              toastr.success(data.success);
              // refresh grid with new data
               $("#TUser").jsGrid("loadData");
            // alert(data.success);
            }else{
              printErrorMsg(data.error);
            }
          }
        });
        $("#UpdateModal").modal("hide");
      });
    });
</script>
@endsection