@extends('layouts.template')
@section('content')
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">
        Visitor
      </h3>
    </div>
    <div class="kt-portlet__head-toolbar">
      <div class="kt-portlet__head-wrapper">
        <div class="kt-portlet__head-actions">
        </div>
      </div>
    </div>
  </div>
  <div class="kt-portlet__body">
    <div class="row">
      <div class="col-12">
              <!-- <div id="TUser"></div>-->
        <table id="TUser" class="table table-striped- table-bordered table-hover table-checkable">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>visitor_name</th>
                    <th>Address</th>
                    <th>Company Name</th>
                    <th>Mobile</th>
                     <th>Status</th>
                  </tr>
                </thead>
              </table>
      </div>  
    </div>
  </div>
</div>
<!-- update form -->
<form id="UpdateForm" name="UpdateForm" method="post" >
  {{ csrf_field() }}
  <div class="modal fade" id="UpdateModal" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Update</h4>
            </div>
            <div class="modal-body">
             <input type='hidden' id="updateId" name='id' readonly>
              <div class="form-group">
              <label>Name:</label>
                <input type="text" id="updateName" name="name" class="form-control" placeholder="name">
              </div>
               <div class="form-group">
                <label>Address:</label>
                <input type="text" id="updateAddress" name="address" class="form-control" placeholder="address">
              </div>
              <div class="form-group">
                <label>Password:</label>
                <input type="text" id="updateMobile" name="mobile" class="form-control" placeholder="mobile">
              </div>
              <div class="form-group">
                <strong>Company:</strong>
                <input type="text" id="updateCompany" name="company" class="form-control" placeholder="company">
              </div>
              <div class="form-group">
                <strong>Card Id:</strong>
                <input type="text" id="updateCardId" name="card_id" class="form-control" placeholder="CardId">
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" id="updateUser" class="btn btn-primary">Submit</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
  </div>
      <!-- /.modal -->
</form>
<form id="formCekIn" name="formCekIn" method="post" >
	{{ csrf_field() }}
  <div class="modal fade" id="modalCekIn" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><b><u>Visitor Detail Cek In</u></b></h4>
        </div>
        <div class="modal-body">
        	<input type="text" id="civisitor_id" name="visitor_id">
          <input type="text" id="civisitor_activity_id" name="visitor_activity_id">
          <div class="row">
              <div class="col-lg-6">
              <div class="form-group">
                <input type="hidden"  readonly name="user_id" value="{{ Auth::user()->id }}">
                <label for="visitorId">Visitor Id</label> 
                <input type="number" step="any" id="civisitor_id" class="form-control form-control-sm" name="visitor_id" placeholder="Ex. 1110501025" disabled>
                <span class="help-block">*auto-generated.</span>
              </div>
              <div class="form-group">
                <label for="visitorName">Visitor Name</label> 
                <input type="text" id="ciname" class="form-control form-control-sm" name="name">
              </div>
              <div class="form-group">

                    <label for="visitorAddress">Visitor Address</label> 
                    <input type="text" id="ciaddress" class="form-control form-control-sm" name="address" placeholder="Enter visitor's home address.">
           
              </div>
              <div class="form-group">
                    <label for="companyName">Company Name</label> 
                    <input type="text" id="cicompany" class="form-control form-control-sm" name="company" placeholder="Enter visitor's company-affiliated name.">
           
              </div>
              <div class="form-group">

                    <label for="companyName">Mobile</label> 
                    <input type="text" id="cimobile" class="form-control form-control-sm" name="mobile" placeholder="Enter mobile phone number, with format : +62 811 888 8888.">
          
              </div>
              
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-6">
                      <img src="{{Config::get('constants.path.media')}}/users/default.jpg" width="100%" height="100%" alt="..." class="img-thumbnail">
                    </div>
                    <div class="col-lg-6">
                      <img src="{{Config::get('constants.path.media')}}/users/default.jpg" width="100%" height="100%" alt="..." class="img-thumbnail">
                    </div>
                  </div>
                </div>
                <div class="form-group">

                    <label for="CardId">Card Id</label> 
                    <input type="text"  class="form-control form-control-sm" name="card_id" id="cicard_id" placeholder="*card Number">
           
                </div>
                <div class="form-group">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  <button type="button" id="submitCekIn" class="btn btn-primary">Check In</button>
                </div>
              </div>  
            </div>
            <div class="row">
              <div class="col-lg-6">
                 <div class="form-group">
                  <label>Host User:</label>
                  <input type='text' class="form-control form-control-sm" name="pic" id="cipic">
                </div>
                <div class="form-group">
                  <label>Floor:</label>
                  <input type='text' class="form-control form-control-sm" name="floor" id="cifloor">
                </div>
                <div class="form-group">
                  <label>Purpose:</label>
                  <input type='text' class="form-control form-control-sm" name="purpose" id="cipurpose">
                </div>
              </div>
             
            </div>
        </div>
      </div>
    </div>
  </div>
</form>
<form id="formCekOut" name="formCekOut" method="post" >
  {{ csrf_field() }}
  <div class="modal fade" id="modalCekOut" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><b><u>Visitor Detail Cek Out</u></b></h4>
        </div>
        <div class="modal-body">
        	<input type="text" id="visitor_id" name="visitor_id">
          <input type="text" id="visitor_activity_id" name="visitor_activity_id">
          <div class="row">
              <div class="col-lg-6">
              <div class="form-group">
                <input type="hidden"  readonly name="user_id" value="{{ Auth::user()->id }}">
                <label for="visitorId">Visitor Id</label> 
                <input type="number" step="any" id="covisitorId" class="form-control form-control-sm" name="visitor_id" placeholder="Ex. 1110501025" disabled>
                <span class="help-block">*auto-generated.</span>
              </div>
              <div class="form-group">
                <label for="visitorName">Visitor Name</label> 
                <input type="text" id="coname" class="form-control form-control-sm" name="name">
              </div>
              <div class="form-group">

                    <label for="visitorAddress">Visitor Address</label> 
                    <input type="text" id="coaddress" class="form-control form-control-sm" name="address" placeholder="Enter visitor's home address.">
           
              </div>
              <div class="form-group">
                    <label for="companyName">Company Name</label> 
                    <input type="text" id="cocompany" class="form-control form-control-sm" name="company" placeholder="Enter visitor's company-affiliated name.">
           
              </div>
              <div class="form-group">

                    <label for="companyName">Mobile</label> 
                    <input type="text" id="comobile" class="form-control form-control-sm" name="mobile" placeholder="Enter mobile phone number, with format : +62 811 888 8888.">
          
              </div>
              
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-6">
                      <img src="{{Config::get('constants.path.media')}}/users/default.jpg" width="100%" height="100%" alt="..." class="img-thumbnail">
                    </div>
                    <div class="col-lg-6">
                      <img src="{{Config::get('constants.path.media')}}/users/default.jpg" width="100%" height="100%" alt="..." class="img-thumbnail">
                    </div>
                  </div>
                </div>
                <div class="form-group">

                    <label for="CardId">Card Id</label> 
                    <input type="text" id="cocard_id" class="form-control form-control-sm" name="card_id" placeholder="*card Number">
           
                </div>
                <div class="form-group">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  <button type="button" id="submitCekOut" class="btn btn-primary">Check Out</button>
                </div>
              </div>  
            </div>
            <div class="row">
              <div class="col-lg-6">
                 <div class="form-group">
                  <label>Host User:</label>
                  <input type='text' class="form-control form-control-sm" name="copic" id="copic">
                </div>
                <div class="form-group">
                  <label>Floor:</label>
                  <input type='text' class="form-control form-control-sm" name="cofloor" id="cofloor">
                </div>
                <div class="form-group">
                  <label>Purpose:</label>
                  <input type='text' class="form-control form-control-sm" name="copurpose" id="copurpose">
                </div>
              </div>
             
            </div>
        </div>
      </div>
    </div>
  </div>
</form>
<form id="insertForm" name="insertForm" method="post" >
{{ csrf_field() }}
<div class="modal fade" id="insertModal" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
            <b><u>Visitor Detail</u></b></h4>
      </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-6">
              <div class="form-group">
                <input type="hidden"  readonly name="user_id" value="{{ Auth::user()->id }}">
                <label for="visitorId">Visitor Id</label> 
                <input type="number" step="any" id="visitorId" class="form-control form-control-sm" name="visitor_id" placeholder="Ex. 1110501025" disabled>
                <span class="help-block">*auto-generated.</span>
              </div>
              <div class="form-group">
                <label for="visitorName">Visitor Name</label> 
                <input type="text" id="name" class="form-control form-control-sm" name="name" placeholder="Enter visitor's full name.">
              </div>
              <div class="form-group">

                    <label for="visitorAddress">Visitor Address</label> 
                    <input type="text" id="address" class="form-control form-control-sm" name="address" placeholder="Enter visitor's home address.">
           
              </div>
              <div class="form-group">
                    <label for="companyName">Company Name</label> 
                    <input type="text" id="company" class="form-control form-control-sm" name="company" placeholder="Enter visitor's company-affiliated name.">
           
              </div>
              <div class="form-group">

                    <label for="companyName">Mobile</label> 
                    <input type="text" id="mobile" class="form-control form-control-sm" name="mobile" placeholder="Enter mobile phone number, with format : +62 811 888 8888.">
          
              </div>
              
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-6">
                      <img src="{{Config::get('constants.path.media')}}/users/default.jpg" width="100%" height="100%" alt="..." class="img-thumbnail">
                    </div>
                    <div class="col-lg-6">
                      <img src="{{Config::get('constants.path.media')}}/users/default.jpg" width="100%" height="100%" alt="..." class="img-thumbnail">
                    </div>
                  </div>
                </div>
                <div class="form-group">

                    <label for="CardId">Card Id</label> 
                    <input type="text" id="card_id" class="form-control form-control-sm" name="card_id" placeholder="*card Number">
           
                </div>
                <div class="form-group">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  <button type="button" id="submitNewUser" class="btn btn-primary">Check In</button>
                </div>
              </div>  
            </div>
            <div class="row">
              <div class="col-lg-6">
                 <div class="form-group">
                  <label>Host User:</label>
                  <input type='text' class="form-control form-control-sm" name="pic" id="insertPic">
                </div>
                <div class="form-group">
                  <label>Floor:</label>
                  <input type='text' class="form-control form-control-sm" name="floor" id="insertFloor">
                </div>
                <div class="form-group">
                  <label>Purpose:</label>
                  <input type='text' class="form-control form-control-sm" name="purpose" id="insertPurpose">
                </div>
              </div>
             
            </div>
          </div>
            
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
</form>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function (e) {
  $("#submitNewUser").click(function(e){
    var postData = $( "#insertForm" ).serialize();
    $.ajax({
      url: "{{ URL::route('editor.visitor.insertData') }}",
      data: postData,
      type: "post",
      dataType: "json",
      cache: false,
      success: function(data) {
        if($.isEmptyObject(data.error)){
          toastr.success(data.msg);
            $('#insertForm')[0].reset();
            $("#insertModal").modal("hide");
             dataTuser.ajax.reload( null, false );
        }else{
            printErrorMsg(data.error);
        }
      }
    });
  });
  $("#submitCekOut").click(function(e){
    var postData = $( "#formCekOut" ).serialize();
    $.ajax({
      url: "{{ URL::route('editor.visitor.cekoutData') }}",
      data: postData,
      type: "post",
      dataType: "json",
      cache: false,
      success: function(data) {
        if($.isEmptyObject(data.error)){
          toastr.success(data.msg);
            $('#formCekOut')[0].reset();
            $("#modalCekOut").modal("hide");
             dataTuser.ajax.reload( null, false );
        }else{
            printErrorMsg(data.error);
        }
      }
    });
  });
  $("#submitCekIn").click(function(e){
    var postData = $( "#formCekIn" ).serialize();
    $.ajax({
      url: "{{ URL::route('editor.visitor.cekinData') }}",
      data: postData,
      type: "post",
      dataType: "json",
      cache: false,
      success: function(data) {
        if($.isEmptyObject(data.error)){
          toastr.success(data.msg);
            $('#formCekIn')[0].reset();
            $("#modalCekIn").modal("hide");
             dataTuser.ajax.reload( null, false );
        }else{
            printErrorMsg(data.error);
        }
      }
    });
  });
  var dataTuser = $('#TUser').DataTable({
    'searching': true,
    "processing": true,
    "serverSide": true,
    "pagingType": "full_numbers",
    'fnDrawCallback': function (oSettings) {
      $('.dataTables_filter').each(function () {
        $(this).empty().append('<input type="text" id="searchByName" placeholder="Visitor Name" class="form-control form-control-sm"> '
          +'<input type="button"  class="btn btn-default xs" id="carix" value="search"> '
          +'<button type="button" id="insertButton" class="btn btn-default xs">New Visitor</button> '
        );
        $("#insertButton").click(function(e){
          $("#insertModal").modal("show");
        });
        $("#carix").click(function(e){
          var searchByName = $('#searchByName').val();
          dataTuser.ajax.reload();
          $("#searchByName").val(searchByName);
        });
      });
    },
    "ajax": {
      "url" : "{{ route('editor.visitor.getData') }}",
      "data": function(data){
        data.searchByName = function() { return $('#searchByName').val() };
      }
    },
    "columns": [
      { "data": "id", "width": 10, "orderable": true },
      { "data": "name" , "orderable": false},
      { "data": "address" , "orderable": false},
      { "data": "mobile", "orderable": false },
      { "data": "company", "orderable": false },
      { "data":  null, "orderable": false}
    ],
    "columnDefs": [{
      // puts a button in the last column
      targets: [-1], render: function (a, b, data, d) {
        if (data.CekIn > 0) {
          return "<button type='button' id='"+data.CekIn+"' class='btn btn-warning btn-sm cekout'>Check Out</button>"+data.CekIn;
        } else {
          return "<button type='button' id='"+data.CekIn+"' class='btn btn-success btn-sm cekin'>Check In</button>"+data.CekIn;
        }
      }
    }]
  });

	$('#TUser tbody').on( 'click', '.cekout', function (e) {
		var data = dataTuser.row( $(this).parents('tr') ).data();
		$('#formCekOut')[0].reset();
  	$("#modalCekOut").modal("show");
  	$.ajax({
      url: "{{ URL::route('editor.visitor.getDataDtl') }}",
      data: { visitor_id: data.id, visitor_activity_id: data.CekIn 	},
      type: "get",
      dataType: "json",
      cache: false,
      success: function(data) {
      	$("#visitor_id").val(data.visitor[0].id);
  			$("#visitor_activity_id").val(data.visitor_activity[0].id);
      	$("#covisitorId").val(data.visitor[0].id);
      	$("#coname").val(data.visitor[0].name);
      	$("#coaddress").val(data.visitor[0].address);
      	$("#comobile").val(data.visitor[0].mobile);
      	$("#cocompany").val(data.visitor[0].company);
      	$("#copic").val(data.visitor_activity[0].pic);
      	$("#cofloor").val(data.visitor_activity[0].floor);
      	$("#copurpose").val(data.visitor_activity[0].purpose);
      	$("#cocard_id").val(data.visitor_activity[0].card_id);
      }
    });
  });
$('#TUser tbody').on( 'click', '.cekin', function (e) {
		var data = dataTuser.row( $(this).parents('tr') ).data();
		$('#formCekIn')[0].reset();
  	$("#modalCekIn").modal("show");
  	$.ajax({
      url: "{{ URL::route('editor.visitor.getDataDtl') }}",
      data: { visitor_id: data.id, visitor_activity_id: 0	},
      type: "get",
      dataType: "json",
      cache: false,
      success: function(data) {
      	$("#civisitor_id").val(data.visitor[0].id);
  			$("#civisitor_activity_id").val(0);
      	$("#ciname").val(data.visitor[0].name);
      	$("#ciaddress").val(data.visitor[0].address);
      	$("#cimobile").val(data.visitor[0].mobile);
      	$("#cicompany").val(data.visitor[0].company);
      }
    });
  });
      /* $("#TUser").jsGrid({
        width: "100%",
        height: "500px",
        paging: true,   
        autoload: true,
        pageSize: 10,
        pageButtonCount: 3,
        pageLoading: true,
        updateOnResize: true,
        controller: {
            loadData: function(filter) {
            var d = $.Deferred();
            $.ajax({    
              url: "{{ route('editor.visitor.getData') }}",
              data: filter,
              dataType: "json"
            }).done(function(response){
              d.resolve(response);
            });
            return d.promise();
          }
        },
        fields: [
          { name: "id", type: "text", width: 30 },
          { name: "name", type: "text", width: 100 },
          { name: "address", type: "text", width: 70 },
          { name: "mobile", type: "text", width: 70 },
{ name: "company", type: "text", width: 70 },

          { name: "id", title:"token", type: "text", width: 70 ,
            itemTemplate: function(value, item) {
              var idData = item.id;
              var customEditButton = $("<button>").text("Edit").on("click", function() {
                  $("#UpdateForm .modal-title").empty().append("Update User "+idData);
                  $.ajax({
                    url: "{{ URL::route('editor.visitor.getDataDtl') }}",
                    data: {
                      id: idData,
                      "_token": "{{ csrf_token() }}",
                    },
                    type: "POST",
                    dataType: 'JSON',
                    cache: false,
                    success: function(data) {
                      $.each(data, function(i, e) {
                        $("#UpdateForm #updateId").empty().val(e.id);
                        $("#UpdateForm #updateName").empty().val(e.name);
                        $("#UpdateForm #updateAddress").empty().val(e.address);
                        $("#UpdateForm #updateCompany").empty().val(e.company);
                        $("#UpdateForm #updateMobile").empty().val(e.mobile);
                        $("#UpdateForm #updateCardId").empty().val(e.card_id);
                      });
                    }
                  });
                  $("#UpdateModal").modal("show");
              });
               var UpdatePasswordButton; /* $("<button>").text("password").on("click", function() {
                  $("#UpdatePasswordForm .modal-title").empty().append("Reset Password "+idData);
                  $("#UpdatePasswordForm #updatePassword").val("");
                  $("#UpdatePasswordForm #updateId").val(idData);
                  $("#UpdatePasswordModal").modal("show"); 
              });
             return $("<div>").append(customEditButton).append(" ").append(UpdatePasswordButton);
            }
          },
        ]
      });
      */

      $("#updateUser").click(function(e){
        var postData = $( "#UpdateForm" ).serialize();
        $.ajax({
          url: "{{ URL::route('editor.visitor.updateData') }}",
          data: postData,
          type: "post",
          dataType: "json",
          cache: false,
          success: function(data) {
            if($.isEmptyObject(data.error)){
              toastr.success(data.msg);
              // refresh grid with new data
               $("#TUser").jsGrid("loadData");
               $("#UpdateModal").modal("hide");
            }else{
              printErrorMsg(data.error);
            }
          }
        });
      });
      $("#updatePasswordUser").click(function(e){
        var postData = $( "#UpdatePasswordForm" ).serialize();
        $.ajax({
          url: "{{ URL::route('editor.visitor.resetPassword') }}",
          data: postData,
          type: "POST",
          dataType: "json",
          cache: false,
          success: function(data) {
            if($.isEmptyObject(data.error)){
              toastr.success(data.success);
              // refresh grid with new data
               $("#TUser").jsGrid("loadData");
            // alert(data.success);
            }else{
              printErrorMsg(data.error);
            }
          }
        });
        $("#UpdateModal").modal("hide");
      });
    });
</script>
@endsection