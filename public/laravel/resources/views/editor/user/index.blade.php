@extends('layouts.template')
@section('content')
@actionStart('user', 'read')
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				User Management
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
					@actionStart('user', 'create')
					<a href="#add" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal">
						<i class="la la-plus"></i>
						Add New
					</a>
					@actionEnd
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
			<thead>
				<tr>
					<th>No</th>
					<th>Username</th>
					<th>Position</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $key => $user)
					<tr>
						<td>{{$number++}}</td>
						<td>{{$user->username}}</td>
						<td>{{$user->roleName}}&nbsp;{{$user->divName}}</td>
						<td>
							@actionStart('uprivilege', 'access')
							<a href="{{ URL::route('editor.uprivilege.edit',$user->userId) }}" class="btn btn-sm btn-warning btn-icon btn-icon-md" title="Privilege"><i class="la la-key"></i></a>
							@actionEnd
							
							@actionStart('user', 'edit')
							<a href="#edit{{$user->userId}}" data-toggle="modal" class="btn btn-sm btn-primary btn-icon btn-icon-md" title="Edit"><i class="la la-edit"></i></a>
							@actionEnd
							
							@actionStart('user', 'delete')
							<a href="#delete{{$user->userId}}" class="btn btn-sm btn-google btn-icon btn-icon-md" title="Delete" data-toggle="modal">
								<i class="la la-trash-o"></i>
							</a>
							@actionEnd
						</td>
					</tr>
					
					{{-- BEGIN MODAL EDIT --}}
					<div class="modal fade" id="edit{{$user->userId}}" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
						<div class="modal-dialog">
							{!! Form::open(array('route' => ['editor.user.update', $user->userId], 'method' => 'PUT'))!!}
							{{ csrf_field() }}
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									</button>
								</div>
								<div class="modal-body">
									<div class="form-group">
										<label>Username</label>
										{{-- <input type="text" class="form-control" name="username" value="{{$user->username}}"> --}}
										<input type="text" class="form-control" value="{{$user->username}}" disabled="disabled">
									</div>
									
									<div class="form-group">
										<label>Position</label>
										<select name="userRoleEdit" class="form-control">
											@foreach($roleDivsList as $key => $value)
												@if($value->id == $user->userRoleDivId)
													<option value="{{$value->id}}" selected="selected">{{$value->roleName}}&nbsp;{{$value->divName}}</option>
												@else
													<option value="{{$value->id}}">{{$value->roleName}}&nbsp;{{$value->divName}}</option>
												@endif
											@endforeach
										</select>
										<input type="hidden" class="form-control" name="userRoleEditOld" value="{{$user->userRoleDivId}}">
									</div>									

									{{-- <div class="form-group">
										<label>Password</label>
										<input type="password" class="form-control" name="password" value="">
									</div> --}}
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary">Save</button>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
					{{-- END MODAL EDIT --}}

					{{-- BEGIN MODAL DELETE --}}
					<div class="modal fade" id="delete{{$user->userId}}" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
						<div class="modal-dialog">
							{!! Form::open(array('route' => ['editor.user.delete', $user->userId], 'method' => 'delete'))!!}
							{{ csrf_field() }}
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Delete Data</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									</button>
								</div>
								<div class="modal-body">
									Are you sure want to delete this data?
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
									<button type="submit" class="btn btn-primary">Yes</button>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
					{{-- END MODAL DELETE --}}
				@endforeach
			</tbody>
		</table>

		<!--end: Datatable -->
	</div>

	{{-- BEGIN MODAL ADD --}}
	<div class="modal fade" id="add" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			{!! Form::open(array('route' => ['editor.user.store'], 'method' => 'POST'))!!}
			{{ csrf_field() }}
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add New User</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Username</label>
						<input type="text" class="form-control" name="username">
					</div>
					
					<div class="form-group">
						<label>Position</label>
						<select name="userRoleAdd" class="form-control">
							@foreach($roleDivsList as $key => $value)
								<option value="{{$value->id}}">{{$value->roleName}}&nbsp;{{$value->divName}}</option>
							@endforeach
						</select>
					</div>
					
					<div class="form-group">
						<label>Password</label>
						<input type="password" class="form-control" name="password">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary" name="saveAdd">Save</button>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
	{{-- END MODAL ADD --}}
</div>
@actionEnd
@endsection