@extends('layouts.template')
@section('content')
@actionStart('uprivilege', 'read')
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">User Privilege</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-actions">
				@actionStart('user', 'access')
				<a href="{{ URL::route('editor.user.index') }}" class="btn btn-secondary btn-hover-brand">
					<i class="la la-angle-double-right"></i>
					User Management
				</a>
				@actionEnd
			</div>
			&nbsp;
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
					<button class="btn btn-brand btn-elevate btn-icon-sm" id="selectButton">
						Select All / Deselect All
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">
		<div class="row">
			<div class="col-xl-3"></div>
			<div class="col-xl-6">
				<div class="kt-section">
					<div class="kt-section__body">
						<div class="form-group row">
							<label class="col-3 col-form-label">Username</label>
							<div class="col-9">
								<input class="form-control" type="text" value="{{$user->username}}" disabled="disabled">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-3 col-form-label">Privilege</label>
							<div class="col-9">
								{!! Form::model($user, array('route' => ['editor.uprivilege.update', $user->id], 'method' => 'PUT'))!!}
								{{ csrf_field() }}
								<table class="table">
									<thead>
										<tr>
											<th></th>
											@foreach($actionList as $actionKey => $action)
												<th>{{$action}}</th>
											@endforeach
										</tr>
									</thead>
									<tbody>
									@foreach($moduleList as $moduleKey => $module)
										<tr>
											<td>{{$module}}</td>
											@foreach($actionList as $actionKey => $action)
												<td>
													<label class="kt-checkbox kt-checkbox--success">
														<input type="checkbox" name="privilege[{{$moduleKey}}][{{$actionKey}}]" id="privilege_{{$moduleKey}}_{{$actionKey}}" value="1">
														<span></span>
													</label>
												</td>
											@endforeach
										</tr>
									@endforeach
									</tbody>
								</table>

								<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@actionEnd
@endsection

@section('script')
@if(isset($user))
	<script>
	jQuery.each({!! $user->privilege !!}, function(key, value)
	{
		$('#privilege_'+value['id_modules']+'_'+value['id_actions']).attr('checked', true);
	});
	</script>
@endif

<script>
$(document).ready(function() {
	var clicked = false;
	$("#selectButton").on("click", function() {
		$(":checkbox").prop("checked", !clicked);
		clicked = !clicked;
	});
});
</script>
@endsection