@extends('layouts.template')
@section('content')
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">
        Reporting  <!-- <button id="sads">asdasd</button> 
        <br />
        <p id='asda'>
          
        </p>
        <a href="{{ URL::route('editor.visitor.export') }}" class="btn btn-success my-3" target="_blank">EXPORT EXCEL</a>-->
      </h3>
    </div>
    <div class="kt-portlet__head-toolbar">
      <div class="kt-portlet__head-wrapper">
        <div class="kt-portlet__head-actions">
        </div>
      </div>
    </div>
  </div>
  <div class="kt-portlet__body">
    <div class="row">
      <div class="col-12">
              <!-- <div id="TUser"></div>-->
        <table id="TUser" class="table table-striped- table-bordered table-hover table-checkable">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>visitor_name</th>
                    <th>Address</th>
                    <th>Company Name</th>
                    <th>Mobile</th>
                    <th>Check In</th>
                    <th>Check Out</th>
                    <th><input type="checkbox" name="select_all" id="example-select-all"></th>
                  </tr>
                </thead>
              </table>
      </div>  
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function (e) {
  var dataTuser = $('#TUser').DataTable({
    'searching': true,
    "processing": true,
    "serverSide": true,
    "pagingType": "full_numbers",
    'fnDrawCallback': function (oSettings) {
      $('.dataTables_filter').each(function () {
        $(this).empty().append(
            '<input type="text" id="fromDate" placeholder="From..." class="TANGGAL form-control form-control-sm">&nbsp :&nbsp<input type="text" id="toDate" placeholder="To..." class="TANGGAL form-control form-control-sm"> &nbsp'
            +'<input type="button"  class="btn btn-default xs" id="carix" value="search"> &nbsp<br /><br />'
            +'<select id="ssec" class="col-sm-2" name="ssec"><option value="bypdf">pdf</option><option value="byexcel">excel</option></select> &nbsp'
            +'<button type="button" id="insertButton" class="btn btn-primary xs">Export</button> &nbsp'
        );
        $("#ssec").select2({width:"100px"});
        $('.TANGGAL').datepicker({
          format: 'yyyy-mm-dd'
        });
        $("#insertButton").click(function (e) {
          var favorite = [];
          $("input:checkbox[name=tipe]:checked").each(function(){
            favorite.push($(this).val());
          });
          var url = "{{ URL::route('editor.report.export_query') }}?id=" + favorite;

          window.location = url;
        });
        $("#carix").click(function(e){
          var searchByName = $('#searchByName').val();
          var ssec = $("#ssec").val();
          var from = $("#fromDate").val();
          var to = $("#toDate").val();
          dataTuser.ajax.reload();
        });
      });
    },
    "ajax": {
      "url" : "{{ route('editor.report.getData') }}",
      "data": function(data){
        data.searchByName = function() { return $('#searchByName').val() };
        data.ssec = function() { return $('#ssec').val() };
        data.from = function () { return $("#fromDate").val() };
        data.to = function () { return $("#toDate").val() };
      }
    },
    "columns": [
      { "data": "id", "width": 10, "orderable": true },
      { "data": "name" , "orderable": false},
      { "data": "address" , "orderable": false},
      { "data": "mobile", "orderable": false },
      { "data": "company", "orderable": false },
      { "data": "cek_in", "orderable": false },
      { "data": "cek_out", "orderable": false },
      { "data":  "CekIn", "orderable": false, render: function ( data, type, row ) {
          return '<input type="checkbox" class="checkbox" name="tipe" value="' + row.id + '">';
        }
      }
    ],
  });
  $('#TUser thead').on('click', '#example-select-all', function(e){
    if(this.checked){
      $('.checkbox').each(function(){
        this.checked = true;
      });
    }else{
      $('.checkbox').each(function(){
        this.checked = false;
      });
    }
  });
});
</script>
@endsection