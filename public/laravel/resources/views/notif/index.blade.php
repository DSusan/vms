@foreach($notif as $key)
{!! Form::open(array('route' => ['editor.notif.read', $key->id], 'method' => 'PUT', 'id' => 'readNotifForm'))!!}
{{-- <form method="post" action="{{ URL::route('editor.notif.read') }}" id="readNotifForm"> --}}
	{{ csrf_field() }}
	<input type="text" name="notifKeyId" value="{{$key->id}}">
	<a href="javascript: submitForm();" class="kt-notification__item">
		<div class="kt-notification__item-icon"><i class="flaticon-mail-1 kt-font-success"></i></div>
		<div class="kt-notification__item-details">
			<div class="kt-notification__item-title">
				New data from <b>{{$key->name_from}} {{$key->id}}</b>
			</div>
			<div class="kt-notification__item-time">
				{{$key->date}}
			</div>
		</div>
	</a>
{{-- </form> --}}
{!! Form::close() !!}

<script>
function submitForm(){
    $('#readNotifForm').submit();
}
</script>
@endforeach

