<button class="kt-aside-close " id="kt_aside_close_btn">
	<i class="la la-close"></i>
</button>
<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

	<!-- begin:: Aside Menu -->
	<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
		<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
			<ul class="kt-menu__nav ">
				<li class="kt-menu__item " aria-haspopup="true">
					<a href="{{ URL::route('editor.visitor.dashboard') }}" class="kt-menu__link kt-menu__toggle">
					<i class="kt-menu__link-icon flaticon2-protection"></i><span class="kt-menu__link-text">Dashboard</span>
					</a>
				</li>
				<li class="kt-menu__item " aria-haspopup="true">
					<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
						<i class="kt-menu__link-icon flaticon-avatar"></i>
						<span class="kt-menu__link-text">&nbsp;&nbsp;Visitor</span>
						<i class="kt-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="kt-menu__submenu ">
						<span class="kt-menu__arrow"></span>
						<ul class="kt-menu__subnav">
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="{{ URL::route('editor.visitor.index') }}" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Visitor List</span>
								</a>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="{{ URL::route('editor.visitor.preregister') }}" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Pre-Register</span>
								</a>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="{{ URL::route('editor.visitorocr.index') }}" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Ocr</span>
								</a>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="{{ URL::route('editor.visitor.block') }}" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Block</span>
								</a>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="{{ URL::route('editor.visitor.fc') }}" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Face Recognition</span>
								</a>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="{{ URL::route('editor.visitoractivity.index') }}" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Activity</span>
								</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="kt-menu__item " aria-haspopup="true">
					<a href="{{ URL::route('editor.report.index') }}" class="kt-menu__link kt-menu__toggle">
						<i class="kt-menu__link-icon flaticon-interface-3"></i>
						<span class="kt-menu__link-text">&nbsp;&nbsp;Reporting</span>
					</a>
				</li>
				<li class="kt-menu__item " aria-haspopup="true">
					<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
						<i class="kt-menu__link-icon flaticon-interface-3"></i>
						<span class="kt-menu__link-text">&nbsp;&nbsp;Device management</span>
						<i class="kt-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="kt-menu__submenu ">
						<span class="kt-menu__arrow"></span>
						<ul class="kt-menu__subnav">
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="{{ URL::route('editor.hardware.index') }}" class="kt-menu__link">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Access Control</span>
								</a>
							</li>
							<!-- <li class="kt-menu__item " aria-haspopup="true">
								<a href="{{ URL::route('editor.hardware.index') }}" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Type</span>
								</a>
							</li>-->
						</ul>
					</div>
				</li>
				@actionStart('setting', 'access')
				<li class="kt-menu__item " aria-haspopup="true">
					<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
						<i class="kt-menu__link-icon flaticon2-gear"></i>
						<span class="kt-menu__link-text">&nbsp;&nbsp;Setting</span>
						<i class="kt-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="kt-menu__submenu ">
						<span class="kt-menu__arrow"></span>
						<ul class="kt-menu__subnav">
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="{{ URL::route('editor.user.index') }}" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">User Management</span>
								</a>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="{{ URL::route('editor.rolediv.index') }}" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Position Management</span>
								</a>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="{{ URL::route('editor.module.index') }}" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Module Setting</span>
								</a>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="{{ URL::route('editor.action.index') }}" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Action Setting</span>
								</a>
							</li>
							{{-- <li class="kt-menu__item " aria-haspopup="true">
								<a href="{{ URL::route('editor.role.index') }}" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Role Setting</span>
								</a>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="{{ URL::route('editor.division.index') }}" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Division Setting</span>
								</a>
							</li> --}}
						</ul>
					</div>
				</li>
				@actionEnd
			</ul>
		</div>
	</div>
	<!-- end:: Aside Menu -->
</div>
