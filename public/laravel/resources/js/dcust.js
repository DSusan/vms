function CekSampling(container,table){
	$("#"+container).append("<div class='form-group'>"
		+"<label>Sampling Data</label>"
		+"<select name='id_sampling_data' id='id_sampling_data' class='form-control'><option value='e'>choose</option></select>"
	);
}
$(document).ready(function(e){
	CekSampling("samplingD","gc_ncg_data");
	$("#id_sampling_data").select2({
		ajax: { 
			url: "{{ URL::route('editor.gcncg.getSampling') }}",
			type: "post",
			placeholder: 'Choose',
          	allowClear: true,
			dataType: 'json',
			data: function (params) {
				return {
					searchTerm: params.term,
					"_token": "{{ csrf_token() }}",
					dataTable: table
		   		};
		   	},
		   	processResults: function (response) {
				return {
		        	results: response
		     	};
		   	},
		   	cache: false
		},
		width:"100%"
	}).on('change', function(e) {
        console.log($('#id_sampling_data').val());
    });
});